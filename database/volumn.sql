/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 100134
 Source Host           : localhost:3306
 Source Schema         : volumn

 Target Server Type    : MySQL
 Target Server Version : 100134
 File Encoding         : 65001

 Date: 07/10/2018 16:00:24
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for building
-- ----------------------------
DROP TABLE IF EXISTS `building`;
CREATE TABLE `building`  (
  `id` int(5) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `mac` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` tinyint(1) NULL DEFAULT 1,
  `version` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '1.5',
  `website` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `feedback` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of building
-- ----------------------------
INSERT INTO `building` VALUES (0, 'Nhà o', 'ab:es', 1, '1.5', 'oritek.vn', 'info@oritek.vn');

-- ----------------------------
-- Table structure for equipment_types
-- ----------------------------
DROP TABLE IF EXISTS `equipment_types`;
CREATE TABLE `equipment_types`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_off` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '' COMMENT 'ảnh khi tắt',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 27 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of equipment_types
-- ----------------------------
INSERT INTO `equipment_types` VALUES (4, 'Đèn bàn', '21297_1521993093.png', '18455_1520433062.png', '40056_1520433062.png', '2018-01-25 18:26:49', '2018-04-14 13:18:41');
INSERT INTO `equipment_types` VALUES (9, 'Điều hòa', '85800_1521993135.png', '29194_1520433170.png', '59588_1520433170.png', '2018-01-29 17:05:36', '2018-04-07 10:45:39');
INSERT INTO `equipment_types` VALUES (10, 'Bóng đèn', '59233_1521993011.png', '35520_1520431039.png', '36485_1520431039.png', '2018-01-29 17:05:55', '2018-04-07 10:45:59');
INSERT INTO `equipment_types` VALUES (11, 'Đèn ngủ', '74197_1521993113.png', '82096_1520433123.png', '11686_1520433123.png', '2018-01-29 17:06:17', '2018-04-07 10:46:14');
INSERT INTO `equipment_types` VALUES (17, 'TV', '62070_1521993155.png', '11905_1520433199.png', '24152_1520433199.png', '2018-01-29 17:09:57', '2018-04-07 11:49:31');
INSERT INTO `equipment_types` VALUES (22, 'Đèn chùm', '79835_1521993040.png', '43439_1520432983.png', '80112_1520432983.png', '2018-04-07 11:39:01', '2018-04-07 11:39:01');
INSERT INTO `equipment_types` VALUES (23, 'Ten 2', '79147_1531017181.png', '18976_1531017181.png', '96761_1531017181.png', '2018-07-08 02:33:01', '2018-07-08 02:33:01');
INSERT INTO `equipment_types` VALUES (24, 'Ten 3', '93459_1531017200.png', '87458_1531017200.png', '51393_1531017200.png', '2018-07-08 02:33:20', '2018-07-08 02:33:20');
INSERT INTO `equipment_types` VALUES (25, 'Ten 4', '97170_1531017216.png', '77595_1531017216.png', '96620_1531017216.png', '2018-07-08 02:33:36', '2018-07-08 02:33:36');
INSERT INTO `equipment_types` VALUES (26, 'Ten 5', '69908_1531017236.png', '73789_1531017236.png', '88159_1531017236.png', '2018-07-08 02:33:56', '2018-07-08 02:33:56');

-- ----------------------------
-- Table structure for equipments
-- ----------------------------
DROP TABLE IF EXISTS `equipments`;
CREATE TABLE `equipments`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name_equip` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `room_id` int(10) UNSIGNED NOT NULL,
  `equipment_type_id` int(10) UNSIGNED NOT NULL,
  `group_id` int(10) UNSIGNED NULL DEFAULT NULL,
  `x_offset` int(4) NULL DEFAULT NULL,
  `y_offset` int(4) NULL DEFAULT NULL,
  `ordered_at` tinyint(2) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `status` tinyint(1) NULL DEFAULT 0 COMMENT '0: tắt, 1: bật',
  `iotdevice_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `time` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `alarm_state` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`, `equipment_type_id`) USING BTREE,
  INDEX `fk_a`(`room_id`) USING BTREE,
  INDEX `fk_b`(`equipment_type_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for groups
-- ----------------------------
DROP TABLE IF EXISTS `groups`;
CREATE TABLE `groups`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  `status` tinyint(1) NULL DEFAULT NULL,
  `icon` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `image_off` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of groups
-- ----------------------------
INSERT INTO `groups` VALUES (7, 'Nhóm 2', '2018-02-01 17:14:42', '2018-04-15 04:34:13', NULL, '53893_1523766853.png', '85110_1523766853.png', '32626_1523766853.png');
INSERT INTO `groups` VALUES (8, 'Nhóm 3', '2018-02-01 17:14:49', '2018-04-15 04:34:36', NULL, '97617_1523766876.png', '33997_1523766876.png', '74337_1523766876.png');
INSERT INTO `groups` VALUES (9, 'Nhóm 5', '2018-02-01 17:14:56', '2018-04-15 04:34:54', NULL, '78012_1523766894.png', '79250_1523766894.png', '14874_1523766894.png');
INSERT INTO `groups` VALUES (10, 'Nhòm image', '2018-04-15 04:35:27', '2018-04-15 04:35:27', NULL, '88800_1523766927.png', '59964_1523766927.png', '45493_1523766927.png');
INSERT INTO `groups` VALUES (11, 'Nhóm 5', '2018-04-15 04:35:59', '2018-04-15 04:35:59', NULL, '70327_1523766959.png', '64320_1523766959.png', '69049_1523766959.png');
INSERT INTO `groups` VALUES (12, 'Cảnh 1', '2018-04-15 04:38:39', '2018-04-15 04:38:39', NULL, '19315_1523767119.png', '23920_1523767119.png', '88636_1523767119.png');
INSERT INTO `groups` VALUES (13, 'Cảnh 2', '2018-04-15 04:39:08', '2018-04-15 04:39:08', NULL, '53240_1523767148.png', '58667_1523767148.png', '19910_1523767148.png');
INSERT INTO `groups` VALUES (14, 'cảnh 3', '2018-04-15 04:39:25', '2018-04-15 04:39:25', NULL, '72148_1523767165.png', '56731_1523767165.png', '66165_1523767165.png');

-- ----------------------------
-- Table structure for iotdevice
-- ----------------------------
DROP TABLE IF EXISTS `iotdevice`;
CREATE TABLE `iotdevice`  (
  `id` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `cid` int(10) NULL DEFAULT NULL,
  `ip` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `mac` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `name_wifi` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `password_wifi` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `active` varchar(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0',
  `online` varchar(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ping` bigint(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for rooms
-- ----------------------------
DROP TABLE IF EXISTS `rooms`;
CREATE TABLE `rooms`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of rooms
-- ----------------------------
INSERT INTO `rooms` VALUES (13, 'abc', NULL, 'no-image-choosen', '2018-07-15 06:22:40', '2018-07-15 06:22:40', 0);
INSERT INTO `rooms` VALUES (14, 'cua cuon thang cuon', NULL, '47589_1531658398.jpeg', '2018-07-15 12:39:58', '2018-07-15 12:39:58', 0);
INSERT INTO `rooms` VALUES (15, 'nguen van a', NULL, '87750_1531658433.jpg', '2018-07-15 12:40:33', '2018-07-15 12:40:33', 0);

-- ----------------------------
-- Table structure for sub_rooms
-- ----------------------------
DROP TABLE IF EXISTS `sub_rooms`;
CREATE TABLE `sub_rooms`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `room_id` int(10) NULL DEFAULT NULL,
  `x_offset` int(5) NULL DEFAULT NULL,
  `y_offset` int(5) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sub_rooms
-- ----------------------------
INSERT INTO `sub_rooms` VALUES (1, 'Phòng tắm', 1, 878, 490);
INSERT INTO `sub_rooms` VALUES (2, 'phòng 2', 1, 190, 130);
INSERT INTO `sub_rooms` VALUES (3, 'Ten 1', 1, 713, -50);
INSERT INTO `sub_rooms` VALUES (4, 'phòng 8', 1, 843, 92);
INSERT INTO `sub_rooms` VALUES (5, '7989', 1, -222, 224);
INSERT INTO `sub_rooms` VALUES (6, NULL, 1, 432, 24);
INSERT INTO `sub_rooms` VALUES (7, NULL, 1, 563, 53);

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted` tinyint(1) NULL DEFAULT 0,
  `role` tinyint(1) NULL DEFAULT 2,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `users_email_unique`(`email`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'admin', 'admin@gmail.com', '$2y$10$IVfdW86b6CfjKGDpoO8w3OWr3CuIGBf71e0OPh82BlouvnHuZmQXW', 'vATCO91Wmay67Dcpb8b9wE8UiGU79rqzGDUKQJJnhnL1KJ8i6pQd2FG9bNM6', '2018-01-25 13:35:55', '2018-06-29 13:46:06', 0, 1, '2fa843442b27dc23d6a701b47a364b23', '29116_1530279966.jpg');
INSERT INTO `users` VALUES (2, 'user', 'user@gmail.com', '$2y$10$.msMJJ/SQkKIi4BcFcwhqesTQahfEriLss0Mjjv5qpSd16PSyQVmO', 'NQSrbvT3aGN4NhCzIJxLJ3RE4ZabRMF1B52jYPkEEr01CJ2qhcV9Hd19eoYk', '2018-01-26 11:44:18', '2018-02-03 10:38:38', 1, 2, NULL, NULL);
INSERT INTO `users` VALUES (3, 'user mới', 'u1@gmail.com', '$2y$10$b6Jt2T4qKqPCTK6uHJn5heOn6tu/PfwhBoeNWoTA1ElWFJparsoke', 'CWIdJ5cUp3aXxvGOHJMvPwOPczuyCduMPXzLAQU92a2K2uvJ3x0uvd5F2PLm', '2018-02-03 10:55:08', '2018-02-03 10:56:23', 0, 2, NULL, NULL);
INSERT INTO `users` VALUES (4, 'hungdv2', 'hungdv2@topica.edu.vn', '$2y$10$iXoSJzPAut.PEL/XNR.MVuBBWL4Iws4V8gDo8EGdLfU81X/xe3DhG', 'o9La2T6kOqNyZU5azCgHSa7tdWxd7qcJ', '2018-06-30 19:41:51', '2018-07-01 02:09:26', 0, 2, NULL, NULL);

-- ----------------------------
-- Table structure for wifi
-- ----------------------------
DROP TABLE IF EXISTS `wifi`;
CREATE TABLE `wifi`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name_wifi` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `password_wifi` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ssid` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

SET FOREIGN_KEY_CHECKS = 1;

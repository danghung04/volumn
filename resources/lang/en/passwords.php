<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Mật khẩu phải có ít nhất 6 ký tự và giống với xác nhận mật khẩu.',
    'reset' => 'Mật khẩu đã được reset!',
    'sent' => 'Chúng tôi đã gửi email để reset mật khẩu cho bạn!',
    'token' => 'Mã token không hợp lệ.',
    'user' => "Email không tồn tại",

];

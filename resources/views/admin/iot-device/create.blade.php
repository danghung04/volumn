@extends('admin.index')
@section('custom css')
 <style>
	.portlet-body form{
		padding-left:18px;
	}
	label.error {
		  color: #a94442;
		  border-color: #ebccd1;
		  padding:1px 20px 1px 20px;
		}
</style>
@stop
@section('custom script')

<script src="{!! asset('public/theme/global/plugins/jquery.validate.min.js') !!}" type="text/javascript"></script>
<script>

	$(function () {


});
</script>

@stop

@section('content')
<h3 class="page-title">
Quản lý thiết bị iot
</h3>
<div class="row">
	<div class="col-md-12">
		<div class="portlet box blue">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-globe"></i>Thêm thiết bị iot
				</div>
			</div>
			<div class="portlet-body form">
				<form class="form-horizontal" id="create-form" action="{{ route('admin.iot-device.store') }}" method="POST" >
				{!! csrf_field() !!}
					<div class="form-body">						
						<div class="form-group">
							<label class="col-md-2 control-label">Tên</label>
							<div class="col-md-10">
								<input type="text" name="name" class="form-control" value="{{ old('name') }}">
								<div class="form-control-focus">
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label">IP</label>
							<div class="col-md-10">
								<input type="text" name="ip" class="form-control" value="{{ old('ip') }}" required>
								<div class="form-control-focus">
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label">MAC</label>
							<div class="col-md-10">
								<input type="text" name="mac" class="form-control" value="{{ old('mac') }}" required>
								<div class="form-control-focus">
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label">Tên wifi</label>
							<div class="col-md-10">
								<input type="text" name="name_wifi" class="form-control" value="{{ old('name_wifi') }}" required>
								<div class="form-control-focus">
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label">Mật khẩu wifi</label>
							<div class="col-md-10">
								<input type="text" name="password_wifi" class="form-control" value="{{ old('password_wifi') }}" required>
								<div class="form-control-focus">
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-3 col-md-offset-2">
								<button type="submit" class="btn blue">Tạo mới</button>
							</div>
						</div>
					</div>	
				</form>
			</div>
		</div>	
	</div>
</div>
@stop

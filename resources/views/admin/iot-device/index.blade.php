@extends('admin.index')
@section('custom css')
    <style>
        .portlet.box > .portlet-body {
            background: transparent;
            padding: 0px;
            box-shadow: none;
            width: 100%;
        }

        .portlet-body .header {
            height: 100px;
            width: 100%;
        }

        .left-header {
            height: 100%;
            padding: 20px;
        }

        .left-header button {
            font-size: 1.2em;
            font-weight: bold;
            text-transform: uppercase;
        }

        .right-header {
            height: 100%;
        }

        .portlet-body .header .right-header .wrap-progress {
            height: 100%;
            padding-top: 5px;
        }

        /*--progress bar --*/
        .portlet-body .header .right-header .wrap-progress .progress-bar {
            background: transparent;
            /*background: blue;*/
            height: 100%;
            width: 137px;
            float: left;
            box-shadow: none;
        }

        .portlet-body .header .right-header .wrap-progress .progress-bar {
            padding: 5px 20px;
            float: left;
        }

        .portlet-body .header .right-header .wrap-progress .progress-bar .phead {
            position: relative;
        }

        .portlet-body .header .right-header .wrap-progress .progress-bar .phead .circle {
            width: 50px;
            display: inline-block;
            position: relative;
            text-align: center;
            vertical-align: top;
        }

        .portlet-body .header .right-header .wrap-progress .progress-bar .circle strong {
            position: absolute;
            left: 0;
            text-align: center;
            line-height: 45px;
            font-size: 43px;
        }

        .portlet-body .header .right-header .wrap-progress .progress-bar .portion {
            position: absolute;
            top: 14px;
            left: 29px;
            font-size: 13px;
        }

        .portlet-body .header .right-header .wrap-progress .progress-bar .portion .exist {
            font-size: 16px;
        }

        .portlet-body .header .right-header .wrap-progress .progress-bar .ptitle {
            position: absolute;
            font-size: 14px;
            color: #ccc;
        }

        /*--end progress bar--*/

        .portlet-body table {
            width: 100%;
            color: #ccc;
            text-align: center;
        }

        .portlet-body table tr td, .portlet-body table tr th {
            border-bottom: 1px solid #657293;
            border-right: 1px solid #657293;
            font-size: 1.2em;
        }

        .portlet-body table tr th {
            padding: 5px;
            font-weight: normal;
            background: #020E21;
            text-align: center;
            text-transform: uppercase;
        }

        .portlet-body table tr td:last-child, .portlet-body table tr th:last-child {
            border-right-width: 0px;
        }

        .portlet-body table tr td {
            height: 50px;
            line-height: 50px;
        }

        .portlet-body table tr td.action {
            text-align: center;
        }

        .portlet-body table tr.add-new td {
            border: none;
            padding-left: 5%;
        }

        .portlet-body table tr.add-new .btn-add {
            font-weight: bold;
            font-size: 2.7em;
            color: #efefef;
            cursor: pointer;
        }

        .portlet-body table tr.add-new .btn-add:hover {
            color: #fff;
        }

        .orange {
            background: #FFBA00;
            color: #000;
        }

        .orange:hover {
            background: #FFAC00;
        }
    </style>
@stop
@section('content')
    <h3 class="page-title">

    </h3>
    <div class="row main-content">
        <div class="portlet box">
            <div class="portlet-body">
                <div class="header">
                    <div class="col-xs-6 left-header">
                        <button class="btn orange" data-toggle="modal" data-target="#modal-custom">Cài đặt wifi</button>
                    </div>
                    <div class="col-xs-6 right-header">
                        <div class="wrap-progress pull-right">
                            <div class="progress-bar">
                                <div class="phead">
                                    <div class="circle" id="circle-accept">
                                        <strong>
                                        </strong>
                                    </div>
                                    <div class="portion">
                                        <span class="exist">{!! $count_iot_accept !!}</span>/{!! $total_iot !!}
                                    </div>
                                </div>
                                <div class="ptitle">
                                    Cho phép/Tổng
                                </div>
                            </div>
                            <div class="progress-bar">
                                <div class="phead">
                                    <div class="circle" id="circle-waiting">
                                        <strong>
                                        </strong>
                                    </div>
                                    <div class="portion">
                                        <span class="exist">{!! $count_iot_waiting !!}</span>/{!! $total_iot !!}
                                    </div>
                                </div>
                                <div class="ptitle">
                                    Đang chờ/Tổng
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <form action="{!! route('admin.iot-device.postDelete') !!}" method="POST">
                    <table>
                        <thead>
                            <tr>
                                <th style="width:5%">STT</th>
                                <th>ID</th>
                                <th>Tên</th>
                                <th>IP</th>
                                <th>MAC</th>
                                <th>Tên wifi</th>
                                <th>Cho phép</th>
                                <th style="width:10%">Trạng thái</th>
                                <th style="width:5%"><input type="checkbox" class="all-delete"
                                                            style="width: 16px; height: 16px;"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($iots as $key => $iot)
                                <tr>
                                    <td>{{ $key + 1 }}</td>
                                    <td>{{ $iot->id }}</td>
                                    <td>0{{ $iot->quantity }}</td>
                                    <td>{{ $iot->ip }}</td>
                                    <td>{{ $iot->mac }}</td>
                                    <td>{{ $iot->name_wifi }}</td>
                                    <td><input type="checkbox" id="{{ $iot->id }}" class="update-active"
                                               style="width: 16px; height: 16px;"
                                               @if($iot->active) checked @endif></td>
                                    <td>@if($iot->online == 1) Online @else Offline @endif</td>
                                    <td><input type="checkbox" class="check-delete" value="{!! $iot->id !!}"
                                               name="checks[]" style="width: 16px; height: 16px;"/></td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                    <div class="col-md-12 pull-right">
                        <div class="pull-left" style="margin-top: 5px">
                            <button type="submit" class="btn btn-danger">Xóa</button>
                        </div>
                        <div class="pull-right">
                            {{ $iots->render() }}
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {!! $form_setting !!}
    <?php if($total_iot == 0) $total_iot = 1; ?>
@stop
@section('custom script')
    <script crossorigin="anonymous"
            src="{{ asset('public/design/js/circle-progress.min.js') }}">
    </script>
    <!-- progress bar circle -->
    <script>
        $(function () {
            var progressBarOptions = {
                startAngle: -1.55,
                size: 50,
                value: 0.75,
                fill: {
                    color: '#ffa500'
                }
            }

            $('.circle').circleProgress(progressBarOptions).on('circle-animation-progress', function (event, progress, stepValue) {
                //$(this).find('strong').text(String(stepValue.toFixed(2)).substr(1));
            });

            var percent_accept = '{!! $count_iot_accept/$total_iot !!}';
            var percent_waiting = '{!! $count_iot_waiting/$total_iot !!}';
            var circle_accept_data = {
                value: percent_accept,
                fill: {
                    color: '#2BEA09'
                }
            };

            var circle_waiting_data = {
                value: percent_waiting,
                fill: {
                    color: '#F1DD05'
                }
            };
            $('#circle-accept').circleProgress(circle_accept_data);

            $('#circle-waiting').circleProgress(circle_waiting_data);

            var socket_url = SOCKET_URL;
            var socket = io(socket_url);
            socket.on('wifichangeresponse', function (data) {
                console.log('change wifi: ' + data);
                var res = JSON.parse(data);
                if (res.wifichange == 'ok') {
                    submitSettingWifi();
                } else {
                    location.reload();
                }
            });
            $('.update-active').change(function () {

                var active = $(this).prop('checked') ? 1 : 0;
                var per = 'deny';
                if (active === 1)
                    per = 'grant';
                var id = $(this).attr('id');
                var dataSend = '{"id":' + id + ',"permission":' + per + '}';
                socket.emit('permission', dataSend);
                $('.update-active').prop('disabled', true);
                var url = "{{ route('admin.iot-device.active') }}";
                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    url: url,
                    data: {
                        id: id,
                        active: active
                    },
                    success: function (data) {
                        $('.update-active').removeAttr('disabled');
                        if (data.state) {
                            location.reload();
                        } else {

                        }
                    }
                });
            });
            $('.savemodal-custom').attr('type', 'submit')
            $('#modal-custom-form').on('submit', function (e) {
                e.preventDefault();
                var form = $('#modal-custom-form');
                var name_wifi_old = form.find('select[name="name_wifi_old"]').val();
                var password_wifi = form.find('input[name="password_wifi"]').val();
                var name_wifi = form.find('input[name="name_wifi"]').val();
                var url = '{!! route('admin.wifi.iot') !!}';
                var _token = '{!! csrf_token() !!}';
                $.ajax({
                    type: 'POST',
                    url: url,
                    dataType: 'json',
                    data: {
                        _token: _token,
                        name_wifi_old: name_wifi_old,
                        name_wifi: name_wifi,
                        password_wifi: password_wifi
                    }
                }).done(function (response) {
                    for (var key in response) {
                        var item = response[key];
                        if (name_wifi === '')
                            name_wifi = name_wifi_old;
                        var dataSend = '{"id":"' + item.id + '","ssid":"' + name_wifi + '","password_wifi":"' + password_wifi + '"}';
                        socket.emit('wifichange', dataSend);
                    }
                    setInterval(function(){
                        location.reload();
                    }, 500);
                });
                return false;
            });


            function submitSettingWifi() {
                var form = $('#modal-custom-form');
                var url = $(form).attr('action');
                var name_wifi = form.find('select[name="name_wifi"]').val();
                var password_wifi = form.find('input[name="password_wifi"]').val();
                var ssid = form.find('input[name="ssid"]').val();
                var _token = '{!! csrf_token() !!}';
                $.ajax({
                    type: 'POST',
                    url: url,
                    data: {
                        _token: _token,
                        name_wifi: name_wifi,
                        password_wifi: password_wifi,
                        ssid: ssid
                    }
                }).done(function (data) {
                    location.reload();
                });
            }

            $('.all-delete').click(function () {
                var is_all_check = $(this).prop('checked');
                var check_element = $('.check-delete').parent();
                if (is_all_check) {
                    check_element.addClass("checked");
                    check_element.find('input').attr('checked', 'checked');
                } else {
                    check_element.removeClass("checked");
                    check_element.find('input').removeAttr('checked');
                }
            });
        });
    </script>
@stop
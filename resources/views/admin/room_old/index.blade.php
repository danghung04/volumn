@extends('admin.index')

@section('content')
<h3 class="page-title">
Quản lý phòng
</h3>
<div class="row">
	<div class="col-md-12">
		<div class="portlet box blue">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-globe"></i>Quản lý phòng
				</div>
			</div>
			<div class="portlet-body">
				<div class="table-toolbar">
					<div class="row">
						<div class="col-md-6">
							<div class="btn-group">
								<a href="{{ route('admin.room.create') }}" class="btn blue">
									Tạo mới <i class="fa fa-plus"></i>
								</a>
							</div>
						</div>
					</div>
				</div>				
				<table class="table table-striped table-hover table-bordered">
					<thead>
						<tr>
							<th style="width:5%">SL</th>
							<th>Tên</th>
							<th>Hình nền</th>
							<th>Mô tả</th>
							<th style="width:8%">Cập nhật</th>
							<th style="width:8%">Xóa</th>
						</tr>
					</thead>
					<tbody>
						@foreach($rooms as $key => $room)
						<tr>
							<td>{{ $key + 1 }}</td>
							<td>{{ $room->name }}</td>
							<td><img src="{{ asset(config('constants.image')).'/'.$room->image }}" style="height: 75px"/></td>
							<td>{{ $room->description }}</td>
							<td>
								<a href="{{ route('admin.room.edit',['id'=>$room->id]) }}" class="btn blue update"><i class="fa fa-pencil li-btn"></i>Cập nhật</a>
							</td>
							<td>
								<a href="{{ route('admin.room.delete',['id'=>$room->id]) }}" onclick="return confirm('Bạn có chắc muốn xóa bản ghi này không?')" class="btn btn-danger"><i class="fa fa-trash-o li-btn"></i>Xóa</a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
				<div class="col-md-12 pull-right">
					<div class="pull-right">
						{{ $rooms->render() }}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@stop
@extends('admin.index')
@section('custom css')
 <style>
	.portlet-body form{
		padding-left:18px;
	}
	label.error {
		  color: #a94442;
		  border-color: #ebccd1;
		  padding:1px 20px 1px 20px;
		}
</style>
@stop
@section('custom script')

<script src="{!! asset('public/theme/global/plugins/jquery.validate.min.js') !!}" type="text/javascript"></script>
<script>

	$(function () {

    $('#update-form').validate({
        rules: {
            name: {
                required: true,
            },
            image: {
                extension: "jpg|jpeg|png|gif"
            },
        }       
    });
});
</script>

@stop

@section('content')
<h3 class="page-title">
Quản lý phòng
</h3>
<div class="row">
	<div class="col-md-12">
		<div class="portlet box blue">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-globe"></i>Cập nhật 
				</div>
			</div>
			<div class="portlet-body form">
				<form class="form-horizontal" id="update-form" action="{{ route('admin.room.update') }}" method="POST" enctype="multipart/form-data">
					{!! csrf_field() !!}
					<input type="hidden" name="id" value="{{ $room->id }}">
					<div class="form-body">						
						<div class="form-group">
							<label class="col-md-2 control-label">Tên</label>
							<div class="col-md-10">
								<input type="text" name="name" class="form-control" value="{{ $room->name }}" required>
								<div class="form-control-focus">
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label">Ảnh đại diện</label>
							<div class="col-md-10">
								<img src="{{ asset(config('constants.image')).'/'.$room->image }}" alt="" style="height: 100px">
								<input type="file" name="image" class="form-control" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label">Mô tả</label>
							<div class="col-md-10">
								<textarea type="text" name="description" class="form-control">{{ $room->description }}</textarea>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-3 col-md-offset-2">
								<button type="submit" class="btn blue">Cập nhật</button>
							</div>
						</div>
					</div>	
				</form>
			</div>
		</div>	
	</div>
</div>
@stop

@extends('admin.index')
@section('custom css')
 <style>
	.portlet-body form{
		padding-left:18px;
	}
	label.error {
		  color: #a94442;
		  border-color: #ebccd1;
		  padding:1px 20px 1px 20px;
		}
</style>
@stop
@section('custom script')

<script src="https://cdn.jsdelivr.net/jquery.validation/1.15.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.15.0/additional-methods.min.js"></script>
<script>

	$(function () {

    $('#create-form').validate({
        rules: {
            name: {
                required: true,
            }
        }
    });
});
</script>

<script type="text/javascript">
	$("#image").change(function () {
	    readURL(this);
	    function readURL(input) {
	        if (input.files && input.files[0]) {
	            var reader = new FileReader();
	            reader.onload = function (e) {
	            $('#img').attr('src', e.target.result);
	        }
	        reader.readAsDataURL(input.files[0]);
	        $('#img').css('height','150px');
	     }
	    }
	});
</script>
@stop

@section('content')
<h3 class="page-title">
Quản lý loại thiết bị
</h3>
<div class="row">
	<div class="col-md-12">
		<div class="portlet box blue">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-globe"></i>Thêm loại thiết bị
				</div>
			</div>
			<div class="portlet-body form">
				<form class="form-horizontal" id="create-form" action="{{ route('admin.equipment-type.update') }}" method="POST" enctype="multipart/form-data">

				{!! csrf_field() !!}
				<input type="hidden" name="id" value="{{ $equipment->id }}">
					<div class="form-body">
						<div class="form-group">
							<label class="col-md-2 control-label">Tên</label>
							<div class="col-md-10">
								<input type="text" name="name" class="form-control" value="{{ $equipment->name }}" required>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label">Icon</label>
							<div class="col-md-10">
								<img src="{{ asset(config('constants.icon')).'/'.$equipment->icon }}" alt="" style="height: 32px">
								<input type="file" name="icon" class="form-control">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label">Ảnh thiết bị (bật)</label>
							<div class="col-md-10">
								<img src="{{ asset(config('constants.image')).'/'.$equipment->image }}" alt="" style="height: 100px">
								<input type="file" name="image" class="form-control">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label">Ảnh thiết bị (tắt)</label>
							<div class="col-md-10">
								<img src="{{ asset(config('constants.image')).'/'.$equipment->image_off }}" alt="" style="height: 100px">
								<input type="file" name="image_off" class="form-control">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label">Mô tả</label>
							<div class="col-md-10">
								<textarea name="description" class="form-control">{{ $equipment->description }}</textarea>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-3 col-md-offset-2">
								<button type="submit" class="btn blue">Cập nhật</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@stop

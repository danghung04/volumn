@extends('admin.index')
@section('custom css')
    <style>
        .portlet.box > .portlet-body {
            background: transparent;
            padding: 0px;
            box-shadow: none;
            width: 100%;
        }

        .grid-wrapper {
            display: grid;
            grid-template-columns: auto auto auto auto auto auto;
            width: 80%;
        }

        .grid-wrapper {
            width: 700px;
            margin: 50px auto;
        }

        .grid-wrapper .grid-item {
            height: 85px;
            margin-top: 20px;
            position: relative;
            cursor: pointer;
        }

        .grid-wrapper .grid-item .icon {
            height: 65px;
            text-align: center;
        }

        .grid-wrapper .grid-item .title {
            text-align: center;
            color: #ccc;
        }

        .add-equipment {
            display: grid;
            width: 700px;
            margin: 50px auto;
        }

        .btn-action {
            display: none;
            width: 25px;
            height: 25px;
            padding: 0;
            z-index: 11;
        }

        .btn-edit {
            position: absolute;
            left: -5px;
            top: -5px;
        }

        .btn-delete {
            position: absolute;
            right: -5px;
            top: -5px;
        }

        .grid-item:hover .btn-action {
            display: block;
        }
        .my-close span{
            font-size: 24px;
        }
        .orange {
            background: #FFBA00;
            color: #000;
        }

        .orange:hover {
            background: #FFAC00;
        }
    </style>
@stop
@section('content')
    <h3 class="page-title">

    </h3>
    <div class="row main-content">
        <div class="portlet-body">
            <div class="grid-wrapper" style="text-transform: none">
                @foreach($equipment_types as $equipment_type)
                    <div class="grid-item">
                        <button class="btn orange btn-circle btn-action btn-edit"><i class="fa fa-pencil"></i></button>
                        <button class="btn btn-danger btn-circle btn-action btn-delete" field-id="{!! $equipment_type->id !!}" data-toggle="modal" data-target="#modal-delete-warning"><span class="glyphicon glyphicon-remove"></span></button>
                        <div class="icon">
                            <img style="height: 60px" eid="{!! $equipment_type->id !!}" icon="{!! asset(config('constants.icon')).'/'.$equipment_type->icon !!}"
                                 image="{!! asset(config('constants.image')).'/'.$equipment_type->image !!}"
                                 image_off="{!! asset(config('constants.image')).'/'.$equipment_type->image_off !!}"
                                 @if($equipment_type->icon) src="{!! asset(config('constants.icon')).'/'.$equipment_type->icon !!}" @else src="{!! asset('public/images/choose-image.gif') !!}" @endif  alt="">
                        </div>
                        <div class="title">
                            {!! $equipment_type->name !!}
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="add-equipment">
                <span style="font-size: 24px; color: #ccc; cursor: pointer"
                      class="add-form-btn">Thêm loại thiết bị</span>
            </div>
        </div>
    </div>
    {!! $setting_form !!}
    {!! $add_form !!}
    {!! $warning_delete !!}
@stop
@section('custom script')
    <script>
        $(function () {
            var modal_add = '#modal-add-equipment';
            var urlCheckDuplicate = '{!! route('admin.equipment-type.checkDuplicate') !!}';
            $('.save-add-equipment').on('click', function () {
                var name = $(modal_add + ' input[name="name"]').val();
                var data = {name: name};
                checkErrorAndSubmitForm(modal_add, urlCheckDuplicate, data);
            });
            var modal_update = '#modal-setting-equipment';
            $('.save-equipment-update').on('click', function () {
                var name = $(modal_update + ' input[name="name"]').val();
                var id = $(modal_update + ' input[name="id"]').val();
                var data = {name: name, id: id};
                checkErrorAndSubmitForm(modal_update, urlCheckDuplicate, data);
            });

            function checkErrorAndSubmitForm(modal, url, data) {
                if (data.name === '') {
                    $(modal + ' .my-error').text('Tên không được để trống');
                    return false;
                }
                $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: 'json',
                    data: data
                }).done(response => {
                    if (response.state === 0) {
                        $(modal + '-form').submit();
                    } else {
                        $(modal + ' .my-error').text(response.msg);
                    }
                })
            }
        });
    </script>
    <script>
        $(function () {
            $('.btn-edit').click(function () {
                var img = $(this).parent().find('img');
                var id = $(img).attr('eid');
                var icon = $(img).attr('icon');
                var image = $(img).attr('image');
                var image_off = $(img).attr('image_off');
                var name = $(img).parent().parent().find('.title').text().trim();
                var modal = $('#modal-setting-equipment');
                modal.find('input[name = "id"]').val(id);
                modal.find('input[name = "name"]').val(name);
                modal.find('#upfile-image-icon').attr('src', icon);
                modal.find('#upfile-image-on').attr('src', image);
                modal.find('#upfile-image-off').attr('src', image_off);
                modal.find('.my-error').text('');
                modal.modal('show');
            });
        });

        $('.add-form-btn').click(function () {
            $('.my-error').text('')
            $('#modal-add-equipment').modal('show');
        });
        var delete_url = '';
        $('.btn-delete').on('click', function () {
            var id = $(this).attr('field-id');
            var url = '{!! $url_delete !!}' + '?id=' + id;
            delete_url = url;
        });
        $('.confirm-delete').on('click', function () {
            if (delete_url !== '')
                window.location.replace(delete_url);
        });

        $('.close-modal').click(function () {
            $('.modal').modal('hide');
        });
    </script>
@stop
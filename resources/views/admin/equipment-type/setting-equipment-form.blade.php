<style>
    .modal-content {
        padding: 5px;
        color: #fff;
    }

    .form-group > label {
        color: #fff;
    }

    .orange {
        background: #FFBA00;
    }

    .orange:hover {
        background: #da8c00;
    }

    .modal-footer {
        color: #000;
    }

    .my-close {
        cursor: pointer;
    }

    #file-image {
        font-size: 20px;
        height: 50px;
    }

    .imagediv {
        float: left;
        margin-top: 50px;
    }

    .imagediv .showonhover {
        background: red;
        padding: 20px;
        opacity: 0.9;
        color: white;
        width: 100%;
        display: block;
        text-align: center;
        cursor: pointer;
    }

    .my-table {
        height: 120px;
    }

    .my-table .my-row {
        height: 20%;
        display: grid;
        grid-template-columns: auto auto auto;
        column-gap: 1px;
        width: 100px;
    }

    .my-table .bottom-row {
        height: 50px;
    }

    .my-table .bottom-row .my-cell {
        height: 50px;
    }

    .my-table .my-row .my-cell {
        width: 80px;
        overflow: hidden;
        padding: 20px;
        text-align: center;
        color: #ccc;
    }

    .my-table .my-row.image-group .my-cell {
        height: 80px;
        background: transparent;
    }

    .my-table .my-row.image-group .middle-icon-cell {
        position: relative;
    }

    .my-table .my-row.image-group .my-cell img {
        background: transparent;
        width: 100%;
        cursor: pointer;
    }

    .my-table .my-row.image-group .my-cell img#upfile-image-icon {
        width: 80%;
    }

    .border-main {
        position: absolute;
        height: 60%;
        width: 2px;
        top: 20px;
        background: #666;
    }

    .border-main-left {
        left: 0;
        width: 3px;
    }

    .border-main-right {
        right: 0;
    }

    .my-table .my-row:last-child {
        position: absolute;
        bottom: 0;
    }

    .modal-header {
        padding-bottom: 5px;
    }

    .form-body {
        padding-right: 30px;
    }

    .my-label {
        padding-left: 40px;
    }

    .my-input {
        width: 70%;
    }
</style>
<?php
$dialog_width = isset($form_width) ? $form_width . 'px' : '480px';
$label_portion = isset($label_width) ? $label_width : 5;
$input_portion = 12 - $label_portion;
$label_width_class = 'col-xs-' . $label_portion;
$input_width_class = 'col-xs-' . $input_portion;
?>
<div class="modal fade in" id="modal-setting-equipment">
    <div class="modal-dialog" style="width: {!! $dialog_width !!}">
        <div class="modal-content">
            <div class="modal-header">
                <div class="my-close" data-dismiss="modal" aria-hidden="true"><span
                            class="glyphicon glyphicon-remove-circle pull-right" style="color: #fff"></span></div>
                <h4 class="modal-title text-center" style="color: #fff;text-transform: uppercase;font-size: 2em;
            font-weight: bold;">Cài đặt loại thiết bị</h4>
            </div>
            <div class="my-divider" style="height: 20px;width: 100%;padding: 5px;">
                <div class="my-bar" style="margin:auto; width: 90%; background: #666; height: 1px;">
                </div>
            </div>
            <form class="form-horizontal" id="modal-setting-equipment-form"
                  action="{!! route('admin.equipment-type.update') !!}"
                  method="POST" enctype="multipart/form-data">
                {!! csrf_field() !!}
                <input type="hidden" name="id" value>
                <div class="form-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="form-group" style="margin-bottom: 0">
                        <label class="{!! $label_width_class !!} control-label my-label" style="padding-left: 40px">Tên
                                                                                                                    loại
                                                                                                                    thiết
                                                                                                                    bị</label>
                        <div class="{!! $input_width_class !!}">
                            <input type="text" name="name"
                                   class="form-control" value="{!! old('name') !!}"
                                   required>
                            <span class="my-error"></span>
                        </div>
                    </div>
                    <div class="my-divider" style="height: 20px;width: 100%;padding: 0 5px 5px 0;">
                        <div class="my-bar" style="margin:auto; width: 80%; background: #666; height: 1px;">
                        </div>
                    </div>
                    <div class="form-group" style="margin-bottom: 0">
                        <div class="col-xs-3" style="padding-left: 40px; padding-right: 5px">
                            <label class="control-label" style="padding-right: 0">Icon
                            </label>
                            <div class="my-divider" style="height: 20px;width: 100%;padding: 0 5px 5px 0;">
                                <div class="my-bar" style="background: #666; height: 1px;">
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-9">
                            <div class="my-table">
                                <div class="my-row image-group">
                                    <div class="my-cell">
                                        <input accept="image/*" capture="" id="file-image-icon" name="icon"
                                               style="display:none" type="file"/>
                                        <img id="upfile-image-icon"
                                             src="{!! asset('public/images/choose-image.gif') !!}"
                                        />
                                    </div>
                                    <div class="my-cell middle-icon-cell">
                                        <div class="border-main border-main-left">
                                        </div>
                                        <div class="middle-main">
                                            <input accept="image/*" capture="" id="file-image-on" name="image"
                                                   style="display:none" type="file"/>
                                            <img id="upfile-image-on"
                                                 src="{!! asset('public/images/choose-image.gif') !!}"
                                            />
                                        </div>
                                        <div class="border-main border-main-right">
                                        </div>
                                    </div>

                                    <div class="my-cell">
                                        <input accept="image/*" capture="" id="file-image-off" name="image_off"
                                               style="display:none" type="file"/>
                                        <img id="upfile-image-off"
                                             src="{!! asset('public/images/choose-image.gif') !!}"
                                        />
                                    </div>

                                </div>
                                <div class="clearfix"></div>
                                <div class="my-row">
                                    <div class="my-cell"></div>
                                    <div class="my-cell">Bật</div>
                                    <div class="my-cell">Tắt</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <div class="modal-footer">
                <div class="col-xs-6">
                    <button class="btn orange pull-left close-modal">Hủy</button>
                </div>
                <div class="col-xs-6">
                    <button class="btn orange save-equipment-update">Lưu</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function (e) {
        $(".showonhover").click(function () {
            $("#selectfile").trigger('click');
        });
        $("#upfile-image-icon").click(function () {
            $("#file-image-icon").trigger('click');
        });
        $("#upfile-image-on").click(function () {
            $("#file-image-on").trigger('click');
        });
        $("#upfile-image-off").click(function () {
            $("#file-image-off").trigger('click');
        });
    });

</script>
<script type="text/javascript">
    function clickToRenderImage(obj, _target) {
        $(obj).change(function () {
            readURL(this);

            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $(_target).attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
        });
    }

    clickToRenderImage('#file-image-icon', '#upfile-image-icon');
    clickToRenderImage('#file-image-on', '#upfile-image-on');
    clickToRenderImage('#file-image-off', '#upfile-image-off');
</script>
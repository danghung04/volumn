<?php
$dialog_width = isset($form_width) ? $form_width . 'px' : '480px';
$label_portion = isset($label_width) ? $label_width : 5;
$input_portion = 12 - $label_portion;
$label_width_class = 'col-md-' . $label_portion;
$input_width_class = 'col-md-' . $input_portion;
?>
<div class="modal fade" id="modal-add-equipment">
    <div class="modal-dialog" style="width: {!! $dialog_width !!}">
        <div class="modal-content">
            <div class="modal-header">
                <div class="my-close" data-dismiss="modal" aria-hidden="true"><span
                            class="glyphicon glyphicon-remove-circle pull-right" style="color: #fff"></span></div>
                <h4 class="modal-title text-center" style="color: #fff;text-transform: uppercase;font-size: 2em;
            font-weight: bold;">Thêm loại thiết bị</h4>
            </div>
            <div class="my-divider" style="height: 20px;width: 100%;padding: 5px;">
                <div class="my-bar" style="margin:auto; width: 90%; background: #666; height: 1px;">
                </div>
            </div>
            <form class="form-horizontal" id="modal-add-equipment-form"
                  action="{!! route('admin.equipment-type.store') !!}"
                  method="POST" enctype="multipart/form-data">
                {!! csrf_field() !!}
                <div class="form-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="form-group" style="margin-bottom: 0">
                        <label class="{!! $label_width_class !!} control-label my-label"
                               style="padding-left: 40px; padding-right: 0">Tên loại thiết bị</label>
                        <div class="{!! $input_width_class !!}">
                            <input type="text" name="name"
                                   class="form-control" value="{!! old('name') !!}"
                                   required>
                            <span class="my-error"></span>
                        </div>
                    </div>
                    <div class="my-divider" style="height: 20px;width: 100%;padding: 0 5px 5px 0;">
                        <div class="my-bar" style="margin:auto; width: 80%; background: #666; height: 1px;">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-3" style="padding-left: 40px; padding-right: 5px">
                            <label class="control-label" style="padding-right: 0">Icon
                            </label>
                            <div class="my-divider" style="height: 20px;width: 100%;padding: 0 5px 5px 0;">
                                <div class="my-bar" style="background: #666; height: 1px;">
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-9">
                            <div class="my-table">
                                <div class="my-row image-group">
                                    <div class="my-cell">
                                        <input accept="image/*" capture="" id="file-image-icon-add" name="icon"
                                               style="display:none" type="file"/>
                                        <img id="upfile-image-icon-add"
                                             src="{!! asset('public/images/choose-image.gif') !!}"
                                        />
                                    </div>
                                    <div class="my-cell middle-icon-cell">
                                        <div class="border-main border-main-left">
                                        </div>
                                        <div class="middle-main">
                                            <input accept="image/*" capture="" id="file-image-on-add" name="image"
                                                   style="display:none" type="file"/>
                                            <img id="upfile-image-on-add"
                                                 src="{!! asset('public/images/choose-image.gif') !!}"
                                            />
                                        </div>
                                        <div class="border-main border-main-right">
                                        </div>
                                    </div>

                                    <div class="my-cell">
                                        <input accept="image/*" capture="" id="file-image-off-add" name="image_off"
                                               style="display:none" type="file"/>
                                        <img id="upfile-image-off-add"
                                             src="{!! asset('public/images/choose-image.gif') !!}"
                                        />
                                    </div>

                                </div>
                                <div class="clearfix"></div>
                                <div class="my-row">
                                    <div class="my-cell"></div>
                                    <div class="my-cell">Bật</div>
                                    <div class="my-cell">Tắt</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <div class="modal-footer">
                <div class="col-xs-6">
                    <button class="btn orange pull-left close-modal">Hủy</button>
                </div>
                <div class="col-xs-6">
                    <button class="btn orange save-add-equipment">Lưu</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function (e) {
        $(".showonhover").click(function () {
            $("#selectfile-add").trigger('click');
        });
        $("#upfile-image-icon-add").click(function () {
            $("#file-image-icon-add").trigger('click');
        });
        $("#upfile-image-on-add").click(function () {
            $("#file-image-on-add").trigger('click');
        });
        $("#upfile-image-off-add").click(function () {
            $("#file-image-off-add").trigger('click');
        });
    });

</script>
<script type="text/javascript">
    function clickToRenderImage(obj, _target) {
        $(obj).change(function () {
            readURL(this);

            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $(_target).attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
        });
    }

    clickToRenderImage('#file-image-icon-add', '#upfile-image-icon-add');
    clickToRenderImage('#file-image-on-add', '#upfile-image-on-add');
    clickToRenderImage('#file-image-off-add', '#upfile-image-off-add');
</script>
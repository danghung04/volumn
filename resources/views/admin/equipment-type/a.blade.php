@extends('admin.index')
@section('custom css')
 <style>
	.portlet-body form{
		padding-left:18px;
	}
	label.error {
		  color: #a94442;
		  border-color: #ebccd1;
		  padding:1px 20px 1px 20px;
		}
</style>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/css/bootstrap-theme.min.css">
<link href="{{ asset('public/web/css/plugins/bootstrap-tagsinput.css') }}" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/rainbow/1.2.0/themes/github.css">
<link href="{{ asset('public/web/css/plugins/app.css') }}" rel="stylesheet" type="text/css">
 <link rel="stylesheet" href="{{ asset('public/plugin/sweet-alert/dist/sweetalert2.min.css') }}">
@stop
@section('custom script')

<script src="https://cdn.jsdelivr.net/jquery.validation/1.15.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.15.0/additional-methods.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.2.20/angular.min.js"></script>
<script src="{{ asset('public/web/js/plugin/bootstrap-tagsinput.js') }}"></script>
<script src="{{ asset('public/web/js/plugin/bootstrap-tagsinput-angular.min.js') }}"></script>
<script src="{{ asset('public/web/js/plugin/app.js') }} "></script>
<script src="{{ asset('public/plugin/bootstrap-filestyle/src/bootstrap-filestyle.js') }}"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
<script src="{{ asset('public/plugin/sweet-alert/dist/sweetalert2.min.js') }}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
<script src="{{ asset('public/js/sweet_admin.js') }}"></script>
<script>

	$(function () {

    $('#create-form').validate({
        rules: {
            version_name: {
                required: true,
            },
            photo: {
            	required:true,
                extension: "jpg|jpeg|png|gif"
            },
            keyword:{
            	required:true,
            },
            link_exe:{
            	required:true,
            	url:true
            },
            link_android:{
            	required:true,
            	url:true
            },
            link_ios:{
            	required:true,
            	url:true
            },
            video:{
            	required:true,
            	url:true
            },
            description:{
            	required:true,
            },

        }       
    });

    $('#update-form').validate({
        rules: {
            version_name: {
                required: true,
            },
            photo: {
                extension: "jpg|jpeg|png|gif"
            },
            keyword:{
            	required:true,
            },
            link_exe:{
            	required:true,
            	url:true
            },
            link_android:{
            	required:true,
            	url:true
            },
            link_ios:{
            	required:true,
            	url:true
            },
            video:{
            	required:true,
            	url:true
            },
            description:{
            	required:true,
            },

        }       
    });
});
</script>

<script type="text/javascript">
function clickToRenderImage(obj, _target){
	$(obj).change(function () {
	    readURL(this);
	    function readURL(input) {
	        if (input.files && input.files[0]) {
	            var reader = new FileReader();
	            reader.onload = function (e) {
	            $(_target).attr('src', e.target.result);	            
	        }
	        reader.readAsDataURL(input.files[0]);
	        $(_target).css('height','150px');
	     }
	    }
	});
}
	clickToRenderImage('#photo','#img');
	clickToRenderImage('#photo-update','#img-update');
</script>

<script type="text/javascript">
	$('#photo').filestyle({
		buttonName : 'btn blue',
        buttonText : ' Select a File'
	});
	$('#photo-update').filestyle({
		buttonName : 'btn blue',
        buttonText : ' Select a File'
	}) 
</script>
<script>
$(document).ready(function(){
	//create new version
	var tbody  = $('#sample_editable_1 > tbody:last-child');
	$('#create-form').submit(function(e){
		e.preventDefault();
		$('.modal').modal('hide');
		$('#cssload-contain').show();
		var formData = new FormData($('#create-form')[0]);
		$.ajax({
			type: 'POST',
			cache: false,
	        contentType: false,
	        processData: false,
			url: "{{ route('admin.version.postCreate') }}",
			data: formData
		}).done(function(data){

		    var result = JSON.parse(data);
		    var title = ['Error','Success'];
		    var type = ['error','success'];
	    	swal({
				title:title[result.state],
				text: result.msg,
				type: type[result.state],
				timer: 2000
			}).catch(swal.noop);
			if(result.state == 1){
				//reset form
				$('#create-form')[0].reset();
				$('#img').remove();
				$('#box-img-1').html('<img id="img" src="" />');

				//add new row in table
				tbody.prepend(result.data);
				$('#'+result.create_id).find('.update-version').click(function(){
					updateVersion(this);
				});

				$('#'+result.create_id).find('.delete-version').click(function(){
					deleteVersion(this);
				});

			}
		}).fail(function(data){
			var text = "Request not allowed or Server side error";
			swal({
				title:"Error",
				text: text,
				type: "error",
				timer: 5000
			}).catch(swal.noop);
		}).always(function(){
			$('#cssload-contain').hide();
		});
	});
	// end create new version

	//delete version
	function deleteVersion(object){
		var row_obj = $(object).parent().parent();
			var id = row_obj.attr('id');
		swal({
		  title: 'Are you sure?',
		  text: "You won't be able to revert this!",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes, delete it!'
		}).then(function () {
			$('#cssload-contain').show();
			$.ajax({
				type: 'POST',
				cache: false,
				url: "{{ route('admin.version.delete') }}",
				data: {
					id: id
				}
			}).done(function(data){
			    var result = JSON.parse(data);
			    var title = ['Error','Success'];
			    var type = ['error','success'];
		    	swal({
					title:title[result.state],
					text: result.msg,
					type: type[result.state],
					timer: 2000
				}).then(function(){
					if(result.state != 0){
						$(row_obj).remove();

					}
				});
			}).fail(function(data){
				var text = "Request not allowed or Server side error";
				swal({
					title:"Error",
					text: text,
					type: "error",
					timer: 5000
				})
			}).always(function(){
				$('#cssload-contain').hide();
			});;
		},function(){
			//cancel action
		});
	}
	$('.delete-version').click(function(){
		deleteVersion(this);
	});
	//end delete version

	// begin update version
	var update_id = 0;
	var index = 0;
	function updateVersion(object){
		$('#update-modal').modal('show');
		var row_obj = $(object).parent().parent();
		var arr = $(row_obj).find('td');

		var sl = $(arr[0]).text();
		index = sl;
		var id= row_obj.attr('id');
		update_id = id;
		var version_name = $(arr[1]).text();
		var org_img = $(arr[2]).html();
		var clone_img = $(org_img).clone();
		var keyword = $(arr[3]).text();
		var link_exe = $(arr[4]).text();
		var link_android = $(arr[5]).text();
		var link_ios = $(arr[6]).text();
		var video = $(arr[7]).text();
		var description = $(arr[8]).text();

		var form_obj = $('#update-form');
		$(form_obj).find('input[name="id"]').val(id);
		$(form_obj).find('input[name="version_name"]').val(version_name);
		$(form_obj).find('input[name="keyword"]').val(keyword);
		$(form_obj).find('input[name="link_exe"]').val(link_exe);
		$(form_obj).find('input[name="link_android"]').val(link_android);
		$(form_obj).find('input[name="link_ios"]').val(link_ios);
		$(form_obj).find('input[name="video"]').val(video);
		$(form_obj).find('textarea[name="description"]').val(description);
		$('#box-img-2').html(clone_img);
		$('#box-img-2').find('img').attr('id','img-update');
		$('#img-update').css('height','150px');
	}
	$('.update-version').click(function(){
		updateVersion(this);
	});

	$('#update-form').submit(function(e){
		e.preventDefault();
		$('.modal').modal('hide');
		$('#cssload-contain').show();
		var formData = new FormData($('#update-form')[0]);
		$.ajax({
			type: 'POST',
			cache: false,
	        contentType: false,
	        processData: false,
			url: "{{ route('admin.version.postUpdate') }}",
			data: formData
		}).done(function(data){
		    var result = JSON.parse(data);
		    var title = ['Error','Success'];
		    var type = ['error','success'];
	    	swal({
				title:title[result.state],
				text: result.msg,
				type: type[result.state],
				timer: 2000
			}).catch(swal.noop);
			if(result.state == 1){
				//reset form
				$('#update-form')[0].reset();
				$('#img-update').remove();
				$('#box-img-2').html('<img id="img-update" src="" />');

				//add new row in table
				$('#'+update_id).html(result.data);
				var i_obj = $('#'+update_id).find('td')[0];
				$(i_obj).text(index);
				$('#'+update_id).find('.update-version').click(function(){
					updateVersion(this);
				});

				$('#'+update_id).find('.delete-version').click(function(){
					deleteVersion(this);
				});

			}
		}).fail(function(data){
			var text = "Request not allowed or Server side error";
			swal({
				title:"Error",
				text: text,
				type: "error",
				timer: 5000
			}).catch(swal.noop);
		}).always(function(){
			$('#cssload-contain').hide();
		});
	});
	// end update version

});
	
</script>
@stop

@section('content')

<div class="modal fade" id="add-version">
	<div class="modal-dialog">
		<div class="modal-content">
			<form class="form-horizontal" id="create-form" method="POST" role="form" enctype="multipart/form-data">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Add version</h4>
				</div>
				<div class="modal-body">
					{!! csrf_field() !!}
					<input type="hidden" name="software_id" value="{{ $software->id }}" />
					<div class="form-group">
						<label class="col-md-3 control-label margin-bottom-14" for="version_name">Version</label>
						<div class="col-md-9 margin-bottom-14">
							<input type="text" class="form-control" value="" name="version_name" required>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-3 control-label margin-bottom-14" for="keyword">Keyword</label>
						<div class="col-md-9 margin-bottom-14">
							<input type="text" class="form-control" value="" name="keyword" required>
						</div>
					</div>

					<div class="form-group">
						<div class="col-md-3"></div>
						<div class=" col-md-9" id="box-img-1"  style="max-width: 300px;overflow:hidden;">
							<img id="img" src="">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label margin-bottom-14" for="photo">Image</label>
						<div class="col-md-9 margin-bottom-14">
							<input type="file" id="photo" class="form-control" value="" name="photo" required>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-3 control-label margin-bottom-14" for="link_exe">Link Exe</label>
						<div class="col-md-9 margin-bottom-14">
							<input type="text" class="form-control" value="" name="link_exe" required>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-3 control-label margin-bottom-14" for="link_android">Link Android</label>
						<div class="col-md-9 margin-bottom-14">
							<input type="text" class="form-control" value="" name="link_android" required>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-3 control-label margin-bottom-14" for="link_ios">Link IOS</label>
						<div class="col-md-9 margin-bottom-14">
							<input type="text" class="form-control" value="" name="link_ios" required>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-3 control-label margin-bottom-14" for="video">Video</label>
						<div class="col-md-9 margin-bottom-14">
							<input type="text" class="form-control" value="" name="video" required>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-3 control-label margin-bottom-14" for="description">Description</label>
						<div class="col-md-9 margin-bottom-14">
							<textarea class="form-control" name="description" id="" rows="3"></textarea>
						</div>
					</div>
					
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="submit" class="btn blue">Create</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div class="modal fade" id="update-modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<form class="form-horizontal" id="update-form" method="POST" role="form" enctype="multipart/form-data">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Update version</h4>
				</div>
				<div class="modal-body">
					{!! csrf_field() !!}
					<input type="hidden" name="id">
					<input type="hidden" name="software_id" value="{{ $software->id }}" />
					<div class="form-group">
						<label class="col-md-3 control-label margin-bottom-14" for="version_name">Version</label>
						<div class="col-md-9 margin-bottom-14">
							<input type="text" class="form-control" value="" name="version_name" required>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-3 control-label margin-bottom-14" for="keyword">Keyword</label>
						<div class="col-md-9 margin-bottom-14">
							<input type="text" class="form-control" value="" name="keyword" required>
						</div>
					</div>

					<div class="form-group">
						<div class="col-md-3"></div>
						<div class=" col-md-9" id="box-img-2"  style="max-width: 300px;overflow:hidden;">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label margin-bottom-14" for="photo">Image</label>
						<div class="col-md-9 margin-bottom-14">
							<input type="file" id="photo-update" class="form-control" value="" name="photo">
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-3 control-label margin-bottom-14" for="link_exe">Link Exe</label>
						<div class="col-md-9 margin-bottom-14">
							<input type="text" class="form-control" value="" name="link_exe" required>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-3 control-label margin-bottom-14" for="link_android">Link Android</label>
						<div class="col-md-9 margin-bottom-14">
							<input type="text" class="form-control" value="" name="link_android" required>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-3 control-label margin-bottom-14" for="link_ios">Link IOS</label>
						<div class="col-md-9 margin-bottom-14">
							<input type="text" class="form-control" value="" name="link_ios" required>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-3 control-label margin-bottom-14" for="video">Video</label>
						<div class="col-md-9 margin-bottom-14">
							<input type="text" class="form-control" value="" name="video" required>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-3 control-label margin-bottom-14" for="description">Description</label>
						<div class="col-md-9 margin-bottom-14">
							<textarea class="form-control" name="description" id="" rows="3"></textarea>
						</div>
					</div>
					
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="submit" class="btn blue">Update</button>
				</div>
			</form>
		</div>
	</div>
</div>

<h3 class="page-title">
Software Manage
</h3>
<div class="row">
	<div class="col-md-12">
		<div class="portlet box blue">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-globe"></i>Update Software
				</div>
			</div>
			<div class="portlet-body form">

				@include('component.loading')
				<form class="form-horizontal" id="create-form" action="{{ route('admin.software.postUpdate',['id'=>$software->id]) }}" method="POST" enctype="multipart/form-data">
				{!! csrf_field() !!}
					<div class="form-body">						
						<div class="form-group">
							<label class="col-md-2 control-label">Name</label>
							<div class="col-md-10">
								<input type="text" name="name" class="form-control" value="{{ $software->name }}" required>								
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-md-2 control-label">Keywords</label>
							<div class="col-md-10">
								<input name="keywords" class="form-control" required="" value="{{ $software->keywords }}">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label">Description</label>
							<div class="col-md-10">
								<textarea name="description" class="form-control" required="">{{ $software->description }}</textarea>
							</div>
						</div>
						{{--<div class="form-group">
							<label class="col-md-2 control-label">Photo</label>
							<div class="col-md-10">
								<input type="file" name="photo" class="form-control" id="photo">
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-2"></div>
							<div class=" col-md-10"  style="max-width: 300px;overflow:hidden;">
								@if(!empty($software->photo))
									@if(@getimagesize(asset(Config::get('constants.image').$software->photo)))
                                       <img style="height: 100px" id="img"  src="{{asset(Config::get('constants.image').$software->photo) }}">
                                     @else
                                       <img style="height: 100px" id="img"  src="{{asset(Config::get('constants.no_image')) }}">
                                     @endif
								
								@else
									<img style="height: 100px" id="img"  src="{{asset(Config::get('constants.no_image')) }}">
								@endif
							</div>
						</div> --}}
						<div class="form-group">
							<label class="col-md-2 control-label">Lisence</label>
							<div class="col-md-10">
								<input type="text" name="lisence" class="form-control" value="{{ $software->lisence }}" required="">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label">Sub Category</label>
							<div class="col-md-10">
								<select name="subcategory_id" class="form-control">
									@foreach ($subcategory as $el)
										<option value="{{ $el->id }}" @if($software->subcategory_id == $el->id) selected="" @endif>{{ $el->name }}</option>
									@endforeach
								</select>
							</div>
						</div>	
						<div class="form-group">
							<label class="col-md-2 control-label">Developer</label>
							<div class="col-md-10">
								<input type="text" name="developer" class="form-control" value="{{ $software->developer }}" required>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label">rank</label>
							<div class="col-md-10">
								<input type="number" name="rank" class="form-control" value="{{ $software->rank }}" required>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label">Review</label>
							<div class="col-md-10">
								<textarea name="review" class="form-control" required>{{ $software->review }}</textarea>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-2">Hash tags </label>
							<div class="col-md-10">
								<input type="text" value="{{ $tag }}" data-role="tagsinput" name="tag" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label">Status</label>
							<div class="col-md-10">
								<select name="status" class="form-control">
									<option value="0">Pending</option>
									<option value="1">Success</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label">Review</label>
							<div class="col-md-10">
								<textarea name="review" class="form-control ckeditor" required="">{{ $software->review }}</textarea>
							</div>
						</div>				
						<div class="form-group">
							<div class="col-md-3 col-md-offset-2">
								<button type="submit" class="btn blue">Update</button>
							</div>
						</div>
					</div>	
				</form>
			</div>
		</div>	
	</div>
	<div class="col-md-12">
		<div class="portlet box green">
			<div class="portlet-title">
				<div class="caption">
					Version Manage
				</div>
				<div class="tools">
					<a onclick="location.reload()" class="reload"></a>
				</div>
			</div>
			<div class="portlet-body">
				<div class="table-toolbar">
					<div class="row">
						<div class="col-md-12">
							<div class="btn-group pull-right">
								<a href="#add-version" data-toggle="modal" class="btn blue">
									Create new version<i class="fa fa-plus"></i>
								</a>
							</div>
						</div>
					</div>
				</div>				
				<table class="table table-striped table-hover table-bordered" id="sample_editable_1" style="">
					<thead>
						<tr>
							<th>SL</th>
							<th>Name</th>
							<th>Photo</th>
							<th>Keyword</th>
							<th>Link Exe</th>
							<th>Link Android</th>
							<th>Link IOS</th>
							<th>Video</th>
							<th>Description</th>
							<th>Photo</th>
							<th>Update</th>
							<th>Delete</th>
						</tr>
					</thead>
					<tbody>
						@foreach($versions as $key => $val)
						<tr id="{{ $val->id }}">
							<td>@if(request()->get('page') != null && request()->get('page') > 1){{ request()->get('page') -1 }}@endif{{ $key + 1 }}</td>
							<td>{{ $val->version_name }}</td>
							<td>
								@if($val->photo == null)
									<img style="height: 30px" src="{{ asset(Config::get('constants.no_image')) }}">
								@else
									@if(@getimagesize(asset(Config::get('constants.image').$val->photo)))
										<img style="height: 30px"  src="{{ asset(Config::get('constants.image').$val->photo) }}">
									@else
										<img style="height: 30px"  src="{{ asset(Config::get('constants.no_image')) }}">
									@endif
								@endif
							</td>
							<td>{{ $val->keyword }}</td>
							<td>{{ $val->link_exe }}</td>
							<td>{{ $val->link_android }}</td>
							<td>{{ $val->link_ios }}</td>
							<td>{{ $val->video }}</td>
							<td>{{ $val->description }}</td>
							<td>
								<a href="{{ route('admin.version.photo',['id'=>$val->id]) }}" class="btn green"><i class="fa fa-dashboard li-btn"></i>Photo</a>
							</td>
							<td>
								<a href="#" class="btn blue update-version"><i class="fa fa-pencil li-btn"></i>Update</a>
							</td>
							<td>
								<a href="#" class="btn btn-danger delete-version"><i class="fa fa-trash-o li-btn"></i>Delete</a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			<div class="table-foooter" style="background-color: #ffffff">
				{!! $versions->appends(['id'=>$software->id])->render() !!}
			</div>
		</div>
	</div>
</div>
@stop
<script>
    $(function () {
        var modal_add = '#modal-add-floor';
        var urlCheckDuplicate = '{!! route('admin.room.checkDuplicate') !!}';
        $('.close-modal-add-floor').on('click', function () {
            $(modal_add).modal('hide');
        });
        $('.save-add-floor').on('click', function () {
            var name = $(modal_add + ' input[name="name"]').val();
            var data = {name: name};
            checkErrorAndSubmitForm(modal_add, urlCheckDuplicate, data);
        });

        function checkErrorAndSubmitForm(modal, url, data) {
            if (data.name === '') {
                $(modal + ' .my-error').text('Tên không được để trống');
                return false;
            }
            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'json',
                data: data
            }).done(response => {
                if (response.state === 0) {
                    $(modal + '-form').submit();
                } else {
                    $(modal + ' .my-error').text(response.msg);
                }
            })
        }

        var modal_update = '#modal-setting-floor';
        var name_origin = $(modal_update + ' input[name="name"]').val();
        $('.close-modal-floor').on('click', function () {
            $(modal_update).modal('hide');
        });
        $('.save-floor').on('click', function () {
            var name = $(modal_update + ' input[name="name"]').val();
            var id = $(modal_update + ' input[name="id"]').val();
            var data = {name: name, id: id};
            checkErrorAndSubmitForm(modal_update, urlCheckDuplicate, data);
        });
        $('.setting-floor-form-btn').on('click', function(){
            $('.my-error').text('')
            $(modal_update + ' input[name="name"]').val(name_origin);
            $('#modal-setting-floor').modal('show');
        });
        $('.add-floor-form-btn').on('click', function(){
            $('.my-error').text('')
            $('#modal-add-floor').modal('show');
        });
    });
</script>
<style>

    .modal-content {
        padding: 5px;
        color: #fff;
    }

    .form-group > label {
        color: #fff;
    }

    .orange {
        background: #FFBA00;
    }

    .orange:hover {
        background: #da8c00;
    }

    .modal-footer {
        color: #000;
    }

    .my-close {
        cursor: pointer;
    }
    .image-area{
        overflow: hidden;
    }
    #upfile-image {
        font-size: 20px;
        height: 120px;
        max-width: 150px;
    }

    .imagediv {
        float: left;
        margin-top: 50px;
    }

    .imagediv .showonhover {
        background: red;
        padding: 20px;
        opacity: 0.9;
        color: white;
        width: 100%;
        display: block;
        text-align: center;
        cursor: pointer;
    }
</style>
<?php
$dialog_width = isset($form_width) ? $form_width . 'px' : '480px';
$label_portion = isset($label_width) ? $label_width : 4;
$input_portion = 12 - $label_portion;
$label_width_class = 'col-md-' . $label_portion;
$input_width_class = 'col-md-' . $input_portion;
?>
<div class="modal fade" id="modal-setting-floor">
    <div class="modal-dialog" style="width: {!! $dialog_width !!}">
        <div class="modal-content">
            <div class="modal-header">
                <div class="my-close" data-dismiss="modal" aria-hidden="true"><span
                            class="glyphicon glyphicon-remove-circle pull-right" style="color: #fff"></span></div>
                <h4 class="modal-title text-center" style="color: #fff;text-transform: uppercase;font-size: 2em;
            font-weight: bold;">{!! $form_title !!}</h4>
            </div>
            <div class="my-divider" style="height: 20px;width: 100%;padding: 5px 5px 5px 0;">
                <div class="my-bar" style="margin:auto; width: 90%; background: #666; height: 2px;">
                </div>
            </div>
            <form class="form-horizontal" id="modal-setting-floor-form" action="{!! $action !!}"
                  method="POST" enctype="multipart/form-data">
                {!! csrf_field() !!}
                <input type="hidden" name="id" value="{!! $room_active->id !!}">
                <div class="form-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="form-group" style="margin-bottom: 0px">
                        <label class="{!! $label_width_class !!} control-label my-label" style="padding-left: 45px">Tên tầng</label>
                        <div class="{!! $input_width_class !!}">
                            <input type="text" name="name"
                                   class="form-control my-input" style="width: 88%;" value="{!! $room_active->name !!}"
                                   required>
                            <span class="my-error"></span>
                        </div>
                    </div>
                        <div class="my-divider" style="height: 20px;width: 100%;padding: 0 5px 5px 0;">
                            <div class="my-bar" style="margin:auto; width: 80%; background: #666; height: 1px;">
                            </div>
                        </div>
                    <div class="form-group" style="margin-bottom: 0px">
                        <div class="col-xs-4" style="padding-left: 40px; padding-right: 5px">
                            <label class="control-label" style="padding-right: 0">Ảnh mặt cắt
                            </label>
                            <div class="my-divider" style="height: 20px;width: 100%;padding: 0 5px 5px 0;">
                                <div class="my-bar" style="background: #666; height: 1px;">
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-8 image-area" style="overflow:hidden;">
                            <input accept="image/*" capture="" id="file-image" name="image" style="display:none"
                                   type="file"/>
                            @if(!empty($room_active->image))
                                @if(@getimagesize(asset(config('constants.image').$room_active->image)))
                                    <img id="upfile-image" style="cursor: pointer"
                                         src="{{ asset(config('constants.image').$room_active->image) }}">
                                @else
                                    <img id="upfile-image" style="background:#fffcfb; cursor: pointer"
                                         src="{{ asset(config('constants.choose_image')) }}">
                                @endif
                            @else
                                <img id="upfile-image" style="background:#fffcfb; cursor: pointer"
                                     src="{{ asset(config('constants.choose_image')) }}">
                            @endif
                        </div>
                    </div>
                </div>
            </form>
            <div class="modal-footer">
                <div class="col-xs-6">
                    <button class="btn orange pull-left close-modal-floor">Hủy</button>
                </div>
                <div class="col-xs-6">
                    <button class="btn orange save-floor">Lưu</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function (e) {
        $(".showonhover").click(function () {
            $("#selectfile").trigger('click');
        });
        $("#upfile-image").click(function () {
            $("#file-image").trigger('click');
        });
    });

</script>
<script type="text/javascript">
    function clickToRenderImage(obj, _target) {
        $(obj).change(function () {
            readURL(this);

            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $(_target).attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
        });
    }

    clickToRenderImage('#file-image', '#upfile-image');
</script>
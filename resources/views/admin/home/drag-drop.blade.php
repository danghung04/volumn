{{--Thêm kéo thả cho các icon--}}
<script src="{!! asset('public/plugin/jquery-ui/jquery-ui.min.js') !!}"></script>
{{--Xử lý kéo thả cho icon thiết bị--}}
<script>
    $(function () {
        var countIot = parseInt('{!! count($iots) !!}');
        var emptyIot = countIot === 0?true: false;
        var ObjectInRoom = {
            data: {},
            urlUpdatePosition: '',
            urlDelete: '',
            modalCreateId: '',
            showCreateForm: function () {
                $('.my-error').text('');
                $('#' + this.modalCreateId).modal('show');
            },
            updatePosition: function () {
                var url = this.urlUpdatePosition;
                var data = this.data;
                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    url: url,
                    data: data
                }).done(function (response) {

                });
            },
            deleteObject: function () {
                var url = this.urlDelete;
                var data = this.data;
                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    url: url,
                    data: data
                }).done(function (response) {

                });
            }
        };


        var equipmentRoom = Object.create(ObjectInRoom);
        equipmentRoom.modalCreateId = '{!! $e_create_modal_id !!}';
        equipmentRoom.urlUpdatePosition = '{!! route('admin.equipment.updatePosition') !!}';
        equipmentRoom.urlDelete = '{!! route('admin.equipment.delete') !!}';
        var counter = 0;
        var offset = null;
        $('#content-scroller').on('hover','.icon-equip', function () {
            $('.icon-drag-temp').remove();
            $(this).css('z-index', '-1');
            var wrapperScroll = $(this).parent();
            var cloneObject = $(this).clone();
            $(cloneObject).addClass('icon-drag');
            $(cloneObject).addClass('icon-drag-temp');
            $(cloneObject).removeClass('icon-equip');
            $(cloneObject).css('position', 'absolute');
            var imgWidth = $(this).find("img").width() * 50/40;
            var totalWidth = $(this).width();
            var offsetLeft = (totalWidth - imgWidth)/2;
            var icon_choose_wrapper = $('.icon-choose-wrapper');
            var left = wrapperScroll.offset().left - icon_choose_wrapper.offset().left + offsetLeft + 5;
            var top = wrapperScroll.offset().top -icon_choose_wrapper.offset().top;
            cloneObject.css('top', top + 'px');
            cloneObject.css('left', left + 'px');
            cloneObject.css('z-index', '6');
            icon_choose_wrapper.append(cloneObject);
            addDraggableListener($(cloneObject));

        });
        //Make element draggable
        function addDraggableListener(object){
            $(object).draggable({
                //When first dragged
                stop: function (ev, ui) {
                    var pos = $(ui.helper).offset();
                    var img = $('.background-wrapper>img');
                    var bound = img.offset();
                    var imgWidth = $(this).width();
                    var boundWidth = img.width();
                    var boundHeight = img.height();
                    var objName = "#clonediv" + counter;
                    offset = {
                        x: pos.left - bound.left - imgWidth/2*4/5 - 138,
                        y: pos.top - bound.top + 45
                    };
                    if (offset.x <= 0 || offset.y <= 0 || offset.x >= boundWidth - 30 || offset.y >= boundHeight - 30) {
                        $(objName).remove();
                        return;
                    }
                    $(objName).css({"left": offset.x, "top": offset.y});
                    $(objName).removeClass("drag");
                    $(objName).draggable({
                        stop: function (ev, ui) {

                        }
                    });
                    var equipment_type_id = $(this).attr('equipment-type-id');
                    var room_id = '{!! !empty($room_active) ?$room_active->id:'' !!}';
                    var form = $('#' + equipmentRoom.modalCreateId + ' form');
                    var data = {
                        x_offset: offset.x,
                        y_offset: offset.y,
                        equipment_type_id: equipment_type_id,
                        room_id: room_id
                    };
                    for (var key in data) {
                        form.find('input[name="' + key + '"]').val(data[key]);
                    }
                }
            });
        }


        $('.icon-drag-update').draggable({
            stop: function (ev, ui) {
                var pos = $(ui.helper).offset();
                var img = $('.background-wrapper>img');
                var bound = img.offset();
                var boundWidth = img.width();
                var boundHeight = img.height();
                offset = {
                    x: pos.left - bound.left,
                    y: pos.top - bound.top
                };
                var equipment_id = $(this).attr('equipment-id');
                var _token = '{!! csrf_token() !!}';
                if (offset.x <= 0 || offset.y <= 0 || offset.x >= boundWidth - 30 || offset.y >= boundHeight - 30) {
                    var data = {
                        id      : equipment_id,
                        _token  : _token
                    };
                    equipmentRoom.data = data;
                    equipmentRoom.deleteObject();
                    $(this).remove();
                    return;
                }
                var data = {
                    _token: _token,
                    x_offset: offset.x,
                    y_offset: offset.y,
                    id: equipment_id
                }
                equipmentRoom.data = data;
                equipmentRoom.updatePosition();


            }
        });
        //Chứa những phần tử được kéo vào
        $("#frame").droppable({
            drop: function (ev, ui) {
                var element = $(ui.draggable);
                //Nếu không phải là icon đã có trong phòng
                if (!element.hasClass('icon-drag-update') && element.hasClass('icon-drag')) {
                    counter++;
                    //Copy phần tử ui đang bị drag
                    var src_image = element.find('img').attr('image');
                    element.find('img').attr('src', src_image);
                    element.addClass("tempclass");
                    element.css('position', 'absolute');
                    $(this).append(element);	//Thêm đối tượng vào khung chứa

                    var idObjectNew = "clonediv" + counter;
                    $(".tempclass").attr("id", idObjectNew);
                    $("#" + idObjectNew).removeClass("tempclass");

                    element.addClass('icon-drag-update');
                    element.removeClass('icon-drag');
                    if(emptyIot){
                        alert('Không tồn tại thiết bị iot khả dụng');
                        element.remove();
                        return false;
                    }
                    equipmentRoom.showCreateForm();	//Tạo dữ liệu trên db
                }
            }
        });
        var e_modal = $('#create-equipment-modal');
        e_modal.on('click', '.my-close', function(){
            var currentEName = "#clonediv" + counter;
            $(currentEName).remove();
        });

        e_modal.on('click', '.close-modalcreate-equipment-modal', function(){
            var currentEName = "#clonediv" + counter;
            $(currentEName).remove();
        });



        var subRoom = Object.create(ObjectInRoom);
        subRoom.modalCreateId = '{!! $r_named_modal_id !!}';
        subRoom.urlUpdatePosition = '{!! route('admin.sub-room.updatePosition') !!}';
        var counter_room = 0;
        var offsetRoom = null;
        //Make element draggable
        $(".text-room-drag").draggable({
            helper: 'clone',//Cho phép một phần tử helper được sử dụng để hiển thị drag

            //When first dragged
            stop: function (ev, ui) {
                var pos = $(ui.helper).offset();
                var bound = $('.background-wrapper>img').offset();
                var objName = "#clonedivRoom" + counter_room;
                offsetRoom = {
                    x: pos.left - bound.left,
                    y: pos.top - bound.top
                };
                var sub_room_id = 0;
                $(objName).css({"left": offsetRoom.x, "top": offsetRoom.y});
                $(objName).removeClass("drag");
                $(objName).draggable({
                    stop: function (ev, ui) {

                    }
                });
                var room_id = '{!! !empty($room_active)?$room_active->id:'' !!}';
                var form = $('#' + subRoom.modalCreateId + ' form');
                var data = {
                    x_offset: offsetRoom.x,
                    y_offset: offsetRoom.y,
                    room_id: room_id
                };
                for (var key in data) {
                    form.find('input[name="' + key + '"]').val(data[key]);
                }
            }
        });

        $('.text-room-drag-update').draggable({
            stop: function (ev, ui) {
                var pos = $(ui.helper).offset();
                var bound = $('.background-wrapper>img').offset();
                offset = {
                    x: pos.left - bound.left,
                    y: pos.top - bound.top
                };
                var sub_room_id = $(this).attr('sub-room-id');
                var _token = '{!! csrf_token() !!}';
                var data = {
                    _token: _token,
                    x_offset: offset.x,
                    y_offset: offset.y,
                    id: sub_room_id
                }
                subRoom.data = data;
                subRoom.updatePosition();
            }
        });
        //Chứa những phần tử được kéo vào
        $(".background-wrapper").droppable({
            drop: function (ev, ui) {
                var element = $(ui.draggable).clone();
                //Nếu không phải là tên đã có trong phòng
                if (element.hasClass('text-room-drag') && !element.hasClass('text-room-drag-update')) {
                    counter_room++;
                    //Copy phần tử ui đang bị drag
                    element.addClass("tempclassRoom");
                    element.css('position', 'absolute');
                    $(this).append(element);	//Thêm đối tượng vào khung chứa

                    var idObjectNew = "clonedivRoom" + counter_room;
                    $(".tempclassRoom").attr("id", idObjectNew);
                    $("#" + idObjectNew).removeClass("tempclassRoom");

                    element.addClass('text-room-drag-update');
                    element.removeClass('text-room-drag');
                    element.removeClass('text-control');
                    subRoom.showCreateForm();
                }
            }
        });

        var r_modal = $('#named-modal');
        r_modal.on('click', '.my-close', function(){
            var currentRName = "#clonedivRoom" + counter_room;
            $(currentRName).remove();
        });

        r_modal.on('click', '.close-modalnamed-modal', function(){
            var currentRName = "#clonedivRoom" + counter_room;
            $(currentRName).remove();
        });
        $(r_modal).find('.savenamed-modal').unbind('click');
        r_modal.on('click', '.savenamed-modal', function(){
            var currentRName = "#clonedivRoom" + counter_room;
            var room_text = $(r_modal).find('input[name="name"]').val();
            if(room_text.length <= 0) {
                alert('Tên phòng không được để trống');
                return false;
            }else{
                $('#named-modal-form').submit();
            }
        });
    });
</script>
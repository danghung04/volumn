<style>

    .modal-content {
        padding: 5px;
        color: #fff;
    }

    .form-group > label {
        color: #fff;
    }

    .orange {
        background: #FFBA00;
    }

    .orange:hover {
        background: #da8c00;
    }

    .modal-footer {
        color: #000;
    }

    .my-close {
        cursor: pointer;
    }
    #file-image {
        font-size: 20px;
        height:50px;
    }
    .imagediv {
        float:left;
        margin-top:50px;
    }
    .imagediv .showonhover {
        background:red;
        padding:20px;
        opacity:0.9;
        color:white;
        width: 100%;
        display:block;
        text-align:center;
        cursor:pointer;
    }
</style>
<?php
$dialog_width = isset($form_width) ? $form_width . 'px' : '400px';
$label_portion = isset($label_width) ? $label_width : 4;
$input_portion = 12 - $label_portion;
$label_width_class = 'col-md-' . $label_portion;
$input_width_class = 'col-md-' . $input_portion;
?>
<div class="modal fade" id="modal-add-floor">
    <div class="modal-dialog" style="width: {!! $dialog_width !!}">
        <div class="modal-content">
            <div class="modal-header">
                <div class="my-close" data-dismiss="modal" aria-hidden="true"><span
                            class="glyphicon glyphicon-remove-circle pull-right" style="color: #fff"></span></div>
                <h4 class="modal-title text-center" style="color: #fff;text-transform: uppercase;font-size: 2em;
            font-weight: bold;">{!! $form_title !!}</h4>
            </div>
            <form class="form-horizontal" id="modal-add-floor-form" action="{!! $action !!}"
                  method="POST" enctype="multipart/form-data">
                {!! csrf_field() !!}
                <div class="form-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="form-group">
                        <label class="{!! $label_width_class !!} control-label my-label">Tên tầng</label>
                        <div class="{!! $input_width_class !!}">
                            <input type="text" name="name"
                                   class="form-control" value="{!! old('name') !!}"
                                   required>
                            <span class="my-error"></span>
                        </div>
                    </div>
                        <div class="form-group">
                            <label class="{!! $label_width_class !!} control-label my-label">Ảnh mặt cắt</label>
                            <div class="{!! $input_width_class !!}" style="height: 150px;overflow:hidden;">
                                <input accept="image/*" capture="" id="file-image-add" name="image" style="display:none" type="file"/>
                                <img id="upfile-image-add" style="height: 150px; background:#fffcfb; cursor: pointer" src="{!! asset('public/images/choose-image.gif') !!}" style="cursor:pointer"/>
                            </div>
                        </div>
                </div>
            </form>
            <div class="modal-footer">
                <div class="col-xs-6">
                    <button class="btn orange pull-left close-modal-add-floor">Hủy</button>
                </div>
                <div class="col-xs-6">
                    <button class="btn orange save-add-floor">Lưu</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(e) {
        $(".showonhover").click(function(){
            $("#selectfile").trigger('click');
        });
        $("#upfile-image-add").click(function () {
            $("#file-image-add").trigger('click');
        });
    });


</script>
<script type="text/javascript">
    function clickToRenderImage(obj, _target){
        $(obj).change(function () {
            readURL(this);
            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $(_target).attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
                    $(_target).css('height','150px');
                }
            }
        });
    }
    clickToRenderImage('#file-image-add','#upfile-image-add');
</script>
@extends('admin.index')
@section('custom css')
 <style>
	.portlet-body form{
		padding-left:18px;
	}
	label.error {
		  color: #a94442;
		  border-color: #ebccd1;
		  padding:1px 20px 1px 20px;
		}
</style>
@stop
@section('custom script')

<script src="{!! asset('theme/global/plugins/jquery.validate.min.js') !!}" type="text/javascript"></script>
<script>

	$(function () {

    $('#create-form').validate({
        rules: {
            name: {
                required: true,
            },
            email: {
            	required: true
            }
        }       
    });
});
</script>

@stop

@section('content')
<h3 class="page-title">
Quản lý người dùng
</h3>
<div class="row">
	<div class="col-md-12">
		<div class="portlet box blue">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-globe"></i>Tạo mới người dùng
				</div>
			</div>
			<div class="portlet-body form">
				<form class="form-horizontal" id="create-form" action="{{ route('admin.user.store') }}" method="POST" >
				{!! csrf_field() !!}
					<div class="form-body">	
						@if ($errors->any())
						    <div class="alert alert-danger">
						        <ul>
						            @foreach ($errors->all() as $error)
						                <li>{{ $error }}</li>
						            @endforeach
						        </ul>
						    </div>
						@endif					
						<div class="form-group">
							<label class="col-md-2 control-label">Tên</label>
							<div class="col-md-10">
								<input type="name" name="name" class="form-control" value="{{ old('name') }}" required>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-2 control-label">Email</label>
							<div class="col-md-10">
								<input type="email" name="email" class="form-control" value="{{ old('email') }}" required>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label">Mật khẩu</label>
							<div class="col-md-10">
								<input type="password" name="password" class="form-control" value="" required>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label">Nhập lại mật khẩu</label>
							<div class="col-md-10">
								<input type="password" class="form-control" name="password_confirmation" required>
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-md-2 control-label">Quyền</label>
							<div class="col-md-10">
								<select name="permission" class="form-control">
									<option value="0">User</option>
									<option value="1">Admin</option>
								</select>
							</div>
						</div>					
						<div class="form-group">
							<div class="col-md-3 col-md-offset-2">
								<button type="submit" class="btn blue">Tạo mới</button>
							</div>
						</div>
					</div>	
				</form>
			</div>
		</div>	
	</div>
</div>
@stop

@extends('admin.index')
@section('custom css')
    <style>
        .portlet.box > .portlet-body {
            background: transparent;
            padding: 0px;
            box-shadow: none;
            width: 100%;
        }

        .portlet-body table {
            width: 100%;
            color: #ccc;
        }

        .portlet-body table tr td, .portlet-body table tr th {
            border-bottom: 1px solid #657293;
            border-right: 1px solid #657293;
            font-size: 1.2em;
        }

        .portlet-body table tr th {
            padding: 20px;
            font-weight: normal;
        }

        .portlet-body table tr td:last-child, .portlet-body table tr th:last-child {
            border-right-width: 0px;
        }

        .portlet-body table tr .admin-name {
            font-weight: bold;
            text-transform: uppercase;
            font-size: 1.7em;
            color: #fff;

        }

        .portlet-body table tr .name {
            padding-left: 5%;
        }

        .portlet-body table tr .email {
            padding-left: 10%;
        }

        .portlet-body table tr th.action {
            text-align: center;
        }

        .portlet-body table tr td {
            height: 50px;
            line-height: 50px;
        }

        .portlet-body table tr td.action {
            text-align: center;
        }

        .portlet-body table tr.add-new td {
            border: none;
            padding-left: 5%;
        }

        .portlet-body table tr.add-new .btn-add {
            font-weight: bold;
            font-size: 2.7em;
            color: #efefef;
            cursor: pointer;
        }

        .portlet-body table tr.add-new .btn-add:hover {
            color: #fff;
        }

        .orange {
            background: #FFBA00;
            color: #000;
        }

        .orange:hover {
            background: #FFAC00;
        }
    </style>
@stop
@section('content')
    <h3 class="page-title">

    </h3>
    <div class="row main-content">
        <div class="portlet box">
            <div class="portlet-body">
                <table>
                    <thead>
                        th*
                    </thead>
                    <tbody>
                        @foreach($users as $key => $val)
                            <tr>
                                <td style="text-align: center;">{{ $key + 1}}</td>
                                <td class="name">{{ $val->name }}</td>
                                <td class="email">{{ $val->email }}</td>
                                <td class="action">
                                    <a class="btn orange update-record" data-toggle="modal" data-target="#modal-update"
                                       field-id="{!! $val->id !!}"><i
                                                class="fa fa-pencil li-btn"></i>Sửa
                                    </a>
                                </td>
                                <td class="action">
                                    <a data-toggle="modal" data-target="#modal-delete-warning"
                                       field-id="{!! $val->id !!}" class="btn btn-danger delete-record"><i
                                                class="fa fa-trash-o li-btn"></i>Xóa
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        <tr class="add-new">
                            <td></td>
                            <td><span class="btn-add" data-toggle="modal" data-target="#modal-add">+</span></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
                <div class="form-area">
                    {!! $create_form !!}
                    {!! $update_form !!}
                    {!! $warning_delete !!};
                </div>
            </div>
        </div>
    </div>
@stop
@section('custom script')
    <script>
        $(function () {
            $('.save-add').on('click', function () {
                $('#create-form').submit();
            });
            $('.save-update').on('click', function () {
                $('#update-form').submit();
            });
            $('.close-modal').on('click', function () {
                $('.modal').modal('hide');
            });
            $('.update-record').on('click', function () {
                var update_form = $('#update-form');
                var id = $(this).attr('field-id');
                update_form.find('input[name="id"]').val(id);
                var url = '{!! $url_get_update_data !!}' + '?id=' + id;
                $.ajax({
                    type: 'GET',
                    dataType: 'json',
                    url: url
                }).done(function (response) {
                    for (var key in response) {
                        update_form.find('#' + key + '-update').val(response[key]['value']);
                    }
                });
            });
            var delete_url = '';
            $('.delete-record').on('click', function () {
                var id = $(this).attr('field-id');
                var url = '{!! $url_delete !!}' + '?id=' + id;
                delete_url = url;
            });
            $('.confirm-delete').on('click', function () {
                if (delete_url !== '')
                    window.location.replace(delete_url);
            });
        });
    </script>
@stop

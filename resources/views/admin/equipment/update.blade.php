@extends('admin.index')
@section('custom css')
 <style>
	.portlet-body form{
		padding-left:18px;
	}
	label.error {
		  color: #a94442;
		  border-color: #ebccd1;
		  padding:1px 20px 1px 20px;
		}
</style>
@stop
@section('custom script')

<script src="{!! asset('public/theme/global/plugins/jquery.validate.min.js') !!}" type="text/javascript"></script>
<script>

	$(function () {

    $('#update-form').validate({
        rules: {
            name_equip: {
                required: true,
            },
        }       
    });
});
</script>

@stop

@section('content')
<h3 class="page-title">
Quản lý thiết bị
</h3>
<div class="row">
	<div class="col-md-12">
		<div class="portlet box blue">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-globe"></i>Cập nhật thiết bị
				</div>
			</div>
			<div class="portlet-body form">
				<form class="form-horizontal" id="update-form" action="{{ route('admin.equipment.update') }}" method="POST" >
				{!! csrf_field() !!}
				<input type="hidden" name="id" value="{{ $equipment->id }}">
					<div class="form-body">						
						<div class="form-group">
							<label class="col-md-2 control-label">Tên</label>
							<div class="col-md-10">
								<input type="text" name="name_equip" class="form-control" value="{{ $equipment->name_equip }}" required>
								<div class="form-control-focus">
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label">Nhóm</label>
							<div class="col-md-10">
								<select name="group_id" id="" class="form-control" required>
									@foreach($groups as $group)
										<option value="{{ $group->id }}" @if($equipment->group_id = $group->id) selected @endif>{{ $group->name }}</option>
									@endforeach
								</select>
								<div class="form-control-focus">
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label">Thiết bị Iot</label>
							<div class="col-md-10">
								<select name="iotdevice_id" id="" class="form-control" required>
									@foreach($iots as $iot)
										<option value="{{ $iot->id }}" @if($equipment->iotdevice_id = $iot->id) selected @endif>{{ $iot->name }}</option>
									@endforeach
								</select>
								<div class="form-control-focus">
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label">ID</label>
							<div class="col-md-10">
								<input type="text" name="mac" class="form-control" value="{{ $equipment->mac }}" required>
								<div class="form-control-focus">
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label">IP</label>
							<div class="col-md-10">
								<input type="text" name="ip" class="form-control" value="{{ $equipment->ip }}" required>
								<div class="form-control-focus">
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2 control-label">Thứ tự</label>
							<div class="col-md-10">
								<input type="number" name="ordered_at" class="form-control" value="{{ $equipment->ordered_at }}" required>
								<div class="form-control-focus order-error text-danger">
								</div>
							</div>
						</div>	
						<div class="form-group">
							<div class="col-md-3 col-md-offset-2">
								<button type="submit" class="btn blue">Cập nhật</button>
							</div>
						</div>
					</div>	
				</form>
			</div>
		</div>	
	</div>
</div>
@stop

@extends('admin.index')

@section('content')
<h3 class="page-title">
Quản lý thiết bị
</h3>
<div class="row">
	<div class="col-md-12">
		<div class="portlet box blue">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-globe"></i>Quản lý thiết bị
				</div>
			</div>
			<div class="portlet-body">			
				<table class="table table-striped table-hover table-bordered">
					<thead>
						<tr>
							<th style="width:5%">SL</th>
							<th>Tên</th>
							<th>Tầng</th>
							<th>ID</th>
							<th>Mac</th>
							<th>Thứ tự</th>
							<th>Nhóm</th>
							<th>Thiết bị IOT</th>
							<th>Trạng thái</th>
							<th style="width:8%">Cập nhật</th>
							<th style="width:8%">Xóa</th>
							
						</tr>
					</thead>
					<tbody>
						@foreach($equipments as $key => $equipment)
						<tr>
							<td>{{ $key + 1 }}</td>
							<td>{{ $equipment->name_equip }}</td>
							<td>{{ $equipment->room->name }}</td>
							<td>{{ $equipment->ip }}</td>
							<td>{{ $equipment->mac }}</td>
							<td>{{ $equipment->ordered_at }}</td>
							<td>@if(!empty($equipment->group)) {{ $equipment->group->name }} @endif</td>
							<td>@if(!empty($equipment->iot)) {{ $equipment->iot->name }} @endif</td>
							<td>{{ config('constants.status')[$equipment->status] }}</td>
							<td>
								<a href="{{ route('admin.equipment.edit',['id'=>$equipment->id]) }}" class="btn blue update"><i class="fa fa-pencil li-btn"></i>Cập nhật</a>
							</td>
							<td>
								<a href="{{ route('admin.equipment.delete',['id'=>$equipment->id]) }}" onclick="return confirm('Bạn có chắc muốn xóa bản ghi này không?')" class="btn btn-danger"><i class="fa fa-trash-o li-btn"></i>Xóa</a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
				<div class="col-md-12 pull-right">
					<div class="pull-right">
						{{ $equipments->render() }}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@stop
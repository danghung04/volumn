@extends('admin.index')
@section('custom css')
    <link rel="stylesheet" href="{!! asset('public/design/css/swiper.min.css') !!}">
    <link rel="stylesheet" href="{!! asset('public/design/css/setting.css') !!}"/>
    <link rel="stylesheet" href="{!! asset(config('constants.plugin').'scroller/jquery.mCustomScrollbar.css') !!}"/>
    <style>

        .icon-item {
            position: relative;
            z-index: 5;
            cursor: pointer;
        }

        .icon-drag img {
            height: 50px;
            cursor: pointer;
            background: rgb(13,25,41);
        }

        #frame {
            position: relative;
            z-index: 4;
        }

        #frame .icon-symbol img {
            height: 35px;
        }

        a.text-room-drag-update {
            color: #ccc;
            text-decoration: none;
        }

        a.text-room-drag-update:hover {
            color: #ccc;
        }

        .icon-drag-update {
            text-align: center;
        }

        .e-text {
            padding: 3px;
            color: #eee;
        }

        .text-room {
            cursor: pointer;
            z-index: 5;
            position: absolute;
            font-size: 18px;
            color: lawngreen;
        }

        .text-control {
            color: #ccc;
            font-size: 16px;
        }

        .setting-item .text-room {
            position: static;
        }

        .swiper-slide li{
            width: 88%;
            margin: auto;
        }
        .swiper-slide li.active {
            background: rgba(28, 28, 72, 0.5);
        }
        .swiper-slide li a{
            display: block;
            height: 100%;
            width: 100%;
        }
        .swiper-slide li a:hover{
            background-color: #251d47;
        }

        .icon-choose-wrapper {
            height: 5px;
            width: 100%;
            position: relative;
        }
    </style>
@stop
@section('content')
    <div class="content" style="text-transform: capitalize">
        <div class="cheader">
            <div class="setting-wrapper">
                <div class="menu-setting">
                    @if($room_active)
                        <div class="setting-item setting-left">
                            <a href="javascript:void(0)" class="setting-floor-form-btn">
                                Cài đặt tầng
                            </a>
                        </div>
                        <div class="setting-item">
                            <div class="text-control text-room text-room-drag" style="z-index:5;">
                        <span style="cursor: pointer">
                            Đặt tên phòng
                        </span>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
            <div class="icon-wrapper" id="content-scroller">
                <div class="my-scroller-content">
                    @foreach($equipment_types as $equipment_type)
                        <div class="icon-item">
                            <div class="icon-symbol @if($room_active->image && @getimagesize(asset(config('constants.image').$room_active->image)))icon-equip @endif" equipment-type-id="{!! $equipment_type->id !!}">
                                <img src="{!! asset(config('constants.icon').$equipment_type->icon) !!}"
                                     image="{!! asset(config('constants.image').$equipment_type->image) !!}"
                                     image_off="{!! asset(config('constants.image').$equipment_type->image_off) !!}"/>
                            </div>
                            <div class="icon-name">
                                {!! $equipment_type->name !!}
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="icon-choose-wrapper"></div>
        </div>

        <div class="bottom-content">
            <div class="left-content">
                <div class="top-left">
                    <div class="top-left-title">
                        <a href="{!! route('admin.group.index') !!}" style="color: #ccc">Cảnh</a>
                    </div>
                    <div class="top-left-divider">
                        <img alt=""
                             src="{!! asset(config('constants.divider_image')) !!}"/>
                    </div>
                </div>
                <div class="bottom-left">
                    <div class="swiper-container">
                        <div class="change-room prev-room">
                        <span class="glyphicon glyphicon-menu-up">
                        </span>
                        </div>
                        <ul class="swiper-wrapper">

                        </ul>
                        <div class="change-room next-room">
                        <span class="glyphicon glyphicon-menu-down">
                        </span>
                        </div>
                    </div>
                </div>
                <div class="action-bottom">
                    <div class="aitem">
                        <a href="javascript:void(0)" class="add-floor-form-btn">Thêm</a>
                    </div>
                    <div class="aitem">
                        @if($room_active)
                            <a href="{!! route('admin.room.delete', ['id' => $room_active->id]) !!}">Xóa</a>
                        @endif
                    </div>
                </div>
            </div>
            <div class="right-content" id="frame">
                <div class="background-wrapper" style="position: relative">
                    @if(!empty($room_active->image))
                        @if(@getimagesize(asset(config('constants.image').$room_active->image)))
                            <img class="bg-image" src="{{ asset(config('constants.image').$room_active->image) }}">
                        @else
                            {{--<img class="bg-image" src="{!! asset('public/design/images/room.png') !!}">--}}
                        @endif
                    @else
                        {{--<img class="bg-image" src="{!! asset('public/design/images/room.png') !!}">--}}
                    @endif
                    @foreach($equipments as $equipment)
                        <div class="icon-symbol icon-drag-update" equipment-id="{!! $equipment->id !!}"
                             id="equipment-{!! $equipment->id !!}"
                             state="{!! $equipment->status !!}"
                             style="position: absolute; left: {!! $equipment->x_offset !!}px; top: {!! $equipment->y_offset !!}px;">
                            <div class="image">
                                <img
                                        src="{!! asset(config('constants.image').$equipment->active_image) !!}"
                                        image="{!! asset(config('constants.image').$equipment->image) !!}"
                                        image_off="{!! asset(config('constants.image').$equipment->image_off) !!}"/>
                            </div>
                            <div class="e-text">
                                {!! $equipment->name_equip !!}
                            </div>
                        </div>
                    @endforeach
                    @foreach($sub_rooms as $sub_room)
                        <div class="text-room text-room-drag-update" sub-room-id='{!! $sub_room->id !!}'
                             style="left: {!! $sub_room->x_offset !!}px; top: {!! $sub_room->y_offset !!}px;">
                            <span>{!! $sub_room->name !!}</span>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    {!! $named_sub_room_form !!}
    {!! $named_setting_floor_form !!}
    {!! $create_equipment_modal !!}
    {!! $update_equipment_modal !!}
    {!! $add_floor_form !!}
@stop
@section('custom script')
    <!-- progress bar circle -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.0.4/socket.io.js"></script>
    <script>
        //        var url = "http://34.229.9.67:2021";
        var url = SOCKET_URL;
        var socket = io(url);
        socket.on('response', function (data) {
            console.log(data);
        });
        socket.on('response', function (data) {
            console.log(data);
            updateEquipmentStatus(data);
        });

        function updateEquipmentStatus(data) {
            var _token = "{!! csrf_token() !!}";
            var update_status_url = "{!!route('web.equipment.updateStatus') !!}";
            var iotdevice_id = data.iotdevice_id;
            var status = data.status;
            var position = data.position;
            $.ajax({
                url: update_status_url,
                type: 'POST',
                dataType: 'json',
                data: {
                    _token: _token,
                    iotdevice_id: iotdevice_id,
                    status: status,
                    ordered_at: position
                },
            }).done(function (data) {
                console.log(data);
                if (data.id) {
                    var eObject = $('#equipment-' + data.id);
                    var imgObject = eObject.find('img');
                    var image = imgObject.attr('image');
                    var image_off = imgObject.attr('image_off');
                    var src = image_off;
                    if (data.status === '1') {
                        src = image;
                        eObject.attr('state', 1);
                    } else {
                        eObject.attr('state', 0);
                    }
                    imgObject.attr('src', src);
                }
            });
        }

        //        socket.emit('request', 'msg from hung');
    </script>
    <!--end progress bar circle -->
    <!-- Thêm slide cho một list -->
    <script src="{!! asset('public/design/js/swiper.min.js') !!}"></script>
    <script>
        //        Lấy dữ liệu về các phòng
        var url_room_origin = '{!! route('admin.index') !!}' + '?rid=';
        var room_active_id = {!! !empty($room_active)?$room_active->id:'' !!};
        var rooms = {!! !empty($rooms)?json_encode($rooms):'' !!};
        var room_ids = [];
        var room_names = [];
        for (var index in rooms) {
            if (rooms.hasOwnProperty(index)) {
                var room = rooms[index];
                room_ids.push(room.id);
                room_names.push(room.name);
            }
        }
        var swiper = new Swiper('.swiper-container', {
            direction: 'vertical',
            slidesPerView: 10,
            centeredSlides: false,
            spaceBetween: 0,
            pagination: {
                el: '.swiper-pagination',
                type: 'fraction',
            },
            navigation: {
                nextEl: '.next-room',
                prevEl: '.prev-room'
            },
            virtual: {
                slides: (function () {
                    var slides = [];
                    for (var index in room_ids) {
                        if (room_ids.hasOwnProperty(index) && room_names.hasOwnProperty(index)) {
                            var url = url_room_origin + room_ids[index];
                            var active = '';
                            if (room_ids[index] === room_active_id) {
                                active = 'class="active"';
                            }
                            var elem = '<li ' + active + '><a href="' + url + '" title="'+ room_names[index] +'">' + room_names[index] + '</a></a>';
                            slides.push(elem);
                        }
                    }
                    slides.push('<li></li>');
                    slides.push('<li></li>');
                    return slides;
                }()),
            },
        });
    </script>
    <script>
        $(function () {
            $('#create-equipment-modal-form').on('submit', function (e) {
//                e.preventDefault();
                var name_equip = $(this).find('input[name="name_equip"]').val();
                var stt = $(this).find('input[name="ordered_at"]').val();
                var valid = true;
                if (name_equip === '') {
                    $(this).find('.error-name_equip').text('Chưa nhập tên thiết bị');
                    valid = false;
                }
                if (stt === '') {
                    $(this).find('.error-ordered_at').text('Chưa nhập số thứ tự');
                    valid = false;
                }
                stt = parseInt(stt);
                if (stt <= 0) {
                    $(this).find('.error-ordered_at').text('Số thứ tự phải lớn hơn 0');
                    valid = false;
                }
                return valid;
            });
        })
    </script>
    <script>
        $(function () {
            $('input[name="ordered_at"]').on('keyup', function () {
                var stt = parseInt($(this).val());
                var max = $(this).attr('max');
                if (stt < 1) {
                    $(this).val(1);
                }
                if(max > 0){
                    if(stt > max){
                        $(this).val(max);
                    }
                }
            });
            $('input[name="status"]').parent().parent().parent().css('text-align', 'right');
        });
    </script>
    <script src="{!! asset(config('constants.plugin').'scroller/jquery.mCustomScrollbar.concat.min.js') !!}"></script>
    <script>
        $(function () {
            $("#content-scroller").mCustomScrollbar({
                axis: "x",
                advanced: {
                    autoExpandHorizontalScroll: true
                }
            });
        });
    </script>
    <script>
        $(function(){
            var create_modal = $('#create-equipment-modal');
            var stt_object = create_modal.find('input[name="ordered_at"]');
            var init_iot_id = create_modal.find('select[name="iotdevice_id"]').val();
            setMaxStt(init_iot_id);
            create_modal.on('change', 'select[name="iotdevice_id"]', function(){
                setMaxStt($(this).val());
           });
            function setMaxStt(id){
                var url = '{!! route("admin.iot-device.getMaxEquip") !!}';
                $.ajax({
                    type: 'POST',
                    url: url,
                    data:{
                        id: id
                    }
                }).done(function(data){
                    $(stt_object).attr('max', data);
                });
            }
        });
    </script>
    <script>
        $(function(){
            var update_modal = $('#update-equipment-modal');
            var stt_object = update_modal.find('input[name="ordered_at"]');
            var init_iot_id = update_modal.find('select[name="iotdevice_id"]').val();
            setMaxStt(init_iot_id);
            update_modal.on('change', 'select[name="iotdevice_id"]', function(){
                setMaxStt($(this).val());
            });
            function setMaxStt(id){
                var url = '{!! route("admin.iot-device.getMaxEquip") !!}';
                $.ajax({
                    type: 'POST',
                    url: url,
                    data:{
                        id: id
                    }
                }).done(function(data){
                    $(stt_object).attr('max', data);
                });
            }

            $('.background-wrapper').on('dblclick', '.icon-symbol', function(){
                var id = $(this).attr('equipment-id');
                var url = '{!! url('/admin/equipment/edit') !!}' + '/' + id;
                $.ajax({
                    type: 'GET',
                    dataType: 'json',
                    url: url
                }).done(function(data){
                    update_modal.find('input[name="id"]').val(data.id);
                    update_modal.find('input[name="room_id"]').val(data.room_id);
                    update_modal.find('input[name="equipment_type_id"]').val(data.equipment_type_id);
                    update_modal.find('input[name="name_equip"]').val(data.name_equip);
                    update_modal.find('select[name="iotdevice_id"]').val(data.iotdevice_id);
                    update_modal.find('input[name="ordered_at"]').val(data.ordered_at);
                    update_modal.find('select[name="group_id"]').val(data.group_id);
                    update_modal.find('input[name="status"]').val(data.status);
                    if(data.status === 1){
                        update_modal.find('input[name="status"]').prop('checked', true);
                    }
                    setMaxStt(data.iotdevice_id);
                    $(update_modal).modal('show');
                });
            });
        });
    </script>
    <script>
        $(function(){
            var create_modal = $('#create-equipment-modal');
            var stt_object = create_modal.find('input[name="ordered_at"]');
            var init_iot_id = create_modal.find('select[name="iotdevice_id"]').val();
            setMaxStt(init_iot_id);
            create_modal.on('change', 'select[name="iotdevice_id"]', function(){
                setMaxStt($(this).val());
           });
            function setMaxStt(id){
                var url = '{!! route("admin.iot-device.getMaxEquip") !!}';
                $.ajax({
                    type: 'POST',
                    url: url,
                    data:{
                        id: id
                    }
                }).done(function(data){
                    $(stt_object).attr('max', data);
                });
            }
        });
    </script>
    <script>
        $(function(){
            var update_modal = $('#update-equipment-modal');
            var stt_object = update_modal.find('input[name="ordered_at"]');
            var init_iot_id = update_modal.find('select[name="iotdevice_id"]').val();
            setMaxStt(init_iot_id);
            update_modal.on('change', 'select[name="iotdevice_id"]', function(){
                setMaxStt($(this).val());
            });
            function setMaxStt(id){
                var url = '{!! route("admin.iot-device.getMaxEquip") !!}';
                $.ajax({
                    type: 'POST',
                    url: url,
                    data:{
                        id: id
                    }
                }).done(function(data){
                    $(stt_object).attr('max', data);
                });
            }

            $('.background-wrapper').on('dblclick', '.icon-symbol', function(){
                var id = $(this).attr('equipment-id');
                var url = '{!! url('/admin/equipment/edit') !!}' + '/' + id;
                $.ajax({
                    type: 'GET',
                    dataType: 'json',
                    url: url
                }).done(function(data){
                    update_modal.find('input[name="id"]').val(data.id);
                    update_modal.find('input[name="room_id"]').val(data.room_id);
                    update_modal.find('input[name="equipment_type_id"]').val(data.equipment_type_id);
                    update_modal.find('input[name="name_equip"]').val(data.name_equip);
                    update_modal.find('select[name="iotdevice_id"]').val(data.iotdevice_id);
                    update_modal.find('input[name="ordered_at"]').val(data.ordered_at);
                    update_modal.find('select[name="group_id"]').val(data.group_id);
                    update_modal.find('input[name="status"]').val(data.status);
                    if(data.status === 1){
                        update_modal.find('input[name="status"]').prop('checked', true);
                    }
                    setMaxStt(data.iotdevice_id);
                    $(update_modal).modal('show');
                });
            });
        });
    </script>
    <script>
        $(function(){
            $('.top-left').on('click', function(){
                window.location.replace($(this).find('a').attr('href'));
            });
        });
    </script>
    @include('admin.home.script_floor')
    @include('admin.home.drag-drop')
@stop
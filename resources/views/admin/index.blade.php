<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8"/>
        <title>@if(isset($title)) {!! $title !!} @else Trang quản trị @endif </title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
        <meta http-equiv="Content-type" content="text/html; charset=utf-8">
        <meta content="" name="description"/>
        <meta content="" name="author"/>
        <link rel="icon" href="{!! asset('public/images/favicon.png') !!}"  />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        @include('admin.includes.css')
        <script src="{{ asset('public/web/js/plugin/jquery.min.js') }}" type="text/javascript"></script>
        <script>
            var url_server = '{!! url('/') !!}';
            var param = url_server.split(':');
            var SOCKET_URL = param[0] +':'+param[1]+':2021';
        </script>
    @yield('custom css')
    <!-- END THEME STYLES -->
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="page-md page-header-fixed page-quick-sidebar-over-content ">
        <!-- BEGIN HEADER -->
    @include('admin.includes.header')
    <!-- END HEADER -->
        <div class="clearfix">
        </div>
        <!-- BEGIN CONTAINER -->

        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
        @include('admin.includes.side-menu')
        <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">
                @include('admin.includes.flash')

                <!-- BEGIN PAGE CONTENT-->
                @yield('content')
                <!-- END PAGE CONTENT-->
                </div>
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->

    <!-- END FOOTER -->
        <!-- BEGIN CORE PLUGINS -->
    @include('admin.includes.js')

    <!-- END PAGE LEVEL SCRIPTS -->
        @yield('custom script')
    </body>
    <!-- END BODY -->
</html>
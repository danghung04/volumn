<div class="page-sidebar-wrapper">
	<div class="page-sidebar navbar-collapse collapse">
		<!-- BEGIN SIDEBAR MENU -->
		<ul class="page-sidebar-menu page-sidebar-menu-light " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
			<li @if(request()->is('admin/room') || request()->is('admin')|| request()->is('admin/group')) class="active" @endif>
				<a href="{{ route('admin.index') }}">
					<i class="icon"><img src="{!! asset('public/images/home-icon.png') !!}" alt=""></i>
					<span class="title">Quản lý nhà</span>
				</a>
			</li>
			<li @if(request()->is('admin/equipment-type')) class="active" @endif>
				<a href="{{ route('admin.equipment-type.index') }}">
					<i class="icon"><img src="{!! asset('public/images/equip-type-icon.png') !!}" alt=""></i>
					<span class="title">Quản lý loại thiết bị</span>
				</a>
			</li>
			{{--<li @if(request()->is('admin/equipment')) class="active" @endif>--}}
				{{--<a href="{{ route('admin.equipment.index') }}">--}}
					{{--<i class="icon-settings"></i>--}}
					{{--<span class="title">Quản lý thiết bị</span>--}}
				{{--</a>--}}
			{{--</li>--}}
			<li @if(request()->is('admin/iot-device')) class="active" @endif>
				<a href="{{ route('admin.iot-device.index') }}">
					<i class="icon"><img src="{!! asset('public/images/iot-icon.png') !!}" alt=""></i>
					<span class="title">Quản lý thiết bị iot</span>
				</a>
			</li>
            {{--<li @if(request()->is('admin/group')) class="active" @endif>--}}
                {{--<a href="{{ route('admin.group.index') }}">--}}
                    {{--<i class="icon-settings"></i>--}}
                    {{--<span class="title">Quản lý phân nhóm</span>--}}
                {{--</a>--}}
            {{--</li>--}}
			<li @if(request()->is('admin/user')) class="active" @endif>
				<a href="{{ route('admin.user.index') }}">
					<i class="icon"><img src="{!! asset('public/images/user-icon.png') !!}" alt=""></i>
					<span class="title">Quản lý người dùng</span>
				</a>
			</li>
		</ul>
	</div>
</div>
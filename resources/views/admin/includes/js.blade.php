<script src="{{ asset('public/web/js/plugin/jquery-migrate.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/web/js/plugin/jquery-ui.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/web/js/plugin/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/web/js/plugin/bootstrap-hover-dropdown.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/web/js/plugin/jquery.slimscroll.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/web/js/plugin/jquery.blockui.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/web/js/plugin/jquery.cokie.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/web/js/plugin/jquery.uniform.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/web/js/plugin/select2.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/web/js/plugin/jquery.dataTables.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/web/js/plugin/dataTables.bootstrap.js') }}" type="text/javascript"></script>
<script type="text/javascript" src="{{ asset('public/theme/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{ asset('public/theme/global/scripts/metronic.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/theme/admin/layout/scripts/layout.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/theme/admin/layout/scripts/quick-sidebar.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/theme/admin/layout/scripts/demo.js') }}" type="text/javascript"></script>
<script src="{{ asset('public/plugin/ckeditor/ckeditor.js') }}" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.0.4/socket.io.js"></script>
<script>
	$(document).ready(function() {
	$('#sample_editable_1').dataTable({
	    "bPaginate": false,
	    "bLengthChange": false,
	    "bFilter": true,
	    "bInfo": false,
	    "bAutoWidth": false,
	    "lengthChange": false });
	});

</script>

<script>
jQuery(document).ready(function() {
	Metronic.init(); // init metronic core components
	Layout.init(); // init current layout
	QuickSidebar.init(); // init quick sidebar
	Demo.init(); // init demo features
        });
</script>
<script>

    var winHeight = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
    $('.page-content').css('min-height',winHeight);
</script>
<style>
    .drop-user{
        margin-top: 0;
        margin-bottom: 0;
    }
    .navbar-nav > li > a {
        padding-top: 11px;
        padding-bottom: 10px;
        line-height: 20px;
    }
    .page-header{
        left: 19px;
    }
    .drop-user img{
        height: 45px;
        width: 45px;
    }
</style>
<div class="page-header navbar">
    <div class="page-header-inner">
        <!-- BEGIN LOGO -->
        <div class="page-logo" style='padding-top: 10px'>
            <a href="{{ url('/') }}" style="color:white; text-transform: none">Trang chủ</a>
            <div class="menu-toggler sidebar-toggler hide">
            </div>
        </div>
        <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse"
           data-target=".navbar-collapse">
        </a>
        {{--<div class="top-menu">--}}
        <ul class="nav navbar-nav pull-right">
            <!-- END TODO DROPDOWN -->
            <!-- BEGIN USER LOGIN DROPDOWN -->
            <li class="dropdown dropdown-user drop-user">
                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                   data-close-others="false"  style="background: transparent; color: #ccc">
                    @if(!empty(Auth::user()->avatar))
                        @if(@getimagesize(asset(config('constants.image').Auth::user()->avatar)))
                            <img style="height: 35px; width: 35px" class="img-circle"
                                 src="{{ asset(config('constants.image').Auth::user()->avatar) }}">
                        @else
                            <img class="img-circle" src="{{ asset(config('constants.default_avatar')) }}">
                        @endif
                    @else
                        <img class="img-circle" src="{{ asset(config('constants.default_avatar')) }}">
                    @endif
                    <span class="username username-hide-on-mobile">
					@if(!empty(Auth::user()))
                            {!! Auth::user()->name !!}
                        @endif</span>
                    <i class="fa fa-angle-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-menu-default">
                    <li>
                        <a href="{{ route('logout') }}" style="color: #ccc;background-color: #000">
                            <span class="glyphicon glyphicon-log-out" style="margin-right: 5px">
                            </span> Đăng xuất</a>
                    </li>
                </ul>
            </li>
        </ul>
    {{--</div>--}}
    <!-- END TOP NAVIGATION MENU -->
    </div>
</div>
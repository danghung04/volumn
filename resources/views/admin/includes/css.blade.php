<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet"
      type="text/css"/>
<link href="{!! asset('public/theme/global/plugins/font-awesome/css/font-awesome.min.css') !!}" rel="stylesheet"
      type="text/css"/>
<link href="{!! asset('public/theme/global/plugins/simple-line-icons/simple-line-icons.min.css') !!}" rel="stylesheet"
      type="text/css"/>
<link href="{!! asset('public/theme/global/plugins/bootstrap/css/bootstrap.css') !!}" rel="stylesheet" type="text/css"/>
<link href="{!! asset('public/theme/global/plugins/uniform/css/uniform.default.css') !!}" rel="stylesheet"
      type="text/css"/>

<link href="{!! asset('public/theme/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css') !!}" rel="stylesheet"
      type="text/css"/>
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="{!! asset('public/theme/global/plugins/select2/select2.css') !!}"/>
<link rel="stylesheet" type="text/css"
      href="{!! asset('public/theme/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css') !!}"/>
<link rel="stylesheet" type="text/css"
      href="{!! asset('public/theme/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') !!}"/>
<!-- END PAGE LEVEL STYLES -->
<!-- BEGIN THEME STYLES -->
<link href="{!! asset('public/theme/global/css/components-md.css') !!}" id="style_components" rel="stylesheet"
      type="text/css"/>
<link href="{!! asset('public/theme/global/css/plugins-md.css') !!}" rel="stylesheet" type="text/css"/>
<link href="{!! asset('public/theme/admin/layout/css/layout.css') !!}" rel="stylesheet" type="text/css"/>
<link id="style_color" href="{!! asset('public/theme/admin/layout/css/themes/darkblue.css') !!}" rel="stylesheet"
      type="text/css"/>
<link href="{!! asset('public/theme/admin/layout/css/custom.css') !!}" rel="stylesheet" type="text/css"/>
<link href="{!! asset('public/design/css/admin.css') !!}" rel="stylesheet" type="text/css"/>
<style>
    body{
        min-width: 1080px;
        text-transform: uppercase;
        max-width: 1368px;
        margin:auto;
        overflow: auto;
    }
    .form-horizontal .control-label {
        text-align: left;
    }

    @media (min-width: 768px) {
        .modal-dialog {
            margin : 100px auto;
        }
    }
    @media (max-width: 993px){
        .page-sidebar-closed.page-sidebar-fixed .page-sidebar:hover .page-sidebar-menu, .page-sidebar .page-sidebar-menu{
            padding-top: 15px;
        }
        .page-sidebar .page-sidebar-menu > li {
            margin-top: 0;
        }
        .page-sidebar .page-sidebar-menu {
             padding-top: 0;
            padding-left: 0;
        }
    }
    /*.modal-open {*/
        /*overflow-y: hidden;*/
    /*}*/
    .modal-dialog {
        background: rgba(0,0,0,0.1);
    }

    .modal-dialog form .form-group label{
        text-transform: uppercase;
    }
    .my-error{
        text-transform: none;
    }
</style>
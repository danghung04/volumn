@extends('admin.index')
@section('custom css')
    <style>
        .portlet.box > .portlet-body {
            background: transparent;
            padding: 0px;
            box-shadow: none;
            width: 100%;
        }

        .portlet-body table {
            width: 100%;
            color: #ccc;
        }

        .portlet-body table tr td, .portlet-body table tr th {
            border-bottom: 1px solid #657293;
            border-right: 1px solid #657293;
            font-size: 1.2em;
        }

        .portlet-body table tr th {
            padding: 20px;
            font-weight: normal;
        }

        .portlet-body table tr td:last-child, .portlet-body table tr th:last-child {
            border-right-width: 0px;
        }

        .portlet-body table tr .admin-name {
            font-weight: bold;
            text-transform: uppercase;
            font-size: 1.7em;
            color: #fff;

        }

        .portlet-body table tr .name {
            padding-left: 5%;
        }

        .portlet-body table tr .email {
            padding-left: 10%;
        }

        .portlet-body table tr th.action {
            text-align: center;
        }

        .portlet-body table tr td {
            height: 50px;
            line-height: 50px;
        }

        .portlet-body table tr td.action {
            text-align: center;
        }

        .portlet-body table tr.add-new td {
            border: none;
            padding-left: 5%;
        }

        .portlet-body table tr.add-new .btn-add {
            font-weight: bold;
            font-size: 2.7em;
            color: #efefef;
            cursor: pointer;
        }

        .portlet-body table tr.add-new .btn-add:hover {
            color: #fff;
        }

        .orange {
            background: #FFBA00;
            color: #000;
        }

        .orange:hover {
            background: #FFAC00;
        }
        .portlet{
         box-shadow: none;
        }
        .my-close span{
            font-size: 24px;
        }
    </style>
@stop
@section('content')
    <h3 class="page-title">

    </h3>
    <div class="row main-content">
        <div class="portlet box">
            <div class="portlet-body" style="text-transform: none">
                <table>
                    <thead>
                        @foreach($admins as $key => $val)
                            <tr>
                                <th style="width: 10%;"></th>
                                <th class="name admin-name">{{ $val->name }}</th>
                                <th class="email">{{ $val->email }}</th>
                                <th class="action">
                                    <a class="btn orange update-record" data-toggle="modal" field-id="{!! $val->id !!}"
                                       data-target="#modal-update"><i
                                                class="fa fa-pencil li-btn"></i>Sửa
                                    </a>
                                </th>
                                <th class="action">
                                </th>
                            </tr>
                        @endforeach
                    </thead>
                    <tbody>
                    @foreach($users as $key => $val)
                        <tr>
                            <td style="text-align: center;">{{ $key + 1}}</td>
                            <td class="name">{{ $val->name }}</td>
                            <td class="email">{{ $val->email }}</td>
                            <td class="action">
                                <a class="btn orange update-record" data-toggle="modal" data-target="#modal-update"
                                   field-id="{!! $val->id !!}"><i
                                            class="fa fa-pencil li-btn"></i>Sửa
                                </a>
                            </td>
                            <td class="action">
                                <a data-toggle="modal" data-target="#modal-delete-warning"
                                   field-id="{!! $val->id !!}" class="btn btn-danger delete-record"><i
                                            class="fa fa-trash-o li-btn"></i>Xóa
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    <tr class="add-new">
                        <td></td>
                        <td><span class="btn-add">+</span></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    </tbody>
                </table>
                <div class="form-area">
                    {!! $create_form !!}
                    {!! $update_form !!}
                    {!! $warning_delete !!};
                </div>
            </div>
        </div>
    </div>
@stop
@section('custom script')
    <script>
        $(function () {
            var modal_add = $('#modal-add');
            var form_add = $('#create-form');
            var modal_update = $('#modal-update');
            var form_update = $('#update-form');
            var urlCheckDuplicate = '{!! route('admin.user.checkDuplicate') !!}';
            var createFieldKeys = ['name', 'email', 'password', 'password_confirmation'];
            var updateFieldKeys = ['name', 'email', 'password'];
            function checkClientError(mode) {
                var checkError = false;
                var modal = null;
                var password = null;
                if (mode === 'add') {
                    modal = modal_add;
                    removeAllErrorMsg();
                    password = getValue(modal, 'password');
                    var password_confirmation = getValue(modal, 'password_confirmation');
                    if (!password || password.length < 6 || !password_confirmation)
                        checkError = true;

                    if (password === '')
                        setErrorMsg(modal, 'password', "Mật khẩu không được để trống");
                    else if (password.length < 6)
                        setErrorMsg(modal, 'password', "Mật khẩu phải chứa ít nhất 6 ký tự");
                    else {
                        if (password_confirmation === '')
                            setErrorMsg(modal, 'password_confirmation', "Xác nhận mật khẩu không được để trống");
                        else if (password_confirmation !== password)
                            setErrorMsg(modal, 'password_confirmation', "Xác nhận mật khẩu không đúng");
                    }
                } else {
                    modal = modal_update;
                    removeAllErrorMsg();
                    password = getValue(modal, 'password');
                }
                var name = getValue(modal, 'name');
                var email = getValue(modal, 'email');
                if (!name || !email || (password && password.length < 6))
                    checkError = true;
                if (name === '')
                    setErrorMsg(modal, 'name', "Tên đăng nhập không được để trống");
                if (email === '')
                    setErrorMsg(modal, 'email', "Email không được để trống");
                if (password && password.length < 6)
                    setErrorMsg(modal, 'password', "Mật khẩu phải chứa ít nhất 6 ký tự");
                return checkError;
            }

            function getValue(container, fieldName) {
                return container.find('input[name="' + fieldName + '"]').val();
            }

            function setErrorMsg(container, fieldName, errorMsg) {
                container.find('.my-error-' + fieldName).text(errorMsg);
            }
            function removeAllErrorMsg(){
                var container = modal_add;
                var listField = createFieldKeys;
                for(var index in listField){
                    if(listField.hasOwnProperty(index)){
                        removeErrorMsg(container, listField[index]);
                    }
                }

                container = modal_update;
                listField = updateFieldKeys;
                for(var index in listField){
                    if(listField.hasOwnProperty(index)){
                        removeErrorMsg(container, listField[index]);
                    }
                }
            }
            function removeErrorMsg(container, fieldName){
                setErrorMsg(container, fieldName, '');
            }

            function checkErrorAndSubmitForm(modal,form, url, data) {
                $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: 'json',
                    data: data
                }).done(response => {
                    if (response.state === 0) {
                        form.submit();
                    } else {
                        if(response.msg.name)
                        setErrorMsg(modal, 'name', response.msg.name);
                        if(response.msg.email)
                        setErrorMsg(modal, 'email', response.msg.email);
                    }
                })
            }

            $('.btn-add').on('click', function(){
                removeAllErrorMsg();
                modal_add.modal('show');
            });
            $('.save-add').on('click', function () {
                var clientError = checkClientError('add');
                if (clientError)
                    return false;
                var modal = modal_add;
                var name = getValue(modal, 'name');
                var email = getValue(modal, 'email');
                var data = {
                    name: name,
                    email: email
                }
                checkErrorAndSubmitForm(modal_add, form_add, urlCheckDuplicate, data);
            });
            $('.save-update').on('click', function () {
                var clientError = checkClientError('update');
                if (clientError)
                    return false;
                var modal = modal_update;
                var id = getValue(modal, 'id');
                var name = getValue(modal, 'name');
                var email = getValue(modal, 'email');
                var data = {
                    id: id,
                    name: name,
                    email: email
                }
                checkErrorAndSubmitForm(modal_update, form_update, urlCheckDuplicate, data);
            });
            $('.close-modal').on('click', function () {
                $('.modal').modal('hide');
            });
            $('.update-record').on('click', function () {
                removeAllErrorMsg();
                var update_form = $('#update-form');
                var id = $(this).attr('field-id');
                update_form.find('input[name="id"]').val(id);
                var url = '{!! $url_get_update_data !!}' + '?id=' + id;
                $.ajax({
                    type: 'GET',
                    dataType: 'json',
                    url: url
                }).done(function (response) {
                    for (var key in response) {
                        update_form.find('#' + key + '-update').val(response[key]['value']);
                    }
                });
            });
            var delete_url = '';
            $('.delete-record').on('click', function () {
                var id = $(this).attr('field-id');
                var url = '{!! $url_delete !!}' + '?id=' + id;
                delete_url = url;
            });
            $('.confirm-delete').on('click', function () {
                if (delete_url !== '')
                    window.location.replace(delete_url);
            });
        });
    </script>
@stop

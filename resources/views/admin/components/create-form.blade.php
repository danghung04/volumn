<style>
    .modal-content {
        padding: 5px;
        color: #fff;
    }

    .form-group > label {
        color: #fff;
    }

    .orange {
        background: #FFBA00;
    }

    .orange:hover {
        background: #da8c00;
    }

    .modal-footer {
        color: #000;
    }
    .my-close{
        cursor: pointer;
    }
</style>
<div class="modal fade" id="modal-add">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <div class="my-close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove-circle pull-right" style="color: #fff"></span></div>
                <h4 class="modal-title text-center" style="color: #fff;text-transform: uppercase;font-size: 2em;
            font-weight: bold;">Thêm</h4>
            </div>
            <form class="form-horizontal" id="create-form" action="{!! $action !!}"
                  method="POST" enctype="multipart/form-data">
                {!! csrf_field() !!}
                <div class="form-body">
                    @foreach($fields as $field_key => $field)
                        <div class="form-group">
                            <label class="col-md-4 control-label my-label">{!! $field['title'] !!}</label>
                            <div class="col-md-8">
                                @if(is_array($field['type']))
                                    <input type="{!! $field['type'][1] !!}" name="{!! $field_key !!}"
                                           class="form-control" value="{{ old($field_key) }}"
                                           required>
                                    <span class="my-error-{!! $field_key !!}"></span>
                                @elseif($field['type'] == 'select')
                                    <select name="{!! $field_key !!}" class="form-control"
                                    @if(isset($field['field_attributes']))
                                        @foreach($field['field_attributes'] as $attribute_key => $attribute_value )
                                            {{ $attribute_key.'='.$attribute_value.' ' }}
                                                @endforeach
                                            @endif
                                    >
                                        @foreach($field['data'] as $data_key => $data_value)
                                            <option value="{!! $data_key !!}">{!! $data_value !!}</option>
                                        @endforeach
                                    </select>
                                @else
                                @endif
                            </div>
                        </div>
                    @endforeach
                </div>
            </form>
            <div class="modal-footer">
                <div class="col-xs-6">
                    <button class="btn orange pull-left close-modal">Hủy</button>
                </div>
                <div class="col-xs-6">
                    <button class="btn orange save-add">Lưu</button>
                </div>
            </div>
        </div>
    </div>
</div>
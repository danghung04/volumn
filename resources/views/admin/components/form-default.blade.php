<style>
    .modal-content {
        padding: 5px;
        color: #fff;
    }

    .form-group > label {
        color: #fff;
    }

    .orange {
        background: #FFBA00;
    }

    .orange:hover {
        background: #da8c00;
    }

    .modal-footer {
        color: #000;
    }

    .my-close {
        cursor: pointer;
    }
    .my-input{
        width: 84%;
    }
    .my-checkbox-input{
        position: absolute;
        right: 42px;
        top: 7px;
    }
</style>
<?php
$dialog_width = isset($form_width) ? $form_width . 'px' : '450px';
$label_portion = isset($label_width) ? $label_width : 6;
$input_portion = 12 - $label_portion;
$label_width_class = 'col-md-' . $label_portion;
$input_width_class = 'col-md-' . $input_portion;
$modal_id = isset($modal_id) ? $modal_id : 'modal-custom';
?>
<div class="modal fade" id="{!! $modal_id !!}">
    <div class="modal-dialog" style="width: {!! $dialog_width !!}">
        <div class="modal-content">
            <div class="modal-header">
                <div class="my-close" data-dismiss="modal" aria-hidden="true"><span
                            class="glyphicon glyphicon-remove-circle pull-right" style="color: #fff; font-size: 24px"></span></div>
                <h4 class="modal-title text-center" style="color: #fff;text-transform: uppercase;font-size: 2em;
            font-weight: bold;">{!! $form_title !!}</h4>
            </div>
            <div class="my-divider" style="height: 20px;width: 100%;padding: 5px;">
                <div class="my-bar" style="margin:auto; width: 90%; background: #666; height: 2px;">
                </div>
            </div>
            <form class="form-horizontal" id="{!! $modal_id !!}-form" action="{!! $action !!}"
                  method="POST" enctype="multipart/form-data">
                {!! csrf_field() !!}
                <div class="form-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @foreach($fields as $field_key => $field)
                        @if(isset($field['title']))
                            <div class="form-group" style="margin-bottom: 0">

                                <label class="{!! $label_width_class !!} control-label my-label" style="padding-left: 40px" >{!! $field['title'] !!}</label>

                                <div class="{!! $input_width_class !!}">
                                    @if(is_array($field['type']))
                                        @if($field['type'][1] == 'checkbox')
                                            <input type="{!! $field['type'][1] !!}" name="{!! $field_key !!}"
                                                   class="my-checkbox-input" style="height: 18px; width: 18px;" value="1">
                                        @else
                                            <input type="{!! $field['type'][1] !!}" name="{!! $field_key !!}"
                                                   class="form-control my-input" value="{{ old($field_key) }}">
                                            <span class="my-error error-{!! $field_key !!}" style="text-transform: none"></span>
                                        @endif

                                    @elseif($field['type'] == 'select')
                                        <select name="{!! $field_key !!}" class="form-control my-input"
                                                {{--Thêm thuộc tính vào cho thẻ select nếu có, thường sẽ là readonly, disabled--}}
                                        @if(isset($field['field_attributes']))
                                            @foreach($field['field_attributes'] as $attribute_key => $attribute_value )
                                                {{ $attribute_key.'='.$attribute_value.' ' }}
                                                    @endforeach
                                                @endif
                                        >
                                            @foreach($field['data'] as $data_key => $data_value)
                                                <option value="{!! $data_key !!}">{!! $data_value !!}</option>
                                            @endforeach
                                        </select>
                                    @else
                                    @endif
                                </div>
                            </div>
                                <div class="my-divider" style="height: 20px;width: 100%;padding: 0 0 5px 0;">
                                    <div class="my-bar" style="margin:auto; width: 80%; background: #666; height: 1px;">
                                    </div>
                                </div>
                        @else
                            <input type="{!! $field['type'][1] !!}" name="{!! $field_key !!}"
                                   value="{{ old($field_key) }}">
                        @endif
                    @endforeach
                </div>
            </form>
            <div class="modal-footer">
                <div class="col-xs-6">
                    <button class="btn orange pull-left close-modal{!! $modal_id !!}">Hủy</button>
                </div>
                <div class="col-xs-6">
                    <button class="btn orange save{!! $modal_id !!}">Lưu</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {
        var modal_id = '{!! $modal_id !!}';
        $('.save' + modal_id).on('click', function () {
            $('#' + modal_id + '-form').submit();
        });
        $('.close-modal' + modal_id).on('click', function () {
            $('#' + modal_id).modal('hide');
        });
    });
</script>
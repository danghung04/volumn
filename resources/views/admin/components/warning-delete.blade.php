<style>
    .modal-content {
        padding: 5px;
        color: #fff;
    }

    .form-group > label {
        color: #fff;
    }
    input:-webkit-autofill {
        /*-webkit-box-shadow: inset 0 0 0px 9999px #3B4583;*/
        -webkit-box-shadow: inset 0 0 0px 9999px #FFF;
    }

    .orange {
        background: #FFBA00;
    }

    .orange:hover {
        background: #da8c00;
    }

    .modal-footer {
        color: #000;
    }
    .content-text{
        padding: 15px;
        text-align: center;
        font-size: 1.2em;
    }
    .modal-header{
        border-width: 0;
    }
</style>
<div class="modal fade" id="modal-delete-warning">
    <div class="modal-dialog" style="width: 400px">
        <div class="modal-content">
            <div class="modal-header">
                <div class="my-close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove-circle pull-right" style="color: #fff"></span></div>
                <h4 class="modal-title text-center" style="color: #fff;text-transform: uppercase;font-size: 1.5em;
            font-weight: bold;">{!! isset($modal_title)?$modal_title: 'Xóa người dùng' !!}</h4>
            </div>
            <div class="content-text" style="text-transform: none;">
                {!! isset($modal_content)?$modal_content: 'Bạn có chắc muốn xóa người dùng này?' !!}
            </div>
            <div class="modal-footer">
                <div class="col-xs-6">
                    <button class="btn orange pull-left close-modal">Hủy</button>
                </div>
                <div class="col-xs-6">
                    <button class="btn orange confirm-delete">Xóa</button>
                </div>
            </div>
        </div>
    </div>
</div>
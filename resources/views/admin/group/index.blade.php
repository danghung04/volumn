@extends('admin.index')
@section('custom css')
    <link rel="stylesheet" href="{!! asset('public/design/css/swiper.min.css') !!}">
    <link rel="stylesheet" href="{!! asset('public/design/css/group.css') !!}"/>
    <style>
        .portlet.box > .portlet-body {
            background: transparent;
            padding: 0px;
            box-shadow: none;
            width: 100%;
        }

        .grid-wrapper {
            display: grid;
            grid-template-columns: auto auto auto auto auto auto;
            width: 700px;
            margin: 50px auto;
        }

        .grid-wrapper .grid-item {
            height: 85px;
            margin-top: 20px;
            position: relative;
            cursor: pointer;
        }

        .grid-wrapper .grid-item .icon {
            height: 65px;
            text-align: center;
        }

        .grid-wrapper .grid-item .title {
            text-align: center;
            color: #ccc;
        }

        .main-content {
            min-height: 600px;
            padding-left: 8px;
        }

        .add-group {
            display: grid;
            width: 700px;
            margin: 50px auto;
        }

        .add-form-btn {
            cursor: pointer;
            max-width: 150px;
        }

        .btn-action {
            display: none;
            width: 25px;
            height: 25px;
            padding: 0;
            z-index: 11;
        }

        .btn-edit {
            position: absolute;
            left: -5px;
            top: -5px;
        }

        .btn-delete {
            position: absolute;
            right: -5px;
            top: -5px;
        }

        .grid-item:hover .btn-action {
            display: block;
        }
        .my-close span{
            font-size: 24px;
        }
        .orange {
            background: #FFBA00;
            color: #000;
        }
        .orange:hover {
            background: #FFAC00;
        }
    </style>
@stop
@section('content')
    <h3 class="page-title">

    </h3>
    <div class="row main-content">
        <div class="portlet-body">
            <div class="left-content">
                <div class="top-left">
                    <div class="top-left-title">
                        <a href="#" style="color: #ccc">Cảnh</a>
                    </div>
                    <div class="top-left-divider">
                        <img alt=""
                             src="{!! asset(config('constants.divider_image')) !!}"/>
                    </div>
                </div>
                <div class="bottom-left">
                    <div class="swiper-container">
                        <div class="change-room prev-room">
                        <span class="glyphicon glyphicon-menu-up">
                        </span>
                        </div>
                        <ul class="swiper-wrapper">

                        </ul>
                        <div class="change-room next-room">
                        <span class="glyphicon glyphicon-menu-down">
                        </span>
                        </div>
                    </div>
                </div>
                <div class="action-bottom">
                    {{--<div class="aitem">--}}
                    {{--<a href="">Thêm</a>--}}
                    {{--</div>--}}
                    {{--<div class="aitem">--}}
                    {{--<a href="">Xóa</a>--}}
                    {{--</div>--}}
                </div>
            </div>
            <div class="grid-wrapper" style="text-transform: none">
                @foreach($groups as $group)
                    <div class="grid-item">
                        <button class="btn orange btn-circle btn-action btn-edit"><i class="fa fa-pencil"></i></button>
                        <button class="btn btn-danger btn-circle btn-action btn-delete" field-id="{!! $group->id !!}" data-toggle="modal" data-target="#modal-delete-warning"><span class="glyphicon glyphicon-remove"></span></button>
                        <div class="icon">
                            <img style="height: 60px" eid="{!! $group->id !!}"
                                 icon="{!! asset(config('constants.icon')).'/'.$group->icon !!}"
                                 image="{!! asset(config('constants.image')).'/'.$group->image !!}"
                                 image_off="{!! asset(config('constants.image')).'/'.$group->image_off !!}"
                                 @if($group->icon) src="{!! asset(config('constants.icon')).'/'.$group->icon !!}"
                                 @else src="{!! asset('public/images/choose-image.gif') !!}" @endif  alt="">
                        </div>
                        <div class="title">
                            {!! $group->name !!}
                        </div>
                    </div>
                @endforeach
                <div class="clearfix"></div>
            </div>
            <div class="add-group">
                <span style="font-size: 24px; color: #ccc; cursor: pointer" class="add-form-btn">Thêm cảnh</span>
            </div>
        </div>
    </div>
    {!! $setting_form !!}
    {!! $add_form !!}
    {!! $warning_delete !!};
@stop
@section('custom script')
    <script src="{!! asset('public/design/js/swiper.min.js') !!}"></script>
    <script>
        //        Lấy dữ liệu về các phòng
        var url_room_origin = '{!! route('admin.index') !!}' + '?rid=';
        var rooms = {!! json_encode($rooms) !!};
        var room_ids = [];
        var room_names = [];
        for (var index in rooms) {
            if (rooms.hasOwnProperty(index)) {
                var room = rooms[index];
                room_ids.push(room.id);
                room_names.push(room.name);
            }
        }
        var swiper = new Swiper('.swiper-container', {
            direction: 'vertical',
            slidesPerView: 10,
            centeredSlides: false,
            spaceBetween: 0,
            pagination: {
                el: '.swiper-pagination',
                type: 'fraction',
            },
            navigation: {
                nextEl: '.next-room',
                prevEl: '.prev-room'
            },
            virtual: {
                slides: (function () {
                    var slides = [];
                    for (var index in room_ids) {
                        if (room_ids.hasOwnProperty(index) && room_names.hasOwnProperty(index)) {
                            var url = url_room_origin + room_ids[index];
                            var active = '';
                            var elem = '<li ' + active + '><a href="' + url + '" title="' + room_names[index] + '">' + room_names[index] + '</a></a>';
                            slides.push(elem);
                        }
                    }
                    slides.push('<li></li>');
                    slides.push('<li></li>');
                    return slides;
                }()),
            },
        });
    </script>

    <script>
        $(function () {
            //add new group, check validator
            var modal_add = '#modal-add-group';
            var urlCheckDuplicate = '{!! route('admin.group.checkDuplicate') !!}';

            $('.close-modal-group').on('click', function () {
                $(modal_add).modal('hide');
            });
            $('.save-add-group').on('click', function () {
                var name = $(modal_add + ' input[name="name"]').val();
                var data = {name: name};
                checkErrorAndSubmitForm(modal_add, urlCheckDuplicate, data)
            });

            //update group, check validator
            var modal_update = '#modal-setting-group';
            $('.close-modal-group').on('click', function () {
                $(modal_update).modal('hide');
            });
            $('.save-group-update').on('click', function () {
                var name = $(modal_update + ' input[name="name"]').val();
                var id = $(modal_update + ' input[name="id"]').val();
                var data = {name: name, id: id};
                console.log('data', data);
                checkErrorAndSubmitForm(modal_update, urlCheckDuplicate, data)
            });
        });

        function checkErrorAndSubmitForm(modal, url, data) {
            if (data.name === '') {
                $(modal + ' .my-error').text('Tên không được để trống');
                return false;
            }
            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'json',
                data: data
            }).done(response => {
                if (response.state === 0) {
                    $(modal + '-form').submit();
                } else {
                    $(modal + ' .my-error').text(response.msg);
                }
            })
        }

        $(function () {
            $('.btn-edit').on('click', function () {
                var img = $(this).parent().find('img');
                var id = $(img).attr('eid');
                var icon = $(img).attr('icon');
                var image = $(img).attr('image');
                var image_off = $(img).attr('image_off');
                var name = $(img).parent().parent().find('.title').text().trim();
                var modal = $('#modal-setting-group');
                modal.find('input[name = "id"]').val(id);
                modal.find('input[name = "name"]').val(name);
                modal.find('#upfile-image-icon').attr('src', icon);
                modal.find('#upfile-image-on').attr('src', image);
                modal.find('#upfile-image-off').attr('src', image_off);
                modal.find('.my-error').text('');
                modal.modal('show');
            });

            $('.add-form-btn').click(function () {
                $('.my-error').text('');
                $('#modal-add-group').modal('show');
            });

            var delete_url = '';
            $('.btn-delete').on('click', function () {
                var id = $(this).attr('field-id');
                var url = '{!! $url_delete !!}' + '?id=' + id;
                delete_url = url;
            });
            $('.confirm-delete').on('click', function () {
                if (delete_url !== '')
                    window.location.replace(delete_url);
            });

            $('.close-modal').click(function () {
                $('.modal').modal('hide');
            });
        });

    </script>
@stop
<!DOCTYPE html>
<html lang="">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Real time</title>

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
		<div id="chat">
			@foreach($messages as $message)
				<p><strong>{{ $message->author }}</strong>: {{ $message->content }}</p>
			@endforeach
		</div>

		<form action="send-message" method="POST" role="form">
			{!! csrf_field() !!}
			<div class="form-group">
				<label for="">Name</label>
				<input type="text" class="form-control" id="" name="author" placeholder="Author">
			</div>

			<div class="form-group">
				<label for="">Content</label>
				<textarea name="content" class="form-control" rows="5"> </textarea>
			</div>
		</form>
		<button id="submit" class="btn btn-primary">Send</button>

		<!-- jQuery -->
		<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.0.4/socket.io.js"></script>
		<!-- Bootstrap JavaScript -->
		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
 		<script>
 			$('#submit').on('click', function(){
 				var url = "{{ route('postMessage') }}";
 				var author = $('input[name="author"]').val();
 				var content = $('textarea[name="content"]').val();
 				var _token = $('input[name="_token"]').val();
 				$.ajax({
 					type: 'POST',
 					url: url,
 					data:{
 						author: author,
 						content: content,
 						_token: _token
 					}
 				});

 			});
 			var socket = io('http://localhost:6001')
 			socket.on('chat:message', function(data){
 				var line = '<p><strong>'+data.author + '</strong>:'+ data.content + '</p>';
 				$('#chat').append(line);
 			})
 		</script>
	</body>
</html>
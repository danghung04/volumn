@extends('admin.index')
@section('custom css')
    <link rel="stylesheet" href="{!! asset('public/design/css/swiper.min.css') !!}">
    <link rel="stylesheet" href="{!! asset('public/design/css/setting.css') !!}" />
@stop
@section('content')
    <div class="content">
        <div class="cheader">
            <div class="menu-setting">
                <div class="setting-item">
                    <a href="#">
                        Cài đặt tầng
                    </a>
                </div>
                <div class="setting-item">
                    <a href="#">
                        Cài đặt phòng
                    </a>
                </div>
            </div>
            <div class="icon-wrapper">
                <div class="icon-item">
                    <div class="icon-symbol">
                        <img src="{!! asset('public/design/images/icon/den5b.png') !!}" />
                    </div>
                    <div class="icon-name">
                        Đèn ngủ
                    </div>
                </div>
                <div class="icon-item">
                    <div class="icon-symbol">
                        <img src="{!! asset('public/design/images/icon/den6b.png') !!}" />
                    </div>
                    <div class="icon-name">
                        Đèn bàn
                    </div>
                </div>
                <div class="icon-item">
                    <div class="icon-symbol">
                        <img src="{!! asset('public/design/images/icon/den7b.png') !!}" />
                    </div>
                    <div class="icon-name">
                        Đèn ngủ
                    </div>
                </div>
                <div class="icon-item">
                    <div class="icon-symbol">
                        <img src="{!! asset('public/design/images/icon/den8b.png') !!}" />
                    </div>
                    <div class="icon-name">
                        Đèn ngủ
                    </div>
                </div>
                <div class="icon-item">
                    <div class="icon-symbol">
                        <img src="{!! asset('public/design/images/icon/den9b.png') !!}" />
                    </div>
                    <div class="icon-name">
                        Đèn trần
                    </div>
                </div>
                <div class="icon-item">
                    <div class="icon-symbol">
                        <img src="{!! asset('public/design/images/icon/den10b.png') !!}" />
                    </div>
                    <div class="icon-name">
                        Đèn ngủ
                    </div>
                </div>
                <div class="icon-item">
                    <div class="icon-symbol">
                        <img src="{!! asset('public/design/images/icon/den11b.png') !!}" />
                    </div>
                    <div class="icon-name">
                        Đèn chùm
                    </div>
                </div>
            </div>
            <div class="progress-bar">
                <div class="phead">
                    <div class="circle" id="circle-alarm">
                        <strong>
                        </strong>
                    </div>
                    <div class="portion">
                        <span class="exist">15</span>/20
                    </div>
                </div>
                <div class="ptitle">
                    Hẹn giờ/Tổng
                </div>
            </div>
        </div>

            <div class="bottom-content">
                <div class="left-content">
                    <div class="top-left">
                        <div class="top-left-title">
                            Cảnh
                        </div>
                        <div class="top-left-divider">
                            <img alt="" src="https://lh3.google.com/u/0/d/1Htp4U4g8B-KB2_vf5UWELwuPjhubXfXC=w1366-h662-iv1">
                            </img>
                        </div>
                    </div>
                    <div class="bottom-left">
                        <div class="swiper-container">
                            <div class="change-room next-room">
                        <span class="glyphicon glyphicon-menu-up">
                        </span>
                            </div>
                            <ul class="swiper-wrapper">

                            </ul>
                            <div class="change-room prev-room">
                        <span class="glyphicon glyphicon-menu-down">
                        </span>
                            </div>
                        </div>
                    </div>
                    <div class="action-bottom">
                        <div class="aitem">
                            <a href="">Thêm</a>
                        </div>
                        <div class="aitem">
                            <a href="">Xóa</a>
                        </div>
                    </div>
                </div>
                <div class="right-content">
                    <img src="{!! asset('public/design/images/room.png') !!}">
                </div>
            </div>
    </div>
@stop
@section('custom script')
    <script crossorigin="anonymous" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-circle-progress/1.1.3/circle-progress.min.js">
    </script>
    <!-- progress bar circle -->
    <script>
        $(function(){
            var progressBarOptions = {
                startAngle: -1.55,
                size: 40,
                value: 0.75,
                fill: {
                    color: '#ffa500'
                }
            }

            $('.circle').circleProgress(progressBarOptions).on('circle-animation-progress', function(event, progress, stepValue) {
                //$(this).find('strong').text(String(stepValue.toFixed(2)).substr(1));
            });
            $('#circle-on').circleProgress({
                value : 0.75,
                fill: {
                    color: '#ffa500'
                }
            });
            $('#circle-alarm').circleProgress({
                value : 0.75,
                fill: {
                    color: '#0F0'
                }
            });
        });
    </script>
    <!--end progress bar circle -->
    <!-- Thêm slide cho một list -->
    <script src="{!! asset('public/design/js/swiper.min.js') !!}"></script>
    <script>
        var swiper = new Swiper('.swiper-container', {
            direction: 'vertical',
            slidesPerView: 10,
            centeredSlides: false,
            spaceBetween: 0,
            pagination: {
                el: '.swiper-pagination',
                type: 'fraction',
            },
            navigation: {
                nextEl: '.next-room',
                prevEl: '.prev-room',
            },
            virtual: {
                slides: (function () {
                    var slides = [];
                    for (var i = 0; i < 40; i ++) {
                        var elem = '<li><a href="#">Phỏng '+i+'</a></a>';
                        if(i >=38){
                            elem = '<li></li>';
                        }
                        slides.push(elem);
                    }
                    return slides;
                }()),
            },
        });
    </script>
@stop
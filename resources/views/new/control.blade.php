<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
    <title>
        Điều khiển
    </title>
    <link href="http://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css"/>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport"/>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
    <link crossorigin="anonymous" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" rel="stylesheet"/>

    <link crossorigin="anonymous" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"/>
    <!-- Thếm slide cho một list -->
    <link rel="stylesheet" href="{!! asset('public/design/css/swiper.min.css') !!}">
	<link rel="stylesheet" href="{!! asset('public/design/css/control.css') !!}" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js">
    </script>
    <script crossorigin="anonymous" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js">
    </script>

    <script crossorigin="anonymous" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-circle-progress/1.1.3/circle-progress.min.js">
    </script>
    <style>
    /*    .wrapper{
            background: green;
        }
        .swiper-wrapper{
            background: blue;
            position: relative;
        }*/
    </style>
</head>
<body>
    <div class="header">
        <div class="logo">
            <img alt="" src="https://lh3.google.com/u/0/d/1LXqXv4WUY8bCd3q2Wb_gs-3gBUiu5QSr=w1366-h662-iv1">
            </img>
        </div>
        <div class="info">
            <div class="i-item">
                <div class="time-text">
                    5:30 PM
                </div>
            </div>
            <div class="i-item">
                <div class="on">
                    <div class="phead">
                        <div class="circle" id="circle-on">
                            <strong>
                            </strong>
                        </div>
                        <div class="portion">
                            <span class="exist">15</span>/20
                        </div>
                    </div>
                    <div class="ptitle">
                        Hẹn giờ/Tổng
                    </div>
                </div>
            </div>
            <div class="i-item">
                <div class="on">
                    <div class="phead">
                        <div class="circle" id="circle-alarm">
                            <strong>
                            </strong>
                        </div>
                        <div class="portion">
                            <span class="exist">15</span>/20
                        </div>
                    </div>
                    <div class="ptitle">
                        Hẹn giờ/Tổng
                    </div>
                </div>
            </div>
            <div class="i-item">
                <div class="connect">
                    <div class="chead">
                        <div class="csymbol">
                            <img alt="" src="https://lh3.google.com/u/0/d/1ID9Ymbq-ad5NpGE_leItW11wPvSfHeDA=w1366-h662-iv1">
                            </img>
                        </div>
                        <div class="cstatus">
                            <img alt="" src="https://upload.wikimedia.org/wikipedia/commons/8/81/Button_Icon_GreenYellow.svg">
                            </img>
                        </div>
                    </div>
                    <div class="ctitle">
                        Kết nối
                    </div>
                </div>
            </div>
        </div>
        <div class="profile">
            <div class="dropdown">
                <button aria-expanded="true" aria-haspopup="true" class="dropdown-toggle" data-toggle="dropdown" id="dropdownMenu" type="button">
                    <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR-n1IDfTgcihvJXElAOzIx2DnWyT-hE6bmj4l_V00TrsnCn1H2"/>
                    <span class="caret">
                    </span>
                </button>
                <ul aria-labelledby="dropdownMenu" class="dropdown-menu dropdown-menu-right">
                    <li>
                        <a href="#">
                            <i class="fa fa-cogs">
                            </i>
                            Cài đặt
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <span class="glyphicon glyphicon-user">
                            </span>
                            Đổi avatar
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <span class="glyphicon glyphicon-home">
                            </span>
                            Đổi tên nhà
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="fa fa-exclamation-circle">
                            </i>
                            Thông tin
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <span class="glyphicon glyphicon-log-out">
                            </span>
                            Thông tin
                        </a>
                    </li>
                </ul>
            </div>
            <span class="name">
                Nhà 1
            </span>
        </div>
    </div>
    <div class="content">
        <div class="left-content">
            <div class="top-left">
                <div class="top-left-title">
                    Cảnh
                </div>
                <div class="top-left-divider">
                    <img alt="" src="https://lh3.google.com/u/0/d/1Htp4U4g8B-KB2_vf5UWELwuPjhubXfXC=w1366-h662-iv1">
                    </img>
                </div>
            </div>
            <div class="bottom-left">
                <div class="swiper-container">
                    <div class="change-room next-room">
                        <span class="glyphicon glyphicon-menu-up">
                        </span>
                    </div>
                    <ul class="swiper-wrapper">
                    
                    </ul>
                    <div class="change-room prev-room">
                        <span class="glyphicon glyphicon-menu-down">
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="right-content">
        	<img src="{!! asset('public/design/images/room.png') !!}">
        </div>
    </div>
    <!-- progress bar circle -->
    <script>
    	$(function(){
    		var progressBarOptions = {
				startAngle: -1.55,
				size: 50,
			    value: 0.75,
			    fill: {
					color: '#ffa500'
				}
			}

			$('.circle').circleProgress(progressBarOptions).on('circle-animation-progress', function(event, progress, stepValue) {
				//$(this).find('strong').text(String(stepValue.toFixed(2)).substr(1));
			});

			$('#circle-on').circleProgress({
				value : 0.75,
				fill: {
					color: '#ffa500'
				}
			});

			$('#circle-alarm').circleProgress({
				value : 0.75,
				fill: {
					color: '#0F0'
				}
			});
    	});
    	
    </script>
    <!--end progress bar circle -->
    <!-- Thêm slide cho một list -->
    <script src="{!! asset('public/design/js/swiper.min.js') !!}"></script>
    <script>
    var swiper = new Swiper('.swiper-container', {
      direction: 'vertical',
      slidesPerView: 10,
      centeredSlides: false,
      spaceBetween: 0,
      pagination: {
        el: '.swiper-pagination',
        type: 'fraction',
      },
      navigation: {
        nextEl: '.next-room',
        prevEl: '.prev-room',
      },
      virtual: {
        slides: (function () {
          var slides = [];
          for (var i = 0; i < 40; i ++) {
            var elem = '<li><a href="#">Phỏng '+i+'</a></a>';
            if(i >=38){
                elem = '<li></li>';
            }
            slides.push(elem);
          }
          return slides;
        }()),
      },
    });
  </script>
</body>
</html>

<style>
    .modal-dialog {
        background: #55619c;
    }

    .modal-content {
        padding: 5px;
        color: #fff;
        background-color: rgba(0,0,0,0.2);
    }

    .form-group > label {
        color: #fff;
    }

    .orange {
        background: #FFBA00;
    }

    .orange:hover {
        background: #da8c00;
    }

    .modal-header {
        border-width: 0;
    }
    .start-form label, .start-form a{
        color: #ccc;
        font-size: 20px;
        text-decoration: none;
        font-weight: normal;
    }
    .start-form a{
        cursor: pointer;
    }

    /*switch button*/
    /* Estilo iOS */
    .switch__container {
        margin: 5px auto;
        width: 26px;
    }

    .switch {
        visibility: hidden;
        position: absolute;
        margin-left: -9999px;
    }

    .switch + label {
        display: block;
        position: relative;
        cursor: pointer;
        outline: none;
        user-select: none;
    }

    .switch--shadow + label {
        padding: 2px;
        width: 26px;
        height: 10px;
        background-color: #dddddd;
        border-radius: 5px;
    }
    .switch--shadow + label:before,
    .switch--shadow + label:after {
        /*width: 25px;*/
        display: block;
        position: absolute;
        top: 1px;
        left: 1px;
        bottom: 1px;
        content: "";
    }
    .switch--shadow + label:before {
        right: 1px;
        width: 26px;
        height: 8px;
        background-color: #f1f1f1;
        border-radius: 5px;
        transition: background 0.4s;
    }
    .switch--shadow + label:after {
        width: 12px;
        height: 12px;
        top: -1px;
        background-color: #fff;
        border-radius: 100%;
        box-shadow: 0 2px 5px rgba(0, 0, 0, 0.3);
        transition: all 0.4s;
    }
    .switch--shadow:checked + label:before {
        background-color: #8ce196;
    }
    .switch--shadow:checked + label:after {
        transform: translateX(15px);
    }
    /*switch button*/
</style>
<div class="modal fade" id="modal-start">
    <div class="modal-dialog" style="width: 450px">
        <div class="modal-content">
            <div class="modal-header">
                <div class="my-close" data-dismiss="modal" aria-hidden="true" style="font-size: 16px"><span
                            class="glyphicon glyphicon-remove-circle pull-right" style="color: #fff"></span></div>
                <h4 class="modal-title text-center" style="color: #fff;text-transform: uppercase;font-size: 1.5em;
            font-weight: bold;">Hẹn giờ</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal start-form" action="#">
                    <div class="form-group">
                        <label for="alarm_status" class="col-md-8">
                            Bật hẹn giờ
                        </label>
                        <div class="col-md-4 text-right">
                            <div class="switch__container">
                                <input id="switch-shadow" class="switch switch--shadow" type="checkbox" checked>
                                <label for="switch-shadow"></label>
                            </div>
                            {{--<input type="radio" name="alarm_status" id="alarm_status">--}}
                        </div>
                    </div>
                    <div class="form-group">
                        <a class="col-md-8 show-modal-alarm" data-toggle="modal" data-target="modal-alarm">
                            Cài đặt hẹn giờ
                        </a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<style>
    .modal-dialog {
        background: #55619c;
    }

    .modal-content {
        padding: 5px;
        color: #fff;
    }

    .form-group > label {
        color: #fff;
    }

    .orange {
        background: #FFBA00;
    }

    .orange:hover {
        background: #da8c00;
    }

    .modal-footer {
        color: #000;
    }
    .my-close span{
        font-size: 24px;
    }

    .content-text {
        padding: 15px;
        text-align: center;
        font-size: 1.2em;
    }

    .modal-header {
        border-width: 0;
    }
    .alarm-on, .alarm-off{
        margin: 15px 0 5px 0;
    }
    .next-hour, .hour, .prev-hour,
    .next-minute, .minute, .prev-minute,
    .next-noon, .noon, .prev-noon{
        height: 40px;
        font-size: 20px;
    }
    
    /*custom checkbox*/
    .checkbox-item {
        display: block;
        position: relative;
        /*margin-bottom: 12px;*/
        cursor: pointer;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }

    /* Hide the browser's default checkbox */
    .checkbox-item input {
        position: absolute;
        opacity: 0;
        cursor: pointer;
    }

    /* Create a custom checkbox */
    .checkmark {
        position: absolute;
        top: 0;
        left: 0;
        height: 20px;
        width: 20px;
        background-color: #eee;
    }


    /* Create the checkmark/indicator (hidden when not checked) */
    .checkmark:after {
        content: "";
        position: absolute;
        display: none;
    }

    /* Show the checkmark when checked */
    .checkbox-item input:checked ~ .checkmark:after {
        display: block;
    }

    /* Style the checkmark/indicator */
    .checkbox-item .checkmark:after {
        left: 6px;
        top: 2px;
        width: 7px;
        height: 14px;
        border: solid orange;
        border-width: 0 3px 3px 0;
        -webkit-transform: rotate(45deg);
        -ms-transform: rotate(45deg);
        transform: rotate(45deg);
    }
    /*custom checkbox*/

    .loop-text{
        text-transform: uppercase;;
        font-size: 15px;
    }
</style>
<div class="modal fade" id="modal-alarm">
    <div class="modal-dialog" style="width: 800px">
        <div class="modal-content">
            <div class="modal-header">
                <div class="my-close" data-dismiss="modal" aria-hidden="true" style="font-size: 16px"><span
                            class="glyphicon glyphicon-remove-circle pull-right" style="color: #fff"></span></div>
                <h4 class="modal-title text-center" style="color: #fff;text-transform: uppercase;font-size: 1.5em;
            font-weight: bold;">Cài đặt hẹn giờ</h4>
            </div>
            <form class="form-horizontal" role = "form" method="POST" action="#">
                <div class="modal-body">
                    <div class="form-group">
                        <div class="col-md-6">
                            <label class="col-md-6">Giờ bật </label>
                            <div class="col-md-6 text-right">
                                <div class="switch__container">
                                    <input id="switch-shadow-on" class="switch switch--shadow" type="checkbox" checked>
                                    <label for="switch-shadow-on"></label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label class="col-md-6">Giờ tắt </label>
                            <div class="col-md-6 text-right">
                                <div class="switch__container">
                                    <input id="switch-shadow-off" class="switch switch--shadow" type="checkbox" checked>
                                    <label for="switch-shadow-off"></label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 text-center alarm-on">
                            <div class="col-md-8 col-md-offset-2">
                                <div class="col-md-4">
                                    <div class="next-hour">
                                        <span class="glyphicon glyphicon-menu-up"></span>
                                    </div>
                                    <div class="hour">
                                        6
                                    </div>
                                    <div class="prev-hour">
                                        <span class="glyphicon glyphicon-menu-down"></span>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="next-minute">
                                        <span class="glyphicon glyphicon-menu-up"></span>
                                    </div>
                                    <div class="minute">
                                        52
                                    </div>
                                    <div class="prev-minute">
                                        <span class="glyphicon glyphicon-menu-down"></span>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="next-noon">
                                        <span class="glyphicon glyphicon-menu-up"></span>
                                    </div>
                                    <div class="noon">
                                        AM
                                    </div>
                                    <div class="prev-noon">
                                        <span class="glyphicon glyphicon-menu-down"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 text-center alarm-off">
                            <div class="col-md-8 col-md-offset-2">
                                <div class="col-md-4">
                                    <div class="next-hour">
                                        <span class="glyphicon glyphicon-menu-up"></span>
                                    </div>
                                    <div class="hour">
                                        6
                                    </div>
                                    <div class="prev-hour">
                                        <span class="glyphicon glyphicon-menu-down"></span>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="next-minute">
                                        <span class="glyphicon glyphicon-menu-up"></span>
                                    </div>
                                    <div class="minute">
                                        52
                                    </div>
                                    <div class="prev-minute">
                                        <span class="glyphicon glyphicon-menu-down"></span>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="next-noon">
                                        <span class="glyphicon glyphicon-menu-up"></span>
                                    </div>
                                    <div class="noon">
                                        AM
                                    </div>
                                    <div class="prev-noon">
                                        <span class="glyphicon glyphicon-menu-down"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group" style="height: 80px; border-top: 2px solid #555; padding-top: 15px; width: 85%; margin:auto">
                        <div class="col-md-2 col-md-offset-1 loop-text">
                            Lặp lại
                        </div>
                        <div class="col-md-1">
                            <label>T2</label>
                            <div class="my-check">
                                <label class="checkbox-item">
                                    <input type="checkbox" checked="checked">
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                        <div class="col-md-1">
                            <label>T3</label>
                            <div class="my-check">
                                <label class="checkbox-item">
                                    <input type="checkbox" checked="checked">
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                        <div class="col-md-1">
                            <label>T4</label>
                            <div class="my-check">
                                <label class="checkbox-item">
                                    <input type="checkbox" checked="checked">
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                        <div class="col-md-1">
                            <label>T5</label>
                            <div class="my-check">
                                <label class="checkbox-item">
                                    <input type="checkbox" checked="checked">
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                        <div class="col-md-1">
                            <label>T6</label>
                            <div class="my-check">
                                <label class="checkbox-item">
                                    <input type="checkbox" checked="checked">
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                        <div class="col-md-1">
                            <label>T7</label>
                            <div class="my-check">
                                <label class="checkbox-item">
                                    <input type="checkbox" checked="checked">
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                        <div class="col-md-1">
                            <label>CN</label>
                            <div class="my-check">
                                <label class="checkbox-item">
                                    <input type="checkbox" checked="checked">
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-12 text-center">
                        <button class="btn btn-danger">Xóa hẹn giờ</button>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="col-xs-6">
                        <button class="btn orange pull-left close-modal">Hủy</button>
                    </div>
                    <div class="col-xs-6">
                        <button class="btn orange confirm-delete">Lưu</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
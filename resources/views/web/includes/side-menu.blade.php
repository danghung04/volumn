<div class="page-sidebar-wrapper">
	<div class="page-sidebar navbar-collapse collapse">
		<!-- BEGIN SIDEBAR MENU -->
		<ul class="page-sidebar-menu page-sidebar-menu-light " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
			<li @if(request()->is('admin/area')) class="active" @endif>
				<a href="{{ route('admin.area.index') }}">
					<i class="icon-pointer"></i>
					<span class="title">Quản lý khu vực</span>
					<span class="selected"></span>
				</a>
			</li>
			<li @if(request()->is('admin/building')) class="active" @endif>
				<a href="{{ route('admin.building.index') }}">
					<i class="icon-bar-chart"></i>
					<span class="title">Quản lý tòa nhà</span>
					<span class="selected"></span>
				</a>
			</li>
			<li @if(request()->is('admin/room')) class="active" @endif>
				<a href="{{ route('admin.room.index') }}">
					<i class="icon-home"></i>
					<span class="title">Quản lý phòng</span>
					<span class="selected"></span>
				</a>
			</li>
			<li @if(request()->is('admin/equipment')) class="active" @endif>
				<a href="{{ route('admin.equipment.index') }}">
					<i class="icon-settings"></i>
					<span class="title">Quản lý thiết bị</span>
					<span class="selected"></span>
				</a>
			</li>
			<li @if(request()->is('admin/user')) class="active" @endif>
				<a href="#">
					<i class="icon-user"></i>
					<span class="title">Quản lý người dùng</span>
					<span class="selected"></span>
				</a>
			</li>
		</ul>
	</div>
</div>
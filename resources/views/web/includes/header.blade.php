<div class="page-header md-shadow-z-1-i navbar navbar-fixed-top">
	<div class="page-header-inner">
		<!-- BEGIN LOGO -->
		<div class="page-logo" style='padding-top: 10px'>
			@if(Auth::user()->role == config('constants.admin_role'))
				<a href="{{ route('admin.index') }}" style="color:white;">Trang config</a>
				<div class="menu-toggler sidebar-toggler hide">
				</div>
			@endif
		</div>
		<a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
		</a>
		<div class="top-menu">
			<ul class="nav navbar-nav pull-right">
				<!-- END TODO DROPDOWN -->
				<!-- BEGIN USER LOGIN DROPDOWN -->
				<li class="dropdown dropdown-user">
					<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
					<img alt="" class="img-circle" src="{!! asset('public/theme/admin/layout/img/avatar3_small.jpg') !!}"/>
					<span class="username username-hide-on-mobile">
					@if(!empty(Auth::user()))
						{!! Auth::user()->name !!}
					@endif</span>
					<i class="fa fa-angle-down"></i>
					</a>
					<ul class="dropdown-menu dropdown-menu-default">						
						<li>
							<a href="{{ route('logout') }}">
							<i class="icon-key"></i>Đăng xuất</a>
						</li>
					</ul>
				</li>				
			</ul>
		</div>
		<!-- END TOP NAVIGATION MENU -->
	</div>
</div>
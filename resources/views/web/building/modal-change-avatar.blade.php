<style>
    .modal-content {
        padding: 5px;
        color: #fff;
    }

    .form-group > label {
        color: #fff;
    }

    .orange {
        background: #FFBA00;
    }

    .orange:hover {
        background: #da8c00;
    }

    .modal-footer {
        color: #000;
    }

    .my-close {
        cursor: pointer;
    }

    #modal-change-avatar img{
        height: 50px;
    }
    .image-area{
        overflow: hidden;
    }
    #file-avatar {
        font-size: 20px;
        max-width: 50px;
    }

    .imagediv {
        float: left;
        margin-top: 50px;
    }

    .imagediv .showonhover {
        background: red;
        padding: 20px;
        opacity: 0.9;
        color: white;
        width: 100%;
        display: block;
        text-align: center;
        cursor: pointer;
    }
</style>
<div class="modal fade" id="modal-change-avatar">
    <div class="modal-dialog" style="width: 280px;margin-top: 125px;margin-right: 191px;">
        <div class="modal-content">
            <div class="modal-header">
                <div class="my-close" data-dismiss="modal" aria-hidden="true"><span
                            class="glyphicon glyphicon-remove-circle pull-right" style="color: #fff"></span></div>
            </div>
            <form class="form-horizontal" id="modal-change-avatar-form" action="{!! route('changeAvatar') !!}"
                  method="POST" enctype="multipart/form-data">
                {!! csrf_field() !!}
                <div class="form-body" style="margin-top: 15px">
                    <div class="form-group">
                        <label class="col-md-5 control-label my-label" style="font-weight: lighter; padding-top: 0; text-transform: none">Avatar</label>
                        <div class="col-md-5 image-area">
                            <input accept="image/*" capture="" id="file-avatar" name="avatar" style="display:none"
                                   type="file"/>
                            @if(!empty(Auth::user()->avatar))
                                @if(@getimagesize(asset(config('custom.upload').Auth::user()->avatar)))
                                    <img id="upfile-avatar" style="height: 75px; background:#fffcfb; cursor: pointer"
                                         src="{{ asset(config('constants.image').Auth::user()->avatar) }}">
                                @else
                                    <img id="upfile-avatar" style="height: 75px; background:#fffcfb; cursor: pointer"
                                         src="{{ asset(config('constants.default_avatar')) }}">
                                @endif
                            @else
                                <img id="upfile-avatar" style="height: 75px; background:#fffcfb; cursor: pointer"
                                     src="{{ asset(config('constants.default_avatar')) }}">
                            @endif
                        </div>
                    </div>
                </div>
            </form>
            <div class="modal-footer" style="padding-top: 0">
                <div class="col-xs-6">
                    <button class="btn orange pull-left cancel-modal">Hủy</button>
                </div>
                <div class="col-xs-6">
                    <button class="btn orange save-change-avatar">Lưu</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(e) {
        $(".showonhover").click(function(){
            $("#selectfile_avatar").trigger('click');
        });
        $("#upfile-avatar").click(function () {
            $("#file-avatar").trigger('click');
        });
    });


</script>
<script type="text/javascript">
    function clickToRenderImage(obj, _target){
        $(obj).change(function () {
            readURL(this);
            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $(_target).attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
        });
    }
    clickToRenderImage('#file-avatar','#upfile-avatar');
</script>
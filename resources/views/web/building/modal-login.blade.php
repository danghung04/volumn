<link href="{!! asset('public/design/css/login-modal.css') !!}" rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
<style>
    input:-webkit-autofill {
        -webkit-box-shadow: inset 0 0 0px 9999px #041C38;
        -webkit-text-fill-color: #eee;
    }
    body{

    }
    a{
        text-decoration: none;
    }
    span.change-p-type{
        position: absolute;
        color: #bbb;
        right: 15px;
        cursor: pointer;
        font-size: 1.2em;
        top: 15px;
    }
    .footer{
        color: rgba(180, 180, 180, 0.9);
        padding: 15px;
        text-align: center;
        font-size: 15px;
    }
</style>

<div class="modal fade" id="modal-login">
    <div class="modal-dialog" style="width: 300px">
        <div class="modal-content">
            <div class="modal-header">
                <div class="my-close" data-dismiss="modal" aria-hidden="true"><span
                            class="glyphicon glyphicon-remove-circle pull-right" style="color: #fff"></span></div>
                <h4 class="modal-title text-center" style="color: #fff;text-transform: uppercase;font-size: 2em;
            font-weight: bold;">Đăng nhập</h4>
            </div>
            <div class="contact-form">
                <form class="form" name="contact_form"  method="POST" action="{{ url('/login-setting') }}">
                    <ul>
                        <li class="input">
                            <label><img src="{!! asset('public/design/images/contact.png') !!}" alt=""></label>
                            <input type="email" class="email" name="email" placeholder="Tên đăng nhập" required />
                            @if ($errors->has('email'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </li>
                        <li class="input">
                            <label><img src="{!! asset('public/design/images/lock.png') !!}" alt=""></label>
                            <div class="password-field">
                                <input type="password" name="password" placeholder="Mật khẩu" required />
                                {{--<span class="change-p-type fas fa-eye"></span>--}}
                            </div>

                            @if ($errors->has('password'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif
                        </li>
                    </ul>
                    <div class="clear"></div>
                    <div class="remember">
                        <input type="checkbox">
                        <label for=""style="text-transform: none; font-weight: lighter">Ghi nhớ tài khoản</label>
                    </div>
                    <div>
                        <button class="btn btn-block">Đăng nhập</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
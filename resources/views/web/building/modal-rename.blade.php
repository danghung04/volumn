<style>

    .modal-content {
        padding: 5px;
        color: #fff;
    }
    .btn.orange{
        text-transform: uppercase;
        color: #000;
    }

    .form-group > label {
        color: #fff;
    }

    .orange {
        background: #FFBA00;
    }

    .orange:hover {
        background: #da8c00;
    }

    .modal-footer {
        color: #000;
    }

    .my-close {
        cursor: pointer;
    }
    #file-image {
        font-size: 20px;
        height:50px;
    }
    .imagediv {
        float:left;
        margin-top:50px;
    }
    .imagediv .showonhover {
        background:red;
        padding:20px;
        opacity:0.9;
        color:white;
        width: 100%;
        display:block;
        text-align:center;
        cursor:pointer;
    }
</style>
<div class="modal fade" id="modal-rename-building">
    <div class="modal-dialog" style="width: 320px;margin-top: 125px;margin-right: 191px;">
        <div class="modal-content">
            <div class="my-close" data-dismiss="modal" aria-hidden="true"><span
                            class="glyphicon glyphicon-remove-circle pull-right" style="color: #fff; margin-top: 7px; margin-right: 7px;"></span></div>
            <form class="form-horizontal" id="modal-rename-building-form" action="{!! route('changeInfo') !!}"
                  method="POST" enctype="multipart/form-data">
                {!! csrf_field() !!}
                <input type="hidden" name="id" value="{!! $building_info->id !!}">
                <div class="form-body" style="margin-top: 15px">
                    <div class="form-group">
                        <label class="col-xs-4 control-label my-label" style="font-weight: lighter; padding-top: 15px; text-transform: none">Tên nhà</label>
                        <div class="col-xs-8">
                            <input type="text" name="name" style="background: transparent; border: 0; color: #ccc; box-shadow: none; border-bottom: 2px solid #666"
                                   class="form-control" value="{!! $building_info->name !!}" />
                        </div>
                    </div>
                </div>
            </form>
            <div class="modal-footer" style="padding-top: 0">
                <div class="col-xs-6">
                    <button class="btn orange pull-left cancel-modal">Hủy</button>
                </div>
                <div class="col-xs-6">
                    <button class="btn orange save-rename-building">Lưu</button>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    .modal-dialog {
        /*background: #55619c;*/
        background: rgba(0, 0, 0, 0.5)
    }

    .modal-content {
        padding: 5px;
        color: #fff;
    }

    .form-group > label {
        color: #fff;
    }

    .orange {
        background: #FFBA00;
    }

    .orange:hover {
        background: #da8c00;
    }

    .modal-footer {
        color: #000;
    }

    .my-close {
        cursor: pointer;
    }

    #file-image {
        font-size: 20px;
        height: 50px;
    }

    .imagediv {
        float: left;
        margin-top: 50px;
    }

    .imagediv .showonhover {
        background: red;
        padding: 20px;
        opacity: 0.9;
        color: white;
        width: 100%;
        display: block;
        text-align: center;
        cursor: pointer;
    }

    #modal-info-building .form-group {
        margin-top: 30px;
        padding-bottom: 5px;
    }

    #modal-info-building .form-group label {
        font-size: 15px;
        font-weight: lighter;
        text-transform: uppercase;
    }
</style>
<div class="modal fade" id="modal-info-building">
    <div class="modal-dialog" style="width: 580px;">
        <div class="modal-content">
            <div class="modal-header">
                <div class="my-close" data-dismiss="modal" aria-hidden="true"><span
                            class="glyphicon glyphicon-remove-circle pull-right" style="color: #fff"></span></div>
                <h4 class="modal-title text-center" style="color: #fff;text-transform: uppercase;font-size: 2em;
            font-weight: bold;">Thông tin</h4>
            </div>
            <div class="my-divider" style="background: #666; width: 86%; margin:auto; height: 3px;">
            </div>
            <form class="form-horizontal" id="modal-info-building-form" action="{!! route('changeInfo') !!}"
                  method="POST" enctype="multipart/form-data">
                {!! csrf_field() !!}
                <input type="hidden" name="id" value="{!! $building_info->id !!}">
                <div class="form-body" style="margin-top: 15px; padding: 0 50px 50px 50px ">
                    <div class="form-group" style="border-bottom: 1px solid #666">
                        <label class="col-md-4 control-label my-label">Tên nhà</label>
                        <div class="col-md-8 pull-right text-right">
                            <input type="text" name="name"
                                   style="background: transparent; border: 0; text-align: right; color: #ccc; box-shadow: none;"
                                   class="form-control" value="{!! $building_info->name !!}"  readonly />
                        </div>
                    </div>
                    <div class="form-group" style="border-bottom: 1px solid #666">
                        <label class="col-md-4 control-label my-label">MAC</label>
                        <div class="col-md-8 pull-right text-right">
                            <input type="text" name="mac"
                                   style="background: transparent; border: 0; text-align: right; color: #ccc; box-shadow: none;"
                                   class="form-control" value="{!! $building_info->mac !!}" readonly />
                        </div>
                    </div>

                    <div class="form-group" style="border-bottom: 1px solid #666">
                        <label class="col-md-4 control-label my-label">Trạng thái</label>
                        <div class="col-md-8 pull-right text-right">
                            <span class="building-status" style="text-transform: none; font-weight: normal; color: #ccc">Đã kết nối</span>
                        </div>
                    </div>
                    <div class="form-group" style="border-bottom: 1px solid #666">
                        <label class="col-md-4 control-label my-label">Phiên bản</label>
                        <div class="col-md-8 pull-right text-right">
                            <input type="text" name="version"
                                   style="background: transparent; border: 0; text-align: right; color: #ccc; box-shadow: none;"
                                   class="form-control" value="{!! $building_info->version?$building_info->version:'1.5' !!}" readonly/>
                        </div>
                    </div>

                    <div class="form-group" style="border-bottom: 1px solid #666">
                        <label class="col-md-4 control-label my-label">Website</label>
                        <div class="col-md-8 pull-right text-right">
                            <input type="text" name="website"
                                   style="background: transparent; border: 0; text-align: right; color: #ccc; box-shadow: none;"
                                   class="form-control" value="{!! !$building_info->website?$building_info->website:'oritek.vn' !!}" readonly/>
                        </div>
                    </div>

                    <div class="form-group" style="border-bottom: 1px solid #666">
                        <label class="col-md-4 control-label my-label">Góp ý</label>
                        <div class="col-md-8 pull-right text-right">
                            <input type="text" name="feedback"
                                   style="background: transparent; border: 0; text-align: right; color: #ccc; box-shadow: none;"
                                   class="form-control" value="{!! !$building_info->feedback?$building_info->feedback:'info@oritek.vn' !!}" readonly/>
                        </div>
                    </div>
                </div>
            </form>
            <div class="modal-footer" style="padding-top: 0">

            </div>
        </div>
    </div>
</div>
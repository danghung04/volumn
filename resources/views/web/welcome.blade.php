<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
    <head>
        <title>
            Điều khiển
        </title>
        <link href="http://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css"/>
        <meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport"/>
        <meta content="text/html; charset=utf-8" http-equiv="Content-Type"/>
        <link href="{!! asset('public/theme/global/plugins/bootstrap/css/bootstrap.css') !!}" rel="stylesheet"
              type="text/css"/>

        <link href="{!! asset('public/theme/global/plugins/font-awesome/css/font-awesome.min.css') !!}" rel="stylesheet"
              type="text/css"/>

        <!-- Thếm slide cho một list -->
        <link rel="stylesheet" href="{!! asset('public/design/css/swiper.min.css') !!}">
        <link rel="stylesheet" href="{!! asset('public/design/css/welcome.css') !!}"/>
        <link rel="icon" href="{!! asset('public/images/favicon.png') !!}"  />
        <script src="{{ asset('public/web/js/plugin/jquery.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('public/web/js/plugin/jquery-ui.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('public/web/js/plugin/bootstrap.min.js') }}" type="text/javascript"></script>
        <link rel="stylesheet" href="{!! asset(config('constants.plugin').'scroller/jquery.mCustomScrollbar.css') !!}"/>
        <script crossorigin="anonymous"
                src="{{ asset('public/design/js/circle-progress.min.js') }}">
        </script>
        <style>
            html{
                background: linear-gradient(-5deg, rgb(04,22,56),rgb(42,36,61)); no-repeat;
                box-sizing: content-box;
                /*overflow:auto;*/
            }
            body {
                text-transform: uppercase;
            }

            .my-wrapper {
                width: 1362px;
                overflow: auto;
            }
            @media(max-width: 1362px){
                .my-wrapper{
                    background: linear-gradient(-5deg, rgb(04,22,56),rgb(42,36,61)) no-repeat;
                }
            }

            #frame {
                position: relative;
                z-index: 4;
            }

            #frame .icon-symbol img {
                width: 35px;
            }

            a.text-room-drag-update {
                color: #ccc;
                text-decoration: none;
            }

            a.text-room-drag-update:hover {
                color: #ccc;
            }

            .swiper-slide li.active {
                background: rgba(28, 28, 72, 0.5);
            }

            .icon-drag-update {
                text-align: center;
            }

            .e-text {
                padding: 3px;
                color: #eee;
            }

            .text-room {
                cursor: pointer;
                z-index: 5;
                position: absolute;
                font-size: 18px;
                color: lawngreen;
            }

            .setting-item .text-room {
                position: static;
            }

            .swiper-slide {
                overflow: hidden;
            }

            .swiper-slide li.active {
                background: rgba(28, 28, 72, 0.5);
            }

            .modal-dialog form .form-group label {
                text-transform: uppercase;
            }
        </style>
    </head>
    <body>
        <div class="my-wrapper">
            <div class="header">
                <div class="logo">
                    <img alt="Logo" src="{!! asset('public/images/logo.png') !!}"/>
                </div>
                <div class="info">
                    <div class="i-item">
                        <div class="time-text" id="clock">
                            00:00 AN
                        </div>
                    </div>
                    <div class="i-item">
                        <div class="on">
                            <div class="phead">
                                <div class="circle" id="circle-on">
                                    <strong>
                                    </strong>
                                </div>
                                <div class="portion">
                                    <span class="exist">{!! $count_equip_on !!}</span>/{!! $total_equip !!}
                                </div>
                            </div>
                            <div class="ptitle">
                                Đang bật/Tổng
                            </div>
                        </div>
                    </div>
                    <div class="i-item">
                        <div class="on">
                            <div class="phead" data-toggle="modal" data-target="#modal-start">
                                <div class="circle" id="circle-alarm">
                                    <strong>
                                    </strong>
                                </div>
                                <div class="portion">
                                    <span class="exist">{!! $count_equip_alarm !!}</span>/{!! $total_equip !!}
                                </div>
                            </div>
                            <div class="ptitle">
                                Hẹn giờ/Tổng
                            </div>
                        </div>
                    </div>
                    <div class="i-item">
                        <div class="connect">
                            <div class="chead">
                                <div class="csymbol">
                                    <img alt=""
                                         src="{!! asset(config('constants.connection_image')) !!}"/>
                                    <div class="cstatus">
                                        <img alt=""
                                             src="https://upload.wikimedia.org/wikipedia/commons/8/81/Button_Icon_GreenYellow.svg"/>
                                    </div>
                                </div>
                            </div>
                            <div class="ctitle">
                                Kết nối
                            </div>
                        </div>
                    </div>
                </div>
                <div class="profile">
                    <div class="dropdown">
                        <button aria-expanded="true" aria-haspopup="true" class="dropdown-toggle" data-toggle="dropdown"
                                id="dropdownMenu" type="button">
                            @if(!empty(Auth::user()->avatar))
                                @if(@getimagesize(asset(config('constants.image').Auth::user()->avatar)))
                                    <img src="{{ asset(config('constants.image').Auth::user()->avatar) }}">
                                @else
                                    <img src="{{ asset(config('constants.default_avatar')) }}">
                                @endif
                            @else
                                <img src="{{ asset(config('constants.default_avatar')) }}">
                            @endif
                            {{--<img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR-n1IDfTgcihvJXElAOzIx2DnWyT-hE6bmj4l_V00TrsnCn1H2"/>--}}
                            <span class="caret">
                    </span>
                        </button>
                        <ul aria-labelledby="dropdownMenu" class="dropdown-menu dropdown-menu-right">
                            <li>
                                <a href="#" class="open-login-modal">
                                    <i class="fa fa-cogs">
                                    </i>
                                    Cài đặt
                                </a>
                            </li>
                            <li>
                                <a href="#" class="open-change-avatar-modal">
                            <span class="glyphicon glyphicon-user">
                            </span>
                                    Đổi avatar
                                </a>
                            </li>
                            <li>
                                <a href="#" class="open-rename-building-modal">
                            <span class="glyphicon glyphicon-home">
                            </span>
                                    Đổi tên nhà
                                </a>
                            </li>
                            <li>
                                <a href="#" class="open-info-building-modal">
                                    <i class="fa fa-exclamation-circle">
                                    </i>
                                    Thông tin
                                </a>
                            </li>
                            <li>
                                <a href="{!! route('logout') !!}">
                            <span class="glyphicon glyphicon-log-out">
                            </span>
                                    Đăng xuất
                                </a>
                            </li>
                        </ul>
                    </div>
                    <span class="name" style="text-transform: uppercase; font-size: 18px">
                    @if(Auth::check())
                            {!! Auth::user()->name !!}
                        @else
                            Nhà 1
                        @endif
            </span>
                </div>
            </div>
            <div class="content">
                <div class="left-content">
                    <div class="top-left anchor">
                        <div class="top-left-title">
                            <a href="{!! route('web.group') !!}" style="color: #ccc">Cảnh</a>
                        </div>
                        <div class="top-left-divider">
                            <img alt=""
                                 src="{!! asset(config('constants.divider_image')) !!}"/>
                        </div>
                    </div>
                    <div class="bottom-left">
                        <div class="swiper-container">
                            <div class="change-room prev-room">
                        <span class="glyphicon glyphicon-menu-up">
                        </span>
                            </div>
                            <ul class="swiper-wrapper">

                            </ul>
                            <div class="change-room next-room">
                        <span class="glyphicon glyphicon-menu-down">
                        </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="right-content" id="frame">
                    <div class="background-wrapper" style="position: relative">
                        @if($room_active)
                            @if(!empty($room_active->image))
                                @if(@getimagesize(asset(config('constants.image').$room_active->image)))
                                    <img src="{{ asset(config('constants.image').$room_active->image) }}">
                                @else
                                    {{--<img src="{!! asset('public/design/images/room.png') !!}">--}}
                                @endif
                            @else
                                {{--<img src="{!! asset('public/design/images/room.png') !!}">--}}
                            @endif
                        @endif
                        @foreach($equipments as $equipment)
                            <div class="icon-symbol icon-drag-update" id="equipment-{!! $equipment->id !!}"
                                 equipment-id="{!! $equipment->id !!}"
                                 iotdevice_id="{!! $equipment->iotdevice_id !!}"
                                 name_equip="{!! $equipment->name_equip !!}"
                                 stt="{!! $equipment->ordered_at !!}"
                                 state="{!! $equipment->status !!}"
                                 id_floor="{!! $equipment->room_id !!}"
                                 style="position: absolute; left: {!! 1.2*$equipment->x_offset !!}px; top: {!! 1.2*$equipment->y_offset !!}px;">
                                <div class="image">
                                    <img
                                            src="{!! asset(config('constants.image').$equipment->active_image) !!}"
                                            image="{!! asset(config('constants.image').$equipment->image) !!}"
                                            image_off="{!! asset(config('constants.image').$equipment->image_off) !!}"
                                    />

                                </div>
                                <div class="e-text">
                                    {!! $equipment->name_equip !!}
                                </div>
                            </div>
                        @endforeach
                        @foreach($sub_rooms as $sub_room)
                            <div class="text-room text-room-drag-update" sub-room-id='{!! $sub_room->id !!}'
                                 style="left: {!! $sub_room->x_offset !!}px; top: {!! $sub_room->y_offset !!}px;">
                                <span>{!! $sub_room->name !!}</span>
                            </div>
                        @endforeach
                    </div>
                    {{--<img src="{!! asset('public/design/images/room.png') !!}">--}}
                </div>
            </div>
        </div>
    {!! $modal_start !!}
    {!! $modal_alarm !!}
    {!! $modal_rename_building !!}
    {!! $modal_info_building !!}
    {!! $modal_login !!}
    {!! $modal_change_avatar !!}
    <?php if($total_equip == 0) $total_equip = 1 ?>
    <!-- progress bar circle -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.0.4/socket.io.js"></script>

        <script>
            $(function () {
//                var url = "http://34.229.9.67:2021";
                var url_server = '{!! url('/') !!}';
                var param = url_server.split(':');
                var url = param[0] + ':' + param[1] + ':2021';
                var socket = io(url);

                socket.on('responseEquipment', function (data) {
                    console.log(data);
                });
                socket.on('responseGroup', function (data) {
                    console.log(data);
                });
                socket.on('response', function (data) {
                    console.log(data);
                    updateEquipmentStatus(data);
                });
                $('.icon-drag-update').click(function (e) {
                    var object = $(this);
                    var state = 1;
                    if ($(object).attr('state') === '1')
                        state = 0;
                    var sendObject = {
                        iotdevice_id: $(object).attr('iotdevice_id'),
                        icon_off: $(object).find('img').attr('image_off'),
                        icon_on: $(object).find('img').attr('image'),
                        name_equip: $(object).attr('name_equip'),
                        id_equip: $(object).attr('equipment-id'),
                        id_floor: $(object).attr('id_floor'),
                        stt: $(object).attr('stt'),
                        state: state,
                    };
                    socket.emit('request', sendObject);
                    console.log(sendObject);
                });

                function updateEquipmentStatus(data) {
                    var _token = "{!! csrf_token() !!}";
                    var update_status_url = "{!!route('web.equipment.updateStatus') !!}";
                    var iotdevice_id = data.iotdevice_id;
                    var status = data.status;
                    var position = data.position;
                    $.ajax({
                        url: update_status_url,
                        type: 'POST',
                        dataType: 'json',
                        data: {
                            _token: _token,
                            iotdevice_id: iotdevice_id,
                            status: status,
                            ordered_at: position
                        },
                    }).done(function (data) {
                        console.log(data);
                        if (data.id) {
                            var eObject = $('#equipment-' + data.id);
                            var imgObject = eObject.find('img');
                            var image = imgObject.attr('image');
                            var image_off = imgObject.attr('image_off');
                            var src = image_off;
                            if (data.status === '1') {
                                src = image;
                                eObject.attr('state', 1);
                            } else {
                                eObject.attr('state', 0);
                            }
                            imgObject.attr('src', src);
                        }
                    });
                }
            });
        </script>
        <script>
            $(function () {
                var progressBarOptions = {
                    startAngle: -1.55,
                    size: 50,
                    value: 0.75,
                    fill: {
                        color: '#ffa500'
                    }
                }

                $('.circle').circleProgress(progressBarOptions).on('circle-animation-progress', function (event, progress, stepValue) {
                    //$(this).find('strong').text(String(stepValue.toFixed(2)).substr(1));
                });

                var percent_on = '{!! $count_equip_on/$total_equip !!}';
                var percent_alarm = '{!! $count_equip_alarm/$total_equip !!}';
                $('#circle-on').circleProgress({
                    value: percent_on,
                    fill: {
                        color: '#ffa500'
                    }
                });

                $('#circle-alarm').circleProgress({
                    value: percent_alarm,
                    fill: {
                        color: '#0F0'
                    }
                });
            });

        </script>
        <!--end progress bar circle -->
        <!-- Thêm slide cho một list -->
        <script src="{!! asset('public/design/js/swiper.min.js') !!}"></script>
        <script>
            var url_room_origin = '{!! route('web.index') !!}' + '?rid=';
            var room_active_id = {!! !empty($room_active)?$room_active->id:'' !!};
            var rooms = {!! json_encode($rooms) !!};
            var room_ids = [];
            var room_names = [];
            for (var index in rooms) {
                if (rooms.hasOwnProperty(index)) {
                    var room = rooms[index];
                    room_ids.push(room.id);
                    room_names.push(room.name);
                }
            }

            var swiper = new Swiper('.swiper-container', {
                direction: 'vertical',
                slidesPerView: 10,
                centeredSlides: false,
                spaceBetween: 0,
                pagination: {
                    el: '.swiper-pagination',
                    type: 'fraction',
                },
                navigation: {
                    nextEl: '.next-room',
                    prevEl: '.prev-room',
                },
                virtual: {
                    slides: (function () {
                        var slides = [];
                        for (var index in room_ids) {
                            if (room_ids.hasOwnProperty(index) && room_names.hasOwnProperty(index)) {
                                var url = url_room_origin + room_ids[index];
                                var active = '';
                                if (room_ids[index] === room_active_id) {
                                    active = 'class="active"';
                                }
                                var elem = '<li ' + active + '><a href="' + url + '" title="'+ room_names[index] +'">' + room_names[index] + '</a></a>';
                                slides.push(elem);
                            }
                        }
                        slides.push('<li></li>');
                        slides.push('<li></li>');
                        return slides;
                    }()),
                },
            });
        </script>

        {{--show modal alarm--}}
        <script>
            $(function () {
                $('.show-modal-alarm').on('click', function () {
                    $('#modal-start').modal('hide');
                    $('#modal-alarm').modal('show');
                });

                $('.close-modal').click(function () {
                    $('#modal-alarm').modal('hide');
                });
            });
        </script>
        {{--endshow modal alarm--}}
        {{--rename building--}}
        <script>
            $(function () {
                var is_administrator = '{!! Auth::user()->role == 1?'1':'' !!}';
                $('.open-rename-building-modal').on('click', function () {
                    $('#modal-rename-building').modal('show');
                    return false;
                });

                $('.open-info-building-modal').on('click', function () {
                    $('#modal-info-building').modal('show');
                    return false;
                });

                $('.open-login-modal').on('click', function () {
                    if (is_administrator) {
                        location.href = "{!! route('admin.index') !!}";
                    } else {
                        $('#modal-login').modal('show');
                    }

                });
                $('.open-change-avatar-modal').on('click', function () {
                    $('#modal-change-avatar').modal('show');
                    return false;
                });
            });
        </script>
        {{--rename building--}}
        {{--login modal--}}
        <script type="text/javascript">
            $('.change-p-type').click(function () {
                if ($(this).hasClass('fa-eye')) {
                    $(this).removeClass('fa-eye');
                    $(this).addClass('fa-eye-slash');
                    $('input[name="password"]').attr('type', 'text');
                } else {
                    $(this).removeClass('fa-eye-slash');
                    $(this).addClass('fa-eye');
                    $('input[name="password"]').attr('type', 'password');
                }
            });
        </script>
        {{--login modal--}}
        <script>
            $(function () {
                //change avatar
                $('.save-change-avatar').on('click', function () {
                    $('#modal-change-avatar-form').submit();
                });

                $('.save-info-building').on('click', function () {
                    $('#modal-info-building-form').submit();
                });

                $('.save-rename-building').on('click', function () {
                    $('#modal-rename-building-form').submit();
                });

                //change avatar
                $('.cancel-modal').on('click', function () {
                    $('.modal').modal('hide');
                    return false;
                });
            });

        </script>
        <script>
            //update giờ
            function updateClock() {
                var currentTime = new Date();
                var currentHours = currentTime.getHours();
                var currentMinutes = currentTime.getMinutes();
//                var currentSeconds = currentTime.getSeconds ( );
                var timeOfDay = 'AM';
                if (currentHours >= 12)
                    timeOfDay = 'PM';
                currentHours = currentHours % 12;
                currentHours = currentHours < 10 ? '0' + currentHours : currentHours;
                currentMinutes = currentMinutes < 10 ? '0' + currentMinutes : currentMinutes;
                // Compose the string for display
                var currentTimeString = currentHours + ":" + currentMinutes + " " + timeOfDay;


                $("#clock").html(currentTimeString);

            }

            $(document).ready(function () {
                setInterval('updateClock()', 1000);
            });
            $().ready(() => {
                $('.anchor').on('click', function(){
                   var url = $(this).find('a').attr('href');
                   window.location.replace(url);
                });
            });
        </script>
        <script>
            $(function(){
                var winHeight = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
                var winWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
                $('html').css('min-height',winHeight);
                $('html').css('min-width',winWidth);
            });
        </script>
    </body>
</html>

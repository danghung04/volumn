<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<title>Đăng nhập</title>
<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="{!! asset('public/design/css/login.css') !!}" rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
	<link rel="icon" href="{!! asset('public/images/favicon.png') !!}"  />
	<!-- -->
    <style>
        input:-webkit-autofill {
            /*-webkit-box-shadow: inset 0 0 0px 9999px #3B4583;*/
            -webkit-box-shadow: inset 0 0 0px 9999px #14182E;
            -webkit-text-fill-color: #eee;
        }
		body{
			font-family: utm avo;
		}
        a{
            text-decoration: none;
        }
        span.change-p-type{
            position: absolute;
            color: #bbb;
            right: 15px;
            cursor: pointer;
            font-size: 1.2em;
            top: 15px;
        }
        .footer{
            color: rgba(180, 180, 180, 0.9);
            padding: 15px;
            text-align: center;
            font-size: 15px;
        }
    </style>
</head>
<body>
<!-- contact-form -->
<div class="wrapper">
	<div class="message warning">
		<div class="logo">
			<img alt="Logo" src="{!! asset('public/images/logo.png') !!}"/>
		</div>
		<div class="contact-form">
			<form class="form" name="contact_form"  method="POST" action="{{ route('login') }}">
				<ul>
					<li class="input">
						 <label><img src="{!! asset('public/design/images/contact.png') !!}" alt=""></label>
						 <input type="email" class="email" name="email" placeholder="Tên đăng nhập" required />
					 </li>
					 <li class="input">
						 <label><img src="{!! asset('public/design/images/lock.png') !!}" alt=""></label>
						 <div class="password-field">
                             <input type="password" name="password" placeholder="Mật khẩu" required />
                             {{--<span class="change-p-type fas fa-eye"></span>--}}
                         </div>
					 </li>
				</ul>
				<div style="color: #fff;">
					@if ($errors->has('email'))
						<strong>Email hoặc mật khẩu không đúng</strong>
					@endif
				</div>
				<div class="clear"></div>
				<div class="remember">
					<input type="checkbox">
					<label for="" style="font-weight: normal">Ghi nhớ tài khoản</label>
				</div>
				<div>
					<button class="btn btn-block">Đăng nhập</button>
				</div>
			</form>
		</div>
		<div class="forgot-password">
			<a  href="{{ route('web.showEmailResetPass') }}">Quên mật khẩu?</a>
		</div>
	</div>
	<div class="footer">
        Copyright &copy; 2018 Oritek
    </div>
</div>
<div class="clear"></div>
    <script type="text/javascript">
        $('.change-p-type').click(function(){
            if($(this).hasClass('fa-eye')){
                $(this).removeClass('fa-eye');
                $(this).addClass('fa-eye-slash');
                $('input[name="password"]').attr('type','text');
            }else{
                $(this).removeClass('fa-eye-slash');
                $(this).addClass('fa-eye');
                $('input[name="password"]').attr('type','password');
            }
        });
    </script>
</body>
</html>
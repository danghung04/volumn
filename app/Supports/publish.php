<?php 
namespace App\Supports;
//require("phpMQTT.php");
use App\Supports\phpMQTT;

// //$client_id = uniqid();
// $client_id = "phpMQTT-publisher"; // make sure this is unique for connecting to sever - you could use uniqid()
// $mqtt = new phpMQTT($server, $port, $client_id);
// if ($mqtt->connect(true, NULL, $username, $password)) {
// 	$mqtt->publish("device/17F01A/sub", "Hello 1 ds World! at " . date("r"), 0);
// 	$mqtt->close();
// } else {
//     echo "Time out!\n";
// }
class Publish {
	private $mqtt;
	private $server = "34.229.9.67";     // change if necessary
	private $port = 1883;                     // change if necessary
	private $username = "";                   // set your username
	private $password = "";                   // set your password
	private $client_id = "phpMQTT-publisher";
	public function publish($id,$status)
	{
		$this->mqtt = new phpMQTT($this->server, $this->port, $this->client_id);
		if ($this->mqtt->connect(true, NULL, $this->username, $this->password)) {
			$this->mqtt->publish("device/".$id."/sub", "{id:".$id.",status:".$status."}");
			$this->mqtt->close();
			return true;
		} else {
			return false;
		}
	}
} ?>

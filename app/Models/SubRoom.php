<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubRoom extends Model
{
    public $table = 'sub_rooms';
    protected $fillable = [
        'room_id','name', 'x_offset', 'y_offset',
    ];
    public $timestamps =false;

    public static function get_named_form()
    {
        $data = array();
        $data['name'] = [
            'type'  => ['input', 'text'],
            'title' => 'Tên phòng'
        ];
        $data['x_offset'] = [
            'type'  => ['input', 'hidden'],
        ];
        $data['y_offset'] = [
            'type'  => ['input', 'hidden']
        ];
        $data['room_id'] = [
            'type' => ['input', 'hidden']
        ];
        return $data;
    }
}

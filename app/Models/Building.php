<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Building extends Model
{
    protected $table = 'building';
    protected $fillable = ['id','name','mac','status','version','website','feedback'];
    public $timestamps = false;
}
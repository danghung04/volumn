<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Equipment extends Model
{
    protected $table = 'equipments';
    protected $fillable = ['name_equip', 'equipment_type_id', 'iotdevice_id', 'room_id', 'group_id', 'x_offset', 'y_offset', 'ordered_at', 'status', 'time', 'alarm_state'];

    public function equipment_type()
    {
        return $this->belongsTo('App\Models\EquipmentTypes');
    }

    public static function get_create_form()
    {
        $data = array();
        $data['room_id'] = [
            'type' => ['input', 'hidden'],
        ];
        $data['equipment_type_id'] = [
            'type' => ['input', 'hidden'],
        ];
        $data['x_offset'] = [
            'type'    => ['input', 'hidden']
        ];
        $data['y_offset'] = [
            'type'  => ['input', 'hidden']
        ];
        $data['name_equip'] = [
            'type'  => ['input','text'],
            'title' => 'Tên thiết bị',
            'field_attributes' => ['required']
        ];
        $data['iotdevice_id'] = [
            'type' => 'select',
            'title' => 'Chọn iot device',
            'data'  => 'iotdevice'
        ];

        $data['ordered_at'] = [
            'type'  => ['input','number'],
            'title' => 'STT'
        ];

        $data['group_id'] = [
            'type' => 'select',
            'title' => 'Chọn nhóm',
            'data'  => 'groups'
        ];

        $data['status'] = [
            'type'  => ['input', 'checkbox'],
            'title' => 'Trạng thái mặc định'
        ];


        return $data;
    }

    public static function get_update_form()
    {
        $data = array();
        $data['id'] = [
            'type' => ['input', 'hidden']
        ];
        $data['room_id'] = [
            'type' => ['input', 'hidden'],
        ];
        $data['equipment_type_id'] = [
            'type' => ['input', 'hidden'],
        ];
        $data['name_equip'] = [
            'type'  => ['input','text'],
            'title' => 'Tên thiết bị',
            'field_attributes' => ['required']
        ];
        $data['iotdevice_id'] = [
            'type' => 'select',
            'title' => 'Chọn iot device',
            'data'  => 'iotdevice'
        ];

        $data['ordered_at'] = [
            'type'  => ['input','number'],
            'title' => 'STT'
        ];

        $data['group_id'] = [
            'type' => 'select',
            'title' => 'Chọn nhóm',
            'data'  => 'groups'
        ];

        $data['status'] = [
            'type'  => ['input', 'checkbox'],
            'title' => 'Trạng thái mặc định'
        ];


        return $data;
    }

    public function room()
    {
        return $this->belongsTo('App\Models\Room');
    }

    public function group()
    {
        return $this->belongsTo('App\Models\Group');
    }

    public function iot()
    {
        return $this->belongsTo('App\Models\IotDevice', 'iotdevice_id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EquipmentTypes extends Model
{
    protected $table = 'equipment_types';
    protected $fillable = ['name', 'image','image_off','active', 'icon','deleted'];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class IotDevice extends Model
{
    protected $keyType = 'string';
    public $incrementing = false;
    protected $table = 'iotdevice';
    protected $fillable = ['id','cid', 'ip', 'mac', 'name_wifi', 'password_wifi', 'active', 'online', 'quantity', 'ping'];
    public $timestamps = false;
}

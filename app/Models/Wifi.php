<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Wifi extends Model
{
    protected $fillable = [
        'name_wifi', 'password_wifi'
    ];

    public static function get_setting_form()
    {
        $data = array();
        $data['name_wifi_old'] = [
            'type'  => 'select',
            'title' => 'Mạng wifi cần đổi',
            'data'  => [
                0   => 'Trọng',
                1   => 'Không Trọng',
            ]
        ];
        $data['name_wifi'] = [
            'type'  => ['input', 'text'],
            'title' => 'Tên mạng wifi'
        ];
        $data['password_wifi'] = [
            'type'  => ['input', 'password'],
            'title' => 'Mật khẩu cần đổi'
        ];
        return $data;
    }
}

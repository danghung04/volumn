<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static function get_create_form()
    {
        $data = array();
        $data['name'] = [
            'type'  => ['input', 'text'],
            'title' => 'Tên'
        ];
        $data['email'] = [
            'type'  => ['input', 'email'],
            'title' => 'Email'
        ];
        $data['password'] = [
            'type'  => ['input', 'password'],
            'title' => 'Mật khẩu'
        ];
        $data['password_confirmation'] = [
            'type'  => ['input', 'password'],
            'title' => 'Xác nhận mật khẩu'
        ];
        $data['permission'] = [
            'type'  => 'select',
            'title' => 'Quyền'
        ];
        $data['permission']['data'] = [
            '2' => 'User',
            '1' => 'Admin'
        ];
        $data['permission']['field_attributes'] = ['disabled' => ''];
        return $data;
    }
    public static function get_update_form()
    {
        $data = array();
        $data['name'] = [
            'type'  => ['input', 'text'],
            'title' => 'Tên'
        ];
        $data['email'] = [
            'type'  => ['input', 'email'],
            'title' => 'Email'
        ];
        $data['password'] = [
            'type'  => ['input', 'password'],
            'title' => 'Mật khẩu'
        ];
        $data['role'] = [
            'type'  => 'select',
            'title' => 'Quyền'
        ];
        $data['role']['data'] = [
            '2' => 'User',
            '1' => 'Admin'
        ];
        $data['role']['field_attributes'] = ['disabled' => ''];
        return $data;
    }
}

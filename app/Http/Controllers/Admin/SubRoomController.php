<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\SubRoom;
class SubRoomController extends Controller
{
    public function store(){
        $request = request();
        $input = $request->except(['_token']);
        SubRoom::create($input);
        return redirect()->back();
    }

    public function updatePosition(){
        $request = request();
        $input = $request->except(['_token', 'id']);
        SubRoom::find($request->id)->update($input);
        $response = [
            'code'  => 200,
            'msg'   => 'Cập nhật thành công'
        ];
        return response()->json($response);
    }
}

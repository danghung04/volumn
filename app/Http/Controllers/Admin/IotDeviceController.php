<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\IotDevice;
use App\Models\Equipment;
use App\Models\Wifi;

class IotDeviceController extends Controller
{
    public function index()
    {
        $data['iots'] = IotDevice::paginate(10);
        $data['total_iot'] = IotDevice::count();
        $data['count_iot_accept'] = IotDevice::where('active', 1)->count();
        $data['count_iot_waiting'] = $data['total_iot'] - $data['count_iot_accept'];
        $data['total_iot'] = str_pad($data['total_iot'], 2, '0', STR_PAD_LEFT);
        $data['count_iot_accept'] = str_pad($data['count_iot_accept'], 2, '0', STR_PAD_LEFT);
        $data['count_iot_waiting'] = str_pad($data['count_iot_waiting'], 2, '0', STR_PAD_LEFT);
        $iot_wifis = IotDevice::distinct()->get(['name_wifi']);
        $wifi_data = [];
        foreach ($iot_wifis as $iot_wifi):
            $wifi_data[$iot_wifi->name_wifi] = $iot_wifi->name_wifi;
        endforeach;
        $from_data['fields'] = Wifi::get_setting_form();
        $from_data['fields']['name_wifi_old']['data'] = $wifi_data;
        $from_data['action'] = route('admin.wifi.update');
        $from_data['form_title'] = 'cài đặt wifi';
        $from_data['form_width'] = 450;
        $data['form_setting'] = view('admin.components.form-default', $from_data);
        $data['title'] = 'Thiết bị Iot';
        return view('admin.iot-device.index', $data);
    }

    public function create()
    {
        return view('admin.iot-device.create');
    }

    public function store()
    {
        $request = request();
        $input = $request->except(['_token']);
        IotDevice::create($input);
        return redirect(route('admin.iot-device.index'));
    }

    public function edit($id)
    {
        $data['iot'] = IotDevice::find($id);
        return view('admin.iot-device.update', $data);
    }

    public function update()
    {
        $request = request();
        $input = $request->except(['_token', 'id']);
        $iot = IotDevice::find($request->id);
        $iot->update($input);
        return redirect(route('admin.iot-device.index'));
    }

    public function delete($id)
    {
        Equipment::where('iotdevice_id', $id)->delete();
        IotDevice::destroy($id);
        return redirect(route('admin.iot-device.index'));
    }

    public function active(Request $request)
    {
        $iot = IotDevice::find($request->id);
        if (empty($iot)) {
            $response = [
                'state' => 0,
                'msg'   => 'Not found'
            ];
        } else {
            $iot->active = !$iot->active;
            if ($iot->save()) {
                $response = [
                    'state' => 1,
                    'msg'   => 'Success'
                ];
            } else {
                $response = [
                    'state' => 0,
                    'msg'   => 'Error in DB'
                ];
            }
        }
        return response()->json($response);
    }
    public function postDelete(Request $request){
        if(!empty($request->checks)){
            Equipment::whereIn('iotdevice_id', $request->checks)->delete();
            IotDevice::whereIn('id', $request->checks)->delete();
        }
        return redirect()->back();
    }
    public function getMaxEquip(Request $request){
        $iot = IotDevice::find($request->id);
        if(!empty($iot)){
            return $iot->quantity;
        }
        return 0;
    }
}

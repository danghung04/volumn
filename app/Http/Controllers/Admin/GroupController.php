<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Group;
use App\Models\Equipment;
use App\Models\Room;
use Config;
use File;
class GroupController extends Controller
{
    public function index(){
    	$data['groups'] = Group::all();
    	$data['rooms'] = Room::all();
        $data['setting_form'] = view('admin.group.setting-group-form');
        $data['add_form'] = view('admin.group.add-group-form');
        $data_warning['modal_title'] = 'Xóa cảnh';
        $data_warning['modal_content'] = 'Bạn có chắc muốn xóa cảnh này?';
        $data['warning_delete'] = view('admin.components.warning-delete', $data_warning);
        $data['url_delete'] = route('admin.group.delete');
        $data['title'] = 'Quản lý cảnh';
		return view('admin.group.index', $data);
	}

	public function create(){
		return view('admin.group.create');
	}

	public function store(){
        $request = request();
        $input = $request->except(['_token', 'icon', 'image','image_off']);
        //image
        if ($request->image) {
            $thumb_ext = $request->file('image')->getClientOriginalExtension();
            if(in_array($thumb_ext,Config::get('constants.image_valid_extensions'))){
                //delete old image
                $thumb_name = rand(11111, 99999) . '_' . time() . '.' . $thumb_ext;
                $thumbnail_upload = $request->file('image')->move(Config::get('constants.path_upload_image'), $thumb_name);
                $thumbnail = $thumbnail_upload->getFilename();
                $input['image'] = $thumbnail;
            }
        }else{
            $thumb_name = rand(11111, 99999) . '_' . time() . '.' . 'png';
            File::copy(config('constants.default_group_on'), config('constants.path_upload_image').$thumb_name);
            $input['image'] = $thumb_name;
        }

        if ($request->image_off) {
            $thumb_ext = $request->file('image_off')->getClientOriginalExtension();
            if(in_array($thumb_ext,Config::get('constants.image_valid_extensions'))){
                //delete old image
                $thumb_name = rand(11111, 99999) . '_' . time() . '.' . $thumb_ext;
                $thumbnail_upload = $request->file('image_off')->move(Config::get('constants.path_upload_image'), $thumb_name);
                $thumbnail = $thumbnail_upload->getFilename();
                $input['image_off'] = $thumbnail;
            }
        }else{
            $thumb_name = rand(11111, 99999) . '_' . time() . '.' . 'png';
            File::copy(config('constants.default_group_off'), config('constants.path_upload_image').$thumb_name);
            $input['image_off'] = $thumb_name;
        }
        //icon
        if ($request->icon) {
            $thumb_ext = $request->file('icon')->getClientOriginalExtension();
            if(in_array($thumb_ext,Config::get('constants.image_valid_extensions'))){
                //delete old image
                $thumb_name = rand(11111, 99999) . '_' . time() . '.' . $thumb_ext;
                $thumbnail_upload = $request->file('icon')->move(Config::get('constants.path_upload_icon'), $thumb_name);
                $thumbnail = $thumbnail_upload->getFilename();
                $input['icon'] = $thumbnail;
            }
        }else{
            $thumb_name = rand(11111, 99999) . '_' . time() . '.' . 'png';
            File::copy(config('constants.default_group_icon'), config('constants.path_upload_icon').$thumb_name);
            $input['icon'] = $thumb_name;
        }

        Group::create($input);
		return redirect(route('admin.group.index'));
	}

	public function edit($id){
		$data['group'] = Group::find($id);
		if($data['group']->deleted){
			return back();
		}
		return view('admin.group.update', $data);
	}

	public function update(){
        $request = request();
        $input = $request->except(['_token','id','image', 'icon', 'image_off']);
        $group = Group::find($request->id);

        //image
        if (!empty($request->image)) {
            $thumb_ext = $request->file('image')->getClientOriginalExtension();
            if(in_array($thumb_ext,Config::get('constants.image_valid_extensions'))){

                //delete old image
                $old_img = Config::get('constants.path_upload_image').$group->image;
                if(!empty($group->image)){
                    if(file_exists($old_img)){
                        unlink($old_img);
                    }
                }

                $thumb_name = rand(11111, 99999) . '_' . time() . '.' . $thumb_ext;
                $thumbnail_upload = $request->file('image')->move(Config::get('constants.path_upload_image'), $thumb_name);
                $thumbnail = $thumbnail_upload->getFilename();
                $input['image'] = $thumbnail;
            }
        }

        if (!empty($request->image_off)) {
            $thumb_ext = $request->file('image_off')->getClientOriginalExtension();
            if(in_array($thumb_ext,Config::get('constants.image_valid_extensions'))){

                //delete old image
                $old_img = Config::get('constants.path_upload_image').$group->image_off;
                if(!empty($group->image_off)){
                    if(file_exists($old_img)){
                        unlink($old_img);
                    }
                }

                $thumb_name = rand(11111, 99999) . '_' . time() . '.' . $thumb_ext;
                $thumbnail_upload = $request->file('image_off')->move(Config::get('constants.path_upload_image'), $thumb_name);
                $thumbnail = $thumbnail_upload->getFilename();
                $input['image_off'] = $thumbnail;
            }
        }

        if (!empty($request->icon)) {
            $thumb_ext = $request->file('icon')->getClientOriginalExtension();
            if(in_array($thumb_ext,Config::get('constants.image_valid_extensions'))){

                //delete old image
                $old_img = Config::get('constants.path_upload_icon').$group->icon;
                if(!empty($group->icon)){
                    if(file_exists($old_img)){
                        unlink($old_img);
                    }
                }

                $thumb_name = rand(11111, 99999) . '_' . time() . '.' . $thumb_ext;
                $thumbnail_upload = $request->file('icon')->move(Config::get('constants.path_upload_icon'), $thumb_name);
                $thumbnail = $thumbnail_upload->getFilename();
                $input['icon'] = $thumbnail;
            }
        }


        $group->update($input);
        return redirect(route('admin.group.index'));
	}

	public function delete(Request $request){
        Equipment::where('group_id', $request->id)->delete();
        $group = Group::find($request->id);
        if(!empty($group)){
            $this->delete_image($group->icon);
            $this->delete_image($group->image);
            $this->delete_image($group->image_off);
        }
        Group::destroy($request->id);
		return redirect(route('admin.group.index'));
	}

	public function delete_image($image){
	    $path_image = config('constants.path_upload_image') . $image;
        if (!empty($image)) {
            if (file_exists($path_image)) {
                unlink($path_image);
            }
        }
    }

    public function checkDuplicate(Request $request){
        $response = [
            'state' => 0,
            'msg'   => 'Không trùng'
        ];
        $where = [
            'name' => $request->name
        ];
        $group = Group::where('name', $request->name);
        if($request->id){
            $where['id'] = $request->id;
            $group = $group->whereNotIn('id', [$request->id]);
        }
        $group = $group->first();
        if($group){
            $response = [
                'state' => 1,
                'msg'   => 'Tên cảnh đã tồn tại'
            ];
        }
        return response()->json($response);
    }
}

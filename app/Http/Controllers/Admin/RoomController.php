<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Room;
use App\Models\Equipment;
use App\Models\SubRoom;
use Config;
class RoomController extends Controller
{
    public function index(){

    	$data['rooms'] = Room::where('deleted',0)->paginate(10);
		return view('admin.room.index', $data);
	}

	public function create(){
		return view('admin.room.create');
	}

	public function store(){
		$request = request();
		$input = $request->except(['_token','image']);
		if ($request->image) {         
            $thumb_ext = $request->file('image')->getClientOriginalExtension();
            if(in_array($thumb_ext,Config::get('constants.image_valid_extensions'))){
                //delete old image
                $thumb_name = rand(11111, 99999) . '_' . time() . '.' . $thumb_ext;
                $thumbnail_upload = $request->file('image')->move(Config::get('constants.path_upload_image'), $thumb_name);
                $thumbnail = $thumbnail_upload->getFilename();                     
                $input['image'] = $thumbnail; 
            }        
        }else{
        	$input['image'] = 'no-image-choosen';
        }
		Room::create($input);
		return redirect()->back();
	}

	public function edit($id){
		$data['room'] = Room::find($id);
		if($data['room']->deleted){
			return back();
		}
		return view('admin.room.update', $data);
	}

	public function update(){
		$request = request();
		$input = $request->except(['_token','id','image']);
		$room = Room::find($request->id);
		if(!empty($room)){
            if (!empty($request->image)) {
                $thumb_ext = $request->file('image')->getClientOriginalExtension();
                if(in_array($thumb_ext,Config::get('constants.image_valid_extensions'))){

                    //delete old image
                    $old_img = Config::get('constants.path_upload_image').$room->image;
                    if(!empty($room->image)){
                        if(file_exists($old_img)){
                            unlink($old_img);
                        }
                    }

                    $thumb_name = rand(11111, 99999) . '_' . time() . '.' . $thumb_ext;
                    $thumbnail_upload = $request->file('image')->move(Config::get('constants.path_upload_image'), $thumb_name);
                    $thumbnail = $thumbnail_upload->getFilename();
                    $input['image'] = $thumbnail;
                }
            }
            $room->update($input);
        }
		return redirect()->back();
	}

	public function delete($id){
		$room = Room::find($id);
		Equipment::where('room_id',$id)->delete();
        $image = config('constants.path_upload_image') . $room->image;
        if (!empty($room->image)) {
            if (file_exists($image)) {
                unlink($image);
            }
        }
        Room::destroy($id);
		return redirect(route('admin.index'));
	}
	public function namedSubRoom(){
        echo 'da vao';
    }

    public function checkDuplicate(Request $request){
        $response = [
            'state' => 0,
            'msg'   => 'Không trùng'
        ];
        $where = [
            'name' => $request->name
        ];
        $room = Room::where('name', $request->name);
        if($request->id){
            $where['id'] = $request->id;
            $room = $room->whereNotIn('id', [$request->id]);
        }
        $room = $room->first();
        if($room){
            $response = [
                'state' => 1,
                'msg'   => 'Tên tầng đã tồn tại'
            ];
        }
        return response()->json($response);
    }
}

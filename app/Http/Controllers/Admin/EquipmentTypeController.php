<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\EquipmentTypes;
use App\Models\Equipment;
use Config;
use File;
class EquipmentTypeController extends Controller
{
    public function index()
    {
        $request = request();
        $data['equipment_types'] = EquipmentTypes::all();
        $data['setting_form'] = view('admin.equipment-type.setting-equipment-form');
        $data['add_form'] = view('admin.equipment-type.add-equipment-form');
        $data_warning['modal_title'] = 'Xóa loại thiết bị';
        $data_warning['modal_content'] = 'Bạn có chắc muốn xóa loại thiết bị này?';
        $data['warning_delete'] = view('admin.components.warning-delete', $data_warning);
        $data['url_delete'] = route('admin.equipment-type.delete');
        $data['title'] = 'Quản lý loại thiết bị';
        return view('admin.equipment-type.index', $data);
    }

    public function create()
    {
        return view('admin.equipment-type.create');
    }

    public function store()
    {
        $request = request();
        $input = $request->except(['_token', 'icon', 'image', 'image_off']);
        //image
        if ($request->image) {
            $thumb_ext = $request->file('image')->getClientOriginalExtension();
            if (in_array($thumb_ext, Config::get('constants.image_valid_extensions'))) {
                //delete old image
                $thumb_name = rand(11111, 99999) . '_' . time() . '.' . $thumb_ext;
                $thumbnail_upload = $request->file('image')->move(Config::get('constants.path_upload_image'), $thumb_name);
                $thumbnail = $thumbnail_upload->getFilename();
                $input['image'] = $thumbnail;
            }
        } else {
            $thumb_name = rand(11111, 99999) . '_' . time() . '.' . 'png';
            File::copy(config('constants.default_equip_on'), config('constants.path_upload_image').$thumb_name);
            $input['image'] = $thumb_name;
        }

        if ($request->image_off) {
            $thumb_ext = $request->file('image_off')->getClientOriginalExtension();
            if (in_array($thumb_ext, Config::get('constants.image_valid_extensions'))) {
                //delete old image
                $thumb_name = rand(11111, 99999) . '_' . time() . '.' . $thumb_ext;
                $thumbnail_upload = $request->file('image_off')->move(Config::get('constants.path_upload_image'), $thumb_name);
                $thumbnail = $thumbnail_upload->getFilename();
                $input['image_off'] = $thumbnail;
            }
        } else {
            $thumb_name = rand(11111, 99999) . '_' . time() . '.' . 'png';
            File::copy(config('constants.default_equip_off'), config('constants.path_upload_image').$thumb_name);
            $input['image_off'] = $thumb_name;
        }
        //icon
        if ($request->icon) {
            $thumb_ext = $request->file('icon')->getClientOriginalExtension();
            if (in_array($thumb_ext, Config::get('constants.image_valid_extensions'))) {
                //delete old image
                $thumb_name = rand(11111, 99999) . '_' . time() . '.' . $thumb_ext;
                $thumbnail_upload = $request->file('icon')->move(Config::get('constants.path_upload_icon'), $thumb_name);
                $thumbnail = $thumbnail_upload->getFilename();
                $input['icon'] = $thumbnail;
            }
        } else {
            $thumb_name = rand(11111, 99999) . '_' . time() . '.' . 'png';
            File::copy(config('constants.default_equip_icon'), config('constants.path_upload_icon').$thumb_name);
            $input['icon'] = $thumb_name;
        }

        EquipmentTypes::create($input);
        return redirect(route('admin.equipment-type.index'));
    }

    public function edit($id)
    {
        $data['equipment'] = EquipmentTypes::find($id);
        if ($data['equipment']->deleted) {
            return back();
        }
        return view('admin.equipment-type.update', $data);
    }

    public function update()
    {
        $request = request();
        $input = $request->except(['_token', 'id', 'image', 'icon', 'image_off']);
        $equipment = EquipmentTypes::find($request->id);

        //image
        if (!empty($request->image)) {
            $thumb_ext = $request->file('image')->getClientOriginalExtension();
            if (in_array($thumb_ext, Config::get('constants.image_valid_extensions'))) {

                //delete old image
                $old_img = Config::get('constants.path_upload_image') . $equipment->image;
                if (!empty($equipment->image)) {
                    if (file_exists($old_img)) {
                        unlink($old_img);
                    }
                }

                $thumb_name = rand(11111, 99999) . '_' . time() . '.' . $thumb_ext;
                $thumbnail_upload = $request->file('image')->move(Config::get('constants.path_upload_image'), $thumb_name);
                $thumbnail = $thumbnail_upload->getFilename();
                $input['image'] = $thumbnail;
            }
        }

        if (!empty($request->image_off)) {
            $thumb_ext = $request->file('image_off')->getClientOriginalExtension();
            if (in_array($thumb_ext, Config::get('constants.image_valid_extensions'))) {

                //delete old image
                $old_img = Config::get('constants.path_upload_image') . $equipment->image_off;
                if (!empty($equipment->image_off)) {
                    if (file_exists($old_img)) {
                        unlink($old_img);
                    }
                }

                $thumb_name = rand(11111, 99999) . '_' . time() . '.' . $thumb_ext;
                $thumbnail_upload = $request->file('image_off')->move(Config::get('constants.path_upload_image'), $thumb_name);
                $thumbnail = $thumbnail_upload->getFilename();
                $input['image_off'] = $thumbnail;
            }
        }

        if (!empty($request->icon)) {
            $thumb_ext = $request->file('icon')->getClientOriginalExtension();
            if (in_array($thumb_ext, Config::get('constants.image_valid_extensions'))) {

                //delete old image
                $old_img = Config::get('constants.path_upload_icon') . $equipment->icon;
                if (!empty($equipment->icon)) {
                    if (file_exists($old_img)) {
                        unlink($old_img);
                    }
                }

                $thumb_name = rand(11111, 99999) . '_' . time() . '.' . $thumb_ext;
                $thumbnail_upload = $request->file('icon')->move(Config::get('constants.path_upload_icon'), $thumb_name);
                $thumbnail = $thumbnail_upload->getFilename();
                $input['icon'] = $thumbnail;
            }
        }

        $equipment->update($input);
        return redirect(route('admin.equipment-type.index'));
    }

    public function updateActive(Request $request)
    {
        $equipmentType = EquipmentTypes::find($request->id);
        if (empty($equipmentType)) {
            $response = [
                'state' => 0,
                'msg'   => 'Not found'
            ];
        } else {
            $equipmentType->active = !$equipmentType->active;
            if ($equipmentType->save()) {
                $response = [
                    'state' => 1,
                    'msg'   => 'Success'
                ];
            } else {
                $response = [
                    'state' => 0,
                    'msg'   => 'Error in DB'
                ];
            }
        }
        return response()->json($response);
    }

    public function delete(Request $request)
    {
        Equipment::where('equipment_type_id', $request->id)->delete();
        $eq = EquipmentTypes::find($request->id);
        if (!empty($eq)) {
            $this->delete_image($eq->icon);
            $this->delete_image($eq->image);
            $this->delete_image($eq->image_off);
        }
        EquipmentTypes::destroy($request->id);
        return redirect(route('admin.equipment-type.index'));
    }

    public function delete_image($image)
    {
        $path_image = config('constants.path_upload_image') . $image;
        if (!empty($image)) {
            if (file_exists($path_image)) {
                unlink($path_image);
            }
        }
    }

    public function checkDuplicate(Request $request){
        $response = [
            'state' => 0,
            'msg'   => 'Không trùng'
        ];
        $where = [
            'name' => $request->name
        ];
        $equipmentType = EquipmentTypes::where('name', $request->name);
        if($request->id){
            $where['id'] = $request->id;
            $equipmentType = $equipmentType->whereNotIn('id', [$request->id]);
        }
        $equipmentType = $equipmentType->first();
        if($equipmentType){
            $response = [
                'state' => 1,
                'msg'   => 'Tên loại thiết bị đã tồn tại'
            ];
        }
        return response()->json($response);
    }
}

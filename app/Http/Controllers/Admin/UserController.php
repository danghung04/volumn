<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Validator;

class UserController extends Controller
{
    public function index()
    {
        $data['users'] = User::where('role', 2)->orderBy('name')->get();
        $data['admins'] = User::where('role', 1)->orderBy('name')->get();
        $from_create_data['fields'] = User::get_create_form();
        $from_create_data['action'] = route('admin.user.store');
        $data['create_form'] = view('admin.components.create-form', $from_create_data);

        $from_update_data['fields'] = User::get_update_form();
        $from_update_data['action'] = route('admin.user.update');
        $data['update_form'] = view('admin.components.update-form', $from_update_data);
        $data['warning_delete'] = view('admin.components.warning-delete');
        $data['url_get_update_data'] = route('admin.user.edit');
        $data['url_delete'] = route('admin.user.delete');
        $data['title'] = 'Quản lý người dùng';
        return view('admin.user.index', $data);
    }

    public function store()
    {
        $request = request();
        $request->validate([
            'name'     => 'required|string|max:255',
            'email'    => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ], [
            'name.required'      => 'Tên không bỏ trống',
            'email.required'     => 'Email không bỏ trống',
            'email.unique'       => 'Email đã tồn tại',
            'password.min'       => 'Mật khẩu phải lớn hơn 6 ký tự',
            'password.confirmed' => 'Xác nhận mật khẩu không đúng',
            'password.required' => 'Mật khẩu không bỏ trống'
        ]);
        $input = $request->except(['_token', 'password_confirmation', 'password']);
        $input['password'] = bcrypt($request->password);
        User::create($input);
        return redirect(route('admin.user.index'));
    }

    public function edit()
    {
        $request = request();
        $id = $request->id;
        $fields = User::get_update_form();
        $user = User::find($id);
        if (!empty($user)) {
            foreach ($fields as $field_key => $field_value) {
                if ($field_key != 'password')
                    $fields[$field_key]['value'] = $user->$field_key;
            }
            return response()->json($fields);
        }

    }

    public function update()
    {
        $request = request();
        $request->validate([
            'name'  => 'required|string|max:255',
            'email'    => 'required|string|email',
            'password' => 'confirmed',
        ], [
            'name.required'  => 'Tên không để trống',
            'email.required' => 'Email không để trống',
            'email.email'    => 'Email không đúng định dạng',
            'password.confirmed' => 'Xác nhận mật khẩu không đúng'
        ]);

        $input = $request->except(['_token', 'password']);

        $user = User::find($request->id);
        if ($request->password) {
            $input['password'] = bcrypt($request->password);
        }
        $user->update($input);
        return redirect(route('admin.user.index'));
    }

    public function delete()
    {
        $request = request();
        $user = User::find($request->id);
        if(!empty($user) && $user->role == 1)
            return redirect()->back();
        User::destroy($request->id);
        return redirect(route('admin.user.index'));
    }

    public function checkDuplicate(Request $request){
        $response = [
            'state' => 0,
            'msg'   => 'Không trùng'
        ];
        $state = 0;
        $msg = [];
        $userByName = User::where('name', $request->name);
        if($request->id){
            $userByName = $userByName->whereNotIn('id', [$request->id]);
        }
        $userByName = $userByName->first();
        if($userByName){
            $state = 1;
            $msg['name'] = 'Tên đăng nhập đã tồn tại';
        }


        $userByEmail = User::where('email', $request->email);
        if($request->id){
            $userByEmail = $userByEmail->whereNotIn('id', [$request->id]);
        }
        $userByEmail = $userByEmail->first();
        if($userByEmail){
            $state = 1;
            $msg['email'] = 'Email đã tồn tại';
        }
        $response = [
            'state' => $state,
            'msg'   => $msg
        ];
        return response()->json($response);
    }
}

<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\SubRoom;
use App\Models\Room;
use App\Models\EquipmentTypes;
use App\Models\Equipment;
use DB;

class HomeController extends Controller
{
    public function index()
    {
        $request = request();
        $data['room_active_id'] = $request->rid;

        $data['rooms'] = Room::all();
        $room_active = null;
        if(empty($data['room_active_id'])){ //Nếu load lần đầu thì lấy trang đầu tiên
            if(count($data['rooms']) > 0){
                $room_active = $data['rooms'][0];
            }
        }else{
            $room_active = Room::find($request->rid);
        }
        $data['room_active'] = null;
        $data['named_setting_floor_form'] = "";
        $data['named_sub_room_form'] = "";
        $data['create_equipment_modal'] = "";
        $data['add_floor_form'] = $this->getAddFloorForm();
        $data['equipment_types'] = [];
        $data['sub_rooms'] = [];
        $data['equipments'] = [];
        $data['e_create_modal_id'] = '';
        $data['r_named_modal_id'] = '';
        if(!$room_active){
            return view('admin.home', $data);
        }

        $data['room_active'] = $room_active;
        $data['named_setting_floor_form'] = $this->getNamedSettingFloorForm($room_active);
        $named_sub_room_form = $this->getNamedSubRoomForm();
        foreach ($named_sub_room_form as $key => $value) {
            $data[$key] = $value;
        }
        foreach($this->getUpdateEquipmentModal() as $key => $value){
            $data[$key] = $value;
        }
        $create_equipment_modal = $this->getCreateEquipmentModal();
        foreach ($create_equipment_modal as $key => $value) {
            $data[$key] = $value;
        }

        $data['equipment_types'] = EquipmentTypes::all();
        $data['sub_rooms'] = SubRoom::where('room_id', $room_active->id)->get();
        $data['equipments'] = DB::table('equipments')
            ->join('equipment_types', 'equipments.equipment_type_id', '=', 'equipment_types.id')
            ->select(['equipments.id', 'equipments.name_equip', 'equipments.status', 'equipments.equipment_type_id', 'equipments.x_offset', 'equipments.y_offset', 'equipment_types.icon', 'equipment_types.image', 'equipment_types.image_off'])
            ->where('equipments.room_id', $room_active->id)->get();
        $data['iots'] = $this->getIotSelectionData('iotdevice');
        foreach ($data['equipments'] as $equipment):
            $equipment->active_image = $equipment->image;
            if (!$equipment->status):
                $equipment->active_image = $equipment->image_off;
            endif;
        endforeach;
        return view('admin.home', $data);
    }

    public function getCreateEquipmentModal()
    {
        $e_form_create_data['fields'] = Equipment::get_create_form();
        $e_form_create_data['fields']['group_id']['data'] = $this->getGroupSelectionData($e_form_create_data['fields']['group_id']['data']);
        $e_form_create_data['fields']['iotdevice_id']['data'] = $this->getIotSelectionData('iotdevice');
        $e_form_create_data['form_title'] = 'Cài đặt thiết bị';
        $e_form_create_data['action'] = route('admin.equipment.postAdd');
        $e_form_create_data['modal_id'] = 'create-equipment-modal';
        return [
            'e_create_modal_id'      => $e_form_create_data['modal_id'],
            'create_equipment_modal' => view('admin.components.form-default', $e_form_create_data)
        ];
    }

    public function getUpdateEquipmentModal()
    {
        $e_form_update_data['fields'] = Equipment::get_update_form();
        $e_form_update_data['fields']['group_id']['data'] = $this->getGroupSelectionData($e_form_update_data['fields']['group_id']['data']);
        $e_form_update_data['fields']['iotdevice_id']['data'] = $this->getIotSelectionData($e_form_update_data['fields']['iotdevice_id']['data']);

        $e_form_update_data['form_title'] = 'Cập nhật thiết bị';
        $e_form_update_data['action'] = route('admin.equipment.update');
        $e_form_update_data['modal_id'] = 'update-equipment-modal';
        return [
            'e_update_modal_id'      => $e_form_update_data['modal_id'],
            'update_equipment_modal' => view('admin.components.form-default', $e_form_update_data)
        ];
    }

    public function getNamedSubRoomForm()
    {
        $form_named_data['fields'] = SubRoom::get_named_form();
        $form_named_data['form_title'] = 'Đặt tên phòng';
        $form_named_data['action'] = route('admin.sub-room.store');
        $form_named_data['modal_id'] = 'named-modal';
        return [
            'r_named_modal_id'         => $form_named_data['modal_id'],
            'named_sub_room_form' => view('admin.components.form-default', $form_named_data)
        ];
    }

    public function getNamedSettingFloorForm($room_active)
    {
        $form_setting_floor['form_title'] = 'Cài đặt tầng';
        $form_setting_floor['room_active'] = $room_active;
        $form_setting_floor['action'] = route('admin.room.update');
        return view('admin.home.setting-floor', $form_setting_floor);
    }

    public function getAddFloorForm(){
        $data['form_title'] = 'Thêm tầng';
        $data['action'] = route('admin.room.store');
        return view('admin.home.add-floor', $data);
    }

    public function getGroupSelectionData($table_name)
    {
        $data = DB::table($table_name)->select(['id', 'name'])->get();
        $res = [];
        $res['0'] = 'Chưa chọn';
        foreach ($data as $item) {
            $res[$item->id] = $item->name;
        }
        return $res;
    }

    public function getIotSelectionData($table_name)
    {
        $data = DB::table($table_name)->select(['id'])->get();
        $res = [];
        foreach ($data as $item) {
            $res[$item->id] = $item->id;
        }
        return $res;
    }
}

<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Equipment;
use App\Models\Room;
use App\Models\Group;
use App\Models\IotDevice;
class EquipmentController extends Controller
{
    public function index(){
    	$data['equipments'] = Equipment::with(['room','group','equipment_type','iot'])->paginate(10);
		return view('admin.equipment.index', $data);
	}

    public function updatePosition(){
        $request = request();
        $input = $request->except(['_token', 'id']);
        Equipment::find($request->id)
            ->update($input);
        $response = [
            'code'  => 200,
            'msg'   => 'Cập nhật thành công'
        ];
        return response()->json($response);
    }

    public function postAdd(){
        $request = request();
        $input = $request->except(['_token', 'status']);
        if($request->status)
            $input['status'] = 1;
        Equipment::create($input);
        return redirect()->back();
    }
	public function edit($id){
		return Equipment::find($id);
	}

	public function update(Request $request){
		$input = $request->except(['_token','id', 'status']);
		if($request->status){
		    $input['status'] = 1;
        }else{
		    $input['status'] = 0;
        }
		$room = Equipment::find($request->id);
		$room->update($input);
		return redirect()->back();
	}

	public function delete(Request $request){
		Equipment::destroy($request->id);
	}
}

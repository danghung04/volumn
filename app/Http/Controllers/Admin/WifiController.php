<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\IotDevice;
class WifiController extends Controller
{

    public function update(Request $request){
        $data = [];
        if(!empty($request->password_wifi)){
            $data['password_wifi'] = $request->password_wifi;
        }

        if(!empty($request->ssid)){
            $data['name_wifi'] = $request->name_wifi;
        }
        if(!empty($data)){
            IotDevice::where('name_wifi', $request->name_wifi_old)->update($data);
        }
    }

    public function iot(Request $request){
        if(!empty($request->password_wifi)){
            $data['password_wifi'] = $request->password_wifi;
        }
        if(!empty($request->name_wifi)){
            $data['name_wifi'] = $request->name_wifi;
        }
        if(!empty($data)){
            IotDevice::where('name_wifi', $request->name_wifi_old)->update($data);
        }
        $data = IotDevice::where('name_wifi', $request->name_wifi)->get();
        return response()->json($data);
    }
}

<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Version;
use App\Models\ImageDetail;
use Input;
use Config;
use Validator;
class VersionController extends Controller
{
    public function index(){

    }

	public function postCreate(Request $request){
		$rules = [
           'version_name'=>'required',
           'photo' =>'required',       
           'keyword' =>'required',       
           'link_exe' =>'required',       
           'link_android' =>'required',       
           'link_ios' =>'required',       
           'video' =>'required',       
           'description' =>'required',       
        ];


        $validator = Validator::make($request->all(), $rules);
        $check = true;
		$row_data = '';
		$create_id = 0;
        if ($validator->fails()) {
            $state = 0;
            $msg = 'Any field cannot empty!';       
        }else{
        	$input = $request->all();
			
			if (Input::hasFile('photo')) {         
	            $thumb_ext = $request->file('photo')->getClientOriginalExtension();
	            $valid_ext = ['png','PNG','jpg','jpeg','ico','gif'];
	            if(in_array($thumb_ext,$valid_ext)){
	                //delete old image
	                $thumb_name = 'v_'.rand(11111, 99999) . '_' . time() . '.' . $thumb_ext;
	                $thumbnail_upload = $request->file('photo')->move(Config::get('constants.path_upload'), $thumb_name);
	                $thumbnail = $thumbnail_upload->getFilename();                     
	                $input['photo'] = $thumbnail; 
	            }else{
	            	$state = 0;
	            	$msg = 'Image could not upload';
	            	$check - false;
	            }          
	        }
	        if($check){
	        	$id = Version::create($input)->id;
	        	if($id){
	        		$create_id = $id;
		        	$state = 1;
		        	$msg = 'Create new version successful!';
		        	$row_data = '<tr id="'.$id.'">';
		        	$row_data .='<td>0</td>';
		        	$row_data .='<td>'.$input['version_name'].'</td>';
		        	$row_data .='<td><img style="height:30px" src= "'.url('public').'/upload/'.$input['photo'].'" /></td>';
		        	$row_data .='<td>'.$input['keyword'].'</td>';
		        	$row_data .='<td>'.$input['link_exe'].'</td>';
		        	$row_data .='<td>'.$input['link_android'].'</td>';
		        	$row_data .='<td>'.$input['link_ios'].'</td>';
		        	$row_data .='<td>'.$input['video'].'</td>';
		        	$row_data .='<td>'.$input['description'].'</td>';

		        	$row_data .='<td><a href="'.url('/').'/admin/version/photo/'.$id.'" class="btn green"><i class="fa fa-dashboard li-btn"></i>Photo</a></td>';
		        	$row_data .='<td><a href="#" class="btn blue update-version"><i class="fa fa-pencil li-btn"></i>Update</a></td>';
		        	$row_data .='<td><a href="#" class="btn btn-danger delete-version"><i class="fa fa-trash-o li-btn"></i>Delete</a></td>';
		        	$row_data .="</tr>";

		        }else{
		        	$state = 0;
		        	$msg = 'Create unsuccess!';
		        }
	        }
        }
		
        
		$data = array(
			'state'=>$state,
			'msg'  =>$msg,
			'data' => $row_data,
			'create_id' => $create_id
		);
		return json_encode($data);
	}

	public function postUpdate(Request $request){
		$input = $request->all();
		$version = Version::find($request->id);
		$check = true;
		$row_data = '';
		if (Input::hasFile('photo')) {         
            $thumb_ext = $request->file('photo')->getClientOriginalExtension();
            $valid_ext = ['png','PNG','jpg','jpeg','ico','gif'];
            if(in_array($thumb_ext,$valid_ext)){

                //delete old image
                 $old_img = Config::get('constants.path_upload').$version->photo;
                 if(!empty($version->photo)){
                    if(file_exists($old_img)){
                         unlink($old_img);
                     }
                 }

                $thumb_name = 'v_'.rand(11111, 99999) . '_' . time() . '.' . $thumb_ext;
                $thumbnail_upload = $request->file('photo')->move(Config::get('constants.path_upload'), $thumb_name);
                $thumbnail = $thumbnail_upload->getFilename();                     
                $input['photo'] = $thumbnail; 
            }else{
            	$state = 0;
            	$msg = 'File extension is not supported!';
            	$check - false;
            }          
        }else{
        	$input['photo'] = $version->photo; 
        }
        if($check){
        	if($version->update($input)){
	        	$state = 1;
	        	$msg = 'Update version successful!';
	        	$row_data ='<td>0</td>';
	        	$row_data .='<td>'.$input['version_name'].'</td>';
	        	if(!empty($input['photo'])){
	        		$row_data .='<td><img style="height:30px" src= "'.url('public').'/upload/'.$input['photo'].'" /></td>';
	        	}else{
	        		$row_data .='<td><img style="height:30px" src= "'.url('/').Config::get('no_image').'" /></td>';
	        	}
	        	
	        	$row_data .='<td>'.$input['keyword'].'</td>';
	        	$row_data .='<td>'.$input['link_exe'].'</td>';
	        	$row_data .='<td>'.$input['link_android'].'</td>';
	        	$row_data .='<td>'.$input['link_ios'].'</td>';
	        	$row_data .='<td>'.$input['video'].'</td>';
	        	$row_data .='<td>'.$input['description'].'</td>';
	        	$row_data .='<td><a href="'.url('/').'/admin/version/photo/'.$request->id.'" class="btn green"><i class="fa fa-dashboard li-btn"></i>Photo</a></td>';
	        	$row_data .='<td><a href="#" class="btn blue update-version"><i class="fa fa-pencil li-btn"></i>Update</a></td>';
	        	$row_data .='<td><a href="#" class="btn btn-danger delete-version"><i class="fa fa-trash-o li-btn"></i>Delete</a></td>';

	        }else{
	        	$state = 0;
	        	$msg = 'Update unsuccess!';
	        }
        }
        
		$data = array(
			'state'=>$state,
			'msg'  =>$msg,
			'data' => $row_data
		);
		return json_encode($data);
    }
    public function delete(Request $request){
    	$version = Version::find($request->id);
    	if(!empty($version)){
    		if(!empty($version->photo)){
	    		$img = Config::get('constants.path_upload').$version->photo;
	                 if(!empty($version->photo)){
	                    if(file_exists($img)){
	                         unlink($img);
	                     }
	                 }
		        }
            if($version->delete()){
            	$state = 1;
            	$msg = 'Deleted successful!';
	    	}else{
	    		$state = 0;
            	$msg = 'Deleted unsuccessful!';
	    	}
	    }else{
    		$state = 0;
    		$msg = 'Can not find this version on server';
    	}
    	
    	$data = array(
    		'state'=>$state,
    		'msg'=>$msg);
    	return json_encode($data);
    }

    public function photo($id){
    	$photos = ImageDetail::where('version_id','=',$id)->paginate(10);
    	return view('admin.software.photo')->with(['photos'=>$photos,'version_id'=>$id]);
    }

    public function postAddPhoto(Request $request){
    	echo "<pre>";
    	print_r($request->all());
    	echo "</pre>";
    	die;
		echo json_encode($data);
    }

}
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DesignController extends Controller
{
    public function login(){
        return view('new.login');
    }
    public function control(){
        return view('new.control');
    }
    public function setting(){
        return view('new.setting');
    }
}
<?php
/*
@author: Dang Van Hung
15/08/2017
*/
namespace App\Http\Controllers\Base;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Config;
use DB;
use Input;
class BaseController extends Controller
{
    protected $themePath;
    
    protected $model;

    protected $viewPath;
    protected $uploadPath;
    protected $imagePath;
    protected $title;
    protected $data;
    protected $headerTitle;

    protected $className;   //using for create, update, delete url
    public function __construct(){
    	$this->themePath = Config::get('constants.theme_path');
        $this->viewPath = 'admin/index';
        $this->uploadPath = Config::get('constants.upload_path');
        $this->imagePath = url('/').Config::get('constants.image_path');
    }
    public function index(){
    	$rows = $this->model->getAll();
        $attributes = $this->model->getAttributes();
        
        foreach($rows as $row_key => $row){
            foreach ($attributes as $attribute_key => $attribute) {
                $category = $attribute['category'];
                switch($category){
                    case 'select':
                        if(isset($attribute['depend_table'])){
                            $depend_table = $attribute['depend_table'];
                            $record = DB::table($depend_table['name'])->where('id',$row->$attribute_key)->first();
                            $row->$attribute_key = $record->$depend_table['getted_field'];
                        }
                        break;
                    case 'input':
                        switch($attribute['type']){
                            case 'checkbox':
                                $data = $attribute['data'];
                                foreach($data as $sData){
                                    if($sData['value'] == $row->$attribute_key){
                                        $row->$attribute_key = $sData['label'];
                                        break;
                                    }
                                }
                                break;
                            case 'file':
                                if($attribute['file_type'] == 'image'){
                                    $image = $this->imagePath.$row->$attribute_key;
                                    if(!@getimagesize($image)){
                                        $image = $this->imagePath.'noimage.png';
                                    }
                                    $row->$attribute_key = '<img src="'.$image.'" style="width: 70px" />';
                                }
                                break;
                        }
                        
                        break;
                }
            }
        }
    	$this->data['rows'] = $rows;
        $this->data['attributes'] = $attributes;
        $this->data['className'] = isset($this->className)?$this->className:'Unknown';
        $this->data['title'] = isset($this->title)?$this->title:'Home';
    	$this->data['headerTitle'] = isset($this->headerTitle)?$this->headerTitle:'Home';

    	return view($this->themePath.$this->viewPath,$this->data);
    }

    public function getAddData(){
        $attributes = $this->model->getAttributes();
        foreach($attributes as $attribute_key => $attribute){
            if($attribute['category'] == 'select'){
                if(isset($attribute['depend_table'])){
                    $rawDatas = DB::table($attribute['depend_table']['name'])->select('id',$attribute['depend_table']['getted_field'])->get();
                    $data = array();
                    foreach($rawDatas as $rawData_key => $rawData){
                        $data[$rawData->id] = $rawData->$attribute['depend_table']['getted_field'];
                    }
                    $attributes[$attribute_key]['data'] = $data;
                    // echo "<pre>";
                    // print_r($attributes[$attribute_key]['data']);
                    // echo "</pre>";
                    // die;
                }
            }
        }
        return json_encode($attributes);      

    }
    public function add(Request $request){
        $input = $request->all();
        $attributes = $this->model->getAttributes();
        $returnData = $request->all();
        $check = true;
        foreach($attributes as $attribute_key => $attribute){
            //Kiểu checkbox và kiểu file
            if($attribute['category'] == 'input'){
                if($attribute['type'] == 'checkbox'){
                    $data = $attribute['data'];
                    if(!empty($request->$attribute_key)){
                        //checked
                        $input[$attribute_key]      = $data['check']['value'];
                        $returnData[$attribute_key] = $data['check']['label'];
                    }else{
                        //unchecked
                        $input[$attribute_key]      = $data['uncheck']['value'];
                        $returnData[$attribute_key] = $data['uncheck']['label'];
                    }
                }elseif($attribute['type'] == 'file'){
                    if($attribute['file_type'] == 'image'){
                        if (Input::hasFile($attribute_key)){            
                            $imageExtension = $request->file($attribute_key)->getClientOriginalExtension();
                            $validExtension = Config::get('constants.image_valid_extension');
                            if(in_array($imageExtension,$validExtension)){            
                                $imageName   = rand(11111, 99999) . '_' . time() . '.' .$imageExtension;
                                $request->file($attribute_key)->move($this->uploadPath, $imageName);
                                $input[$attribute_key] = $imageName;

                                $returnImage = $this->imagePath.$imageName;
                                if(!@getimagesize($returnImage)){
                                    $returnImage = $this->imagePath.'noimage.png';
                                }
                                $returnData[$attribute_key] = $imageName;
                            }else{
                                $check  = false;
                                $state  = 0;
                                $msg    = 'File extension is invalid!';
                                $returnData = [];
                                break;
                            }
                        }
                    }

                }
            }

        }
        
        if($check){
            $id = $this->model->insertGetId($input);
            if(!empty($id)){
                $returnData['id'] = $id;
                $state = 1;
                $msg   = 'Create new record successful!';
            }else{
                $returnData = [];
                $state = 0;
                $msg   = 'Error create new record!';
            }
        }

        $data = [
            'state' => $state,
            '$msg'  => $msg,
            'data'  => $returnData
        ];
        return json_encode($data);
    }

    public function getUpdateData(){

    }

    public function update(Request $request){

    }
}

<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\ApiBaseController as BaseController;
use App\Models\Group;
use App\Models\Equipment;
use Config;
use DB;

class GroupController extends BaseController
{
    public function index(Request $request)
    {
        try {
            $fillable = ['id as id_group', 'name as name_group', 'image as icon_on', 'image_off as icon_off'];
            $groups = Group::select($fillable)->get();
            foreach ($groups as $group) {
                $group->icon_on = url(config('constants.image')) . '/' . $group->icon_on;
                $group->icon_off = url(config('constants.image')) . '/' . $group->icon_off;
            }
            $data = $groups;
            return $this->returnJson(Config::get('code.ok'), Config::get('message.ok'), $data);
        } catch (\Exception $e) {
            echo $e;
            return $this->returnException();
        }
    }

    public function turnOn()
    {
        $request = request();
        try {
            $id_group = $request->id_group;
            if (!empty($id_group) && !is_numeric($id_group)) {
                return $this->returnJson(Config::get('code.parameter_value_invalid'), Config::get('message.parameter_value_invalid'));
            } else {
                $equipment = Equipment::where('group_id', $id_group)->first();
                if (!empty($equipment)) {
                    $equipment = Equipment::where('group_id', $id_group)
                        ->update([
                            'status' => 1
                        ]);
                    Group::where('id', $id_group)->update([
                        'status' => 1
                    ]);
                    return $this->returnJson(Config::get('code.ok'), Config::get('message.ok'));
                } else {
                    return $this->returnJson(Config::get('code.no_data'), Config::get('message.no_data'));
                }

            }
        } catch (\Exception $e) {
            echo $e;
            return $this->returnException();
        }
    }

    public function turnOff()
    {
        $request = request();
        try {
            $id_group = $request->id_group;
            if (!empty($id_group) && !is_numeric($id_group)) {
                return $this->returnJson(Config::get('code.parameter_value_invalid'), Config::get('message.parameter_value_invalid'));
            } else {
                $equipment = Equipment::where('group_id', $id_group)->first();
                if (!empty($equipment)) {
                    $equipment = Equipment::where('group_id', $id_group)
                        ->update([
                            'status' => 1
                        ]);
                    Group::where('id', $id_group)->update([
                        'status' => 0
                    ]);
                    return $this->returnJson(Config::get('code.ok'), Config::get('message.ok'));
                } else {
                    return $this->returnJson(Config::get('code.no_data'), Config::get('message.no_data'));
                }

            }
        } catch (\Exception $e) {
            echo $e;
            return $this->returnException();
        }
    }

    public function listAllEquipment($id)
    {
        try {
            $equipments = DB::table('equipments')->join('equipment_types', 'equipments.equipment_type_id', '=', 'equipment_types.id')
                ->select('equipments.*', 'equipment_types.image as icon_on', 'equipment_types.image_off as icon_off')
                ->where('equipments.group_id', $id)->orderBy('ordered_at')->get();
            foreach ($equipments as $equipment) {
                $equipment->icon_on = url(config('constants.image')) . '/' . $equipment->icon_on;
                $equipment->icon_off = url(config('constants.image')) . '/' . $equipment->icon_off;
            }
            $data = $equipments;
            return $this->returnJson(Config::get('code.ok'), Config::get('message.ok'), $data);
        } catch (\Exception $e) {
            echo $e;
            return $this->returnException();
        }

    }

    public function groupByEquip(Request $request)
    {
        try {
            $iot_device = $request->iot_device;
            $state = $request->state;
            // if (!empty($iot_device)) {
            //     return $this->returnJson(Config::get('code.parameter_value_invalid'), Config::get('message.parameter_value_invalid'));
            // } else {
                $equipment = DB::table('equipments')->where('iotdevice_id',$iot_device)->where('ordered_at',$request->position)->first();
                if (!empty($equipment)) {
                    $id_group = $equipment->group_id;
                    $equip_in_groups = Equipment::where('group_id', $id_group)->get();
                    $is_success = TRUE;
                    foreach ($equip_in_groups as $equip_in_group) {
                        if ($equip_in_group->status != $state) {
                            $is_success = FALSE;
                            break;
                        }
                    }
                    $data = [
                        'success'  => $is_success,
                        'state'    => intval($state),
                        'id_group' => $id_group
                    ];
                    return $this->returnJson(Config::get('code.ok'), Config::get('message.ok'), $data);
                } else {
                    return $this->returnJson(Config::get('code.no_data'), Config::get('message.no_data'));
                }
            //}
        } catch (\Exception $e) {
            echo $e;
            return $this->returnException();
        }
    }
}

<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Response;
use Config;
class ApiBaseController extends Controller
{
	public function returnJson($state,$message,$data=array()){
		$json = [
			'code'	=> $state,
			'message' => $message
		];
		if(!empty($data)){
			$json['data'] = $data;
		}
		return response()->json($json);
	}

	public function returnException(){
		return $this->returnJson(Config::get('code.exception_error'),Config::get('message.exception_error'));
	}
}

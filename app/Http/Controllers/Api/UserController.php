<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\ApiBaseController as BaseController;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Models\Building;
use Config;
use Hash;
use Auth;
use Input;
use Mail;
use App\User;
class UserController extends BaseController
{
    public function login(Request $request)
    {
        try {
            //1. thiếu tham số
            //2. kiểu không hợp lệ
            //3. giá trị tham số không hợp lệ
            //4. user_not_validate
            //5. exception_error
            $username = $request->username;
            $password = $request->pass;
            if (empty($username) || empty($password)) {
                return $this->returnJson(Config::get('code.parameter_not_enought'), Config::get('message.parameter_not_enought'));
            }else {
                $dataLogin = ['email' => $username, 'password' => $password];
                Auth::attempt($dataLogin);
                $user = '';
                if(Auth::check()){
                    $user = Auth::user();
                }
                if (empty($user)) {
                    return $this->returnJson(Config::get('code.user_not_validate'), Config::get('message.user_not_validate'));
                } else {
                    $user = User::find($user->id);
                    $user->token = md5(time() . $user->username . $user->id . rand(1, 9999));
                    $user->save();
                    $building_info = Building::all()[0];
                    $data = [
                        'name_user' => $user->name,
                        'token_user'    => $user->token,
                        'mac'           => $building_info->mac
                    ];

                    return $this->returnJson(Config::get('code.ok'), Config::get('message.ok'), $data);
                }
            }
        } catch (\Exception $e) {
            return $this->returnException();
        }
    }
}

<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\ApiBaseController as BaseController;
use App\Models\Group;
use App\Models\IotDevice;
use Config;
use DB;
class WifiController extends BaseController
{
    public function listAllIot(){
        $request = request();
        $name = $request->name;
        try{
            $iots = IotDevice::where('name_wifi',$name)->get();
            $data = $iots;
            return $this->returnJson(Config::get('code.ok'),Config::get('message.ok'), $data);
        }catch (\Exception $e)
        {
            echo $e;
            return $this->returnException();
        }
    }
}

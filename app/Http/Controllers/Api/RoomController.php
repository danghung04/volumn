<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\ApiBaseController as BaseController;
use App\Models\Room;
use App\Models\Equipment;
use App\Models\EquipmentTypes;
use Config;
use DB;
use App\Supports\Publish;

class RoomController extends BaseController
{
    public function index(Request $request)
    {
        try {
            $fillable = ['id as id_floor', 'name as name_floor'];
            $rooms = Room::select($fillable)->get();
            $data = $rooms;
            return $this->returnJson(Config::get('code.ok'), Config::get('message.ok'), $data);
        } catch (\Exception $e) {
            echo $e;
            return $this->returnException();
        }
    }

    public function equipment()
    {
        $request = request();
        try {
            $id_floor = $request->id_floor;
            if (!empty($id_floor) && !is_numeric($id_floor)) {
                return $this->returnJson(Config::get('code.parameter_value_invalid'), Config::get('message.parameter_value_invalid'));
            } else {
                $fillable = ['e.id', 'e.room_id','e.iotdevice_id','e.ordered_at', 'e.name_equip', 'e.status', 'et.image', 'et.image_off'];
                $equipments = DB::table('equipments as e')
                    ->select($fillable)
                    ->join('equipment_types as et', 'et.id', '=', 'e.equipment_type_id')
                    ->where('e.room_id', $id_floor)->get();
                $data = [];
                foreach ($equipments as $equipment) {
                    $tmp = [
                        'id_equip'   => $equipment->id,
                        'id_floor'   => $equipment->room_id,
                        'name_equip' => $equipment->name_equip,
                        'state'      => $equipment->status,
                        'iotdevice_id'  => $equipment->iotdevice_id,
                        'stt'       => $equipment->ordered_at,
                        'icon_on'    => url(config('constants.image')) . '/' . $equipment->image,
                        'icon_off'   => url(config('constants.image')) . '/' . $equipment->image_off,
                    ];
                    $data[] = $tmp;
                }
                return $this->returnJson(Config::get('code.ok'), Config::get('message.ok'), $data);

            }
        } catch (\Exception $e) {
            echo $e;
            return $this->returnException();
        }
    }

    public function turnOn()
    {
        $request = request();
        try {
            $id_floor = $request->id_floor;
            $id_equip = $request->id_equip;
            if (!empty($id_floor) && !is_numeric($id_floor)) {
                return $this->returnJson(Config::get('code.parameter_value_invalid'), Config::get('message.parameter_value_invalid'));
            } else {
                $equipment = Equipment::where('id', $id_equip)->where('room_id', $id_floor)->first();
                if (!empty($equipment)) {
                    Equipment::where('id', $id_equip)->where('room_id', $id_floor)
                        ->update([
                            'status' => 1
                        ]);
                    $publish = new Publish();
                    $publish->publish($equipment->id, 1);
                    return $this->returnJson(Config::get('code.ok'), Config::get('message.ok'));
                } else {
                    return $this->returnJson(Config::get('code.no_data'), Config::get('message.no_data'));
                }

            }
        } catch (\Exception $e) {
            echo $e;
            return $this->returnException();
        }
    }

    public function turnOff()
    {
        $request = request();
        try {
            $id_floor = $request->id_floor;
            $id_equip = $request->id_equip;
            if (!empty($id_floor) && !is_numeric($id_floor)) {
                return $this->returnJson(Config::get('code.parameter_value_invalid'), Config::get('message.parameter_value_invalid'));
            } else {
                $equipment = Equipment::where('id', $id_equip)->where('room_id', $id_floor)->first();
                if (!empty($equipment)) {
                    Equipment::where('id', $id_equip)->where('room_id', $id_floor)
                        ->update([
                            'status' => 0
                        ]);
                    $publish = new Publish();
                    $publish->publish($equipment->id, 0);
                    return $this->returnJson(Config::get('code.ok'), Config::get('message.ok'));
                } else {
                    return $this->returnJson(Config::get('code.no_data'), Config::get('message.no_data'));
                }

            }
        } catch (\Exception $e) {
            echo $e;
            return $this->returnException();
        }
    }

    public function alarmEquip()
    {

        $request = request();
        try {
            $id_floor = $request->id_floor;
            $id_equip = $request->id_equip;
            $time = $request->time;
            $alarm_state = $request->alarm_state;
            if ((!empty($id_floor) && !is_numeric($id_floor)) || (!empty($id_equip) && !is_numeric($id_equip)) || (!empty($alarm_state) && !is_numeric($alarm_state))) {
                return $this->returnJson(Config::get('code.parameter_value_invalid'), Config::get('message.parameter_value_invalid'));
            } else {
                $equipment = Equipment::where('id', $id_equip)->where('room_id', $id_floor)->first();
                if (!empty($equipment)) {
                    Equipment::where('id', $id_equip)->where('room_id', $id_floor)
                        ->update([
                            'time'        => $time,
                            'alarm_state' => $alarm_state
                        ]);
                    return $this->returnJson(200, 'OK');
                } else {
                    return $this->returnJson(9994, 'Không có dữ liệu');
                }

            }
        } catch (\Exception $e) {
            echo $e;
            return $this->returnException();
        }
    }

    public function alarmGroup()
    {
        $request = request();
        try {
            $id_group = $request->id_group;
            $time = $request->time;
            $alarm_state = $request->alarm_state;
            if ((!empty($id_group) && !is_numeric($id_group)) || (!empty($alarm_state) && !is_numeric($alarm_state))) {
                return $this->returnJson(Config::get('code.parameter_value_invalid'), Config::get('message.parameter_value_invalid'));
            } else {
                $equipment = Equipment::where('group_id', $id_group)->first();
                if (!empty($equipment)) {
                    Equipment::where('group_id', $id_group)
                        ->update([
                            'time'        => $time,
                            'alarm_state' => $alarm_state
                        ]);
                    return $this->returnJson(200, 'OK');
                } else {
                    return $this->returnJson(9994, 'Không có dữ liệu');
                }

            }
        } catch (\Exception $e) {
            echo $e;
            return $this->returnException();
        }
    }

    public function getAllImages(){
        $equipmentTypes = EquipmentTypes::all();
        $images = array();
        foreach($equipmentTypes as $equipmentType){
            $images[] = [
                'icon_on'    => url(config('constants.image')) . '/' . $equipmentType->image,
                'icon_off'   => url(config('constants.image')) . '/' . $equipmentType->image_off,
            ];
        }
        $data = $images;
        return $this->returnJson(Config::get('code.ok'), Config::get('message.ok'), $data);
    }

    public function updateStatus(Request $request){
        try {
            $iotdevice_id = $request->iotdevice_id;
            $stt = $request->stt;
            $status = $request->status;
            if (!empty($iotdevice_id) && !is_numeric($stt) && !is_numeric($status)) {
                return $this->returnJson(Config::get('code.parameter_value_invalid'), Config::get('message.parameter_value_invalid'));
            } else {
                $equipment = Equipment::where('iotdevice_id', $iotdevice_id)->where('ordered_at', $stt)->first();
                if (!empty($equipment)) {
                    Equipment::where('iotdevice_id', $iotdevice_id)->where('ordered_at', $stt)
                        ->update([
                            'status' => $status
                        ]);
                    $publish = new Publish();
                    $publish->publish($equipment->id, $status);
                    return $this->returnJson(Config::get('code.ok'), Config::get('message.ok'));
                } else {
                    return $this->returnJson(Config::get('code.no_data'), Config::get('message.no_data'));
                }

            }
        } catch (\Exception $e) {
            echo $e;
            return $this->returnException();
        }
    }
}

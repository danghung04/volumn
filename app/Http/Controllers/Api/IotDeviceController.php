<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\ApiBaseController as BaseController;
use App\Models\IotDevice;
use App\Models\Equipment;
use Config;
use DB;
class IotDeviceController extends BaseController
{
    public function store(Request $request){
        try{
            $check_exist = IotDevice::find($request->id);
            if($check_exist)
                return $this->returnJson(303,'ID đã tồn tại');
            IotDevice::create($request->all());
            return $this->returnJson(Config::get('code.ok'),Config::get('message.ok'));
        }catch (\Exception $e)
        {
            echo $e;
            return $this->returnException();
        }
    }

    public function update(Request $request){
        try{
            $id = $request->id;
            $status = $request->status;
            if (empty($id) || !is_numeric($id) || empty($status) || !is_numeric($status)) {
                return $this->returnJson(Config::get('code.parameter_value_invalid'), Config::get('message.parameter_value_invalid'));
            } else {
                IotDevice::where([
                    'id' => $id
                ])->update([
                    'online' => $status
                ]);
                return $this->returnJson(Config::get('code.ok'),Config::get('message.ok'));
            }
        }catch (\Exception $e)
        {
            echo $e;
            return $this->returnException();
        }
    }
    public function listAllStatus($id){
        try{
            $equipments = DB::table('equipments')->join('equipment_types', 'equipments.equipment_type_id', '=','equipment_types.id')
                ->select('equipments.*', 'equipment_types.image as icon_on','equipment_types.image_off as icon_off')->where('iotdevice_id',$id)->orderBy('ordered_at')->get();
            foreach($equipments as $equipment){
                $equipment->icon_on = url(config('constants.image')).'/'.$equipment->icon_on;
                $equipment->icon_off = url(config('constants.image')).'/'.$equipment->icon_off;
            }
            $data = $equipments;
            return $this->returnJson(Config::get('code.ok'),Config::get('message.ok'), $data);
        }catch (\Exception $e)
        {
            echo $e;
            return $this->returnException();
        }
    }
    public function getIotDevice($id)
    {
        try{
            $equipments = DB::table('iotdevice')->where('id',$id)->first();
            $data = $equipments;
            return $this->returnJson(Config::get('code.ok'),Config::get('message.ok'), $data);
        }catch (\Exception $e)
        {
            echo $e;
            return $this->returnException();
        }
    }
}

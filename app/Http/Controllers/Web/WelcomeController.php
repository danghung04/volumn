<?php

namespace App\Http\Controllers\Web;

use App\Models\Building;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\EquipmentTypes;
use App\Models\Room;
use App\Models\Equipment;
use App\Models\Group;
use App\Models\SubRoom;
use App\Models\IotDevice;
use Auth;
use DB;

class WelcomeController extends Controller
{

    public function index()
    {
        $request = request();
        $data['equipment_types'] = EquipmentTypes::all();
        $data['rooms'] = Room::all();
        $data['iots'] = IotDevice::where('active', 1)->get();
        $data['room_active_id'] = $request->rid;
        $room_active = NULL;
        if (empty($data['room_active_id'])) {//Nếu load lần đầu thì lấy trang đầu tiên
            if (count($data['rooms']) > 0) {
                $room_active = $data['rooms'][0];
            }
        } else {
            $room_active = Room::find($request->rid);
        }
        $data['room_active'] = $room_active;
        $data['total_equip'] = '00';
        $data['count_equip_on'] = '00';
        $data['count_equip_alarm'] = '00';
        $data['building_info'] = Building::all()[0];
        $data['modal_start'] = view('web.alarm.modal-start');
        $data['modal_alarm'] = view('web.alarm.modal-alarm');
        $data['modal_rename_building'] = view('web.building.modal-rename', $data);
        $data['modal_info_building'] = view('web.building.modal-info', $data);
        $data['modal_login'] = view('web.building.modal-login');
        $data['modal_change_avatar'] = view('web.building.modal-change-avatar');
        $data['sub_rooms'] = [];
        $data['equipments'] = [];
        if (empty($room_active)) {
            return view('web.welcome', $data);
        }
        $data['sub_rooms'] = SubRoom::where('room_id', $room_active->id)->get();
        $data['equipments'] = DB::table('equipments')
            ->join('equipment_types', 'equipments.equipment_type_id', '=', 'equipment_types.id')
            ->select(['equipments.id', 'equipments.name_equip', 'equipments.room_id', 'equipments.iotdevice_id', 'equipments.ordered_at', 'equipments.status', 'equipments.equipment_type_id', 'equipments.x_offset', 'equipments.y_offset', 'equipment_types.icon', 'equipment_types.image', 'equipment_types.image_off'])
            ->where('equipments.room_id', $room_active->id)->get();
        $data['total_equip'] = str_pad(Equipment::count(), 2, '0', STR_PAD_LEFT);
        $data['count_equip_on'] = str_pad(Equipment::where('status', 1)->count(), 2, '0', STR_PAD_LEFT);
        $data['count_equip_alarm'] = str_pad(Equipment::where('alarm_state', 1)->count(), 2, '0', STR_PAD_LEFT);
        foreach ($data['equipments'] as $group):
            $group->active_image = $group->image;
            if (!$group->status):
                $group->active_image = $group->image_off;
            endif;
        endforeach;
        return view('web.welcome', $data);
    }

    public function postCoordinate()
    {
        $request = request();
        $input = $request->except(['_token']);
        Equipment::create($input);
    }

    public function join()
    {
        $request = request();
        echo '<pre>';
        print_r($request->all());
        return 'Ok joined guys';
    }

    public function loginSetting(Request $request)
    {

        echo '<pre>';
        print_r($request->all());
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password, 'role' => 1])) {
            return redirect(route('admin.index'));
        } else {
            return redirect()->back();
        }
    }

    public function group(){
        $data['rooms'] = Room::all();
        $data['iots'] = IotDevice::where('active', 1)->get();
        $data['total_equip'] = '00';
        $data['count_equip_on'] = '00';
        $data['count_equip_alarm'] = '00';
        $data['building_info'] = Building::all()[0];
        $data['modal_start'] = view('web.alarm.modal-start');
        $data['modal_alarm'] = view('web.alarm.modal-alarm');
        $data['modal_rename_building'] = view('web.building.modal-rename', $data);
        $data['modal_info_building'] = view('web.building.modal-info', $data);
        $data['modal_login'] = view('web.building.modal-login');
        $data['modal_change_avatar'] = view('web.building.modal-change-avatar');
        $data['total_equip'] = str_pad(Equipment::count(), 2, '0', STR_PAD_LEFT);
        $data['count_equip_on'] = str_pad(Equipment::where('status', 1)->count(), 2, '0', STR_PAD_LEFT);
        $data['count_equip_alarm'] = str_pad(Equipment::where('alarm_state', 1)->count(), 2, '0', STR_PAD_LEFT);
        $data['groups'] = Group::all();
        foreach ($data['groups'] as $group):
            $group->active_image = $group->image;
            if (!$group->status):
                $group->active_image = $group->image_off;
            endif;
        endforeach;
        return view('web.group', $data);
    }
}

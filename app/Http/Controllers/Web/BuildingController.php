<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Building;
class BuildingController extends Controller
{
    public function changeInfo(Request $request){
        $input = $request->except(['_token', 'id']);
        Building::where('id', $request->id)
            ->update($input);
        return redirect()->back();
    }
}

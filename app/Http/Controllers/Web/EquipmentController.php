<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Equipment;
use App\Models\EquipmentTypes;
use App\Supports\Publish;
use App\Events\EquipmentEvent;
class EquipmentController extends Controller
{
    //dùng với trường hợp có ordered_at trong dữ liệu gửi lên
    // public function create(){
    // 	$request = request();
    // 	$input = $request->except(['_token']);
    // 	$where_existed = [
    // 		'ordered_at' => $input['ordered_at'],
    // 		'room_id'	=> $input['room_id']
    // 	];
    // 	$check = Equipment::where($where_existed)->count();
    // 	if($check > 0){
    // 		$response = [
    // 			'state'	=> 0,
    // 			'msg'	=> 'Số thứ tự đã tồn tại'
    // 		];
    // 	}else{
    // 		$equipment = Equipment::create($input);
    // 		$response = [
    // 			'state'	=> 1,
    // 			'msg'	=> 'Tạo mới thành công',
    //             'id'    => $equipment->id
    // 		];
    // 	}

    // 	return response()->json($response);
    // }

    public function create(){
        $request = request();
        $input = $request->except(['_token']);
        $where = [
            'iotdevice_id' => $input['iotdevice_id'],
            'room_id'   => $input['room_id']
        ];
        $order_max = Equipment::where($where)->max('ordered_at');
        $input['ordered_at'] = $order_max + 1;
        $equipment = Equipment::create($input);
        $response = [
            'state' => 1,
            'msg'   => 'Tạo mới thành công',
            'id'    => $equipment->id
        ];
        return response()->json($response);
    }

    public function update(){
    	
    }

    public function updatePosition(){
        $request = request();
        $input = $request->except(['_token']);
        $equipment = Equipment::find($request->id);
        $equipment->update($input);
        $response = [
            'state' => 200,
            'code'  => 'OK'
        ];
        return response()->json($response);
    }

    public function updateStatus(){
        $request = request();
        $where = [
            'iotdevice_id' => $request->iotdevice_id,
            'ordered_at'    => $request->ordered_at
        ];
        $equipment = Equipment::where($where)->first();
        if(!empty($equipment)){
            Equipment::where($where)->update([
                'status'    => $request->status
            ]);
            $response = [
                'id'    => $equipment->id,
                'status'    => $request->status
            ];
            return response()->json($response);
        }
    }
}

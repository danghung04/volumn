<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Auth;
use Mail;
use Validator;
class UserController extends Controller
{
    public function changeAvatar(Request $request){
        if ($request->avatar) {
            $old_img = config('constants.path_upload_image') . Auth::user()->avatar;
            if (!empty(Auth::user()->avatar)) {
                if (file_exists($old_img)) {
                    unlink($old_img);
                }
            }
            $extension = $request->file('avatar')->getClientOriginalExtension();
            $name = rand(11111, 99999) . '_' . time() . '.' . $extension;
            $request->file('avatar')->move(config('constants.path_upload_image'), $name);
            $user = User::find(Auth::user()->id);
            $user->avatar = $name;
            $user->save();
            Auth::user()->avatar = $name;
        }
        return redirect()->back();
    }

    public function showEmailResetPass(){
        $data['title'] = 'Quên mật khẩu';
        return view('auth.passwords.email', $data);
    }

    public function emailResetPass(Request $request){
        $data['email'] = $request->email;
        $str_random = str_random(32);
        $data['token'] = $str_random;
        $user = User::where('email', $data['email'])->get();
        if(count($user) < 1){
            return redirect()->back();
        }
        User::where('email', $data['email'])
            ->update([
                'remember_token' => $str_random
            ]);
        $data['user'] = $user[0];
        Mail::send('mail.reset-password', $data, function ($m) use ($data){
            $m->to($data['user']->email, 'Reset mật khẩu')->subject('Reset mật khẩu');
        });
        return redirect(route('login'));
    }

    public function showResetPass(){
        $data['token'] = request()->get('token');
        $user = User::where('remember_token', $data['token'])->get();
        if(count($user) < 1){
            return redirect(route('login'));
        }
        $data['title'] = 'Reset mật khẩu';
        return view('auth.passwords.reset', $data);
    }
    public function resetPass(Request $request){
        Validator::make($request->all(),
            [
                'password'       => 'confirmed| min: 6',
            ],
            [
                'password.confirmed'       => 'Xác nhận mật khẩu không đúng',
                'password.min'       => 'Mật khẩu quá ngắn',
            ])->validate();
        $data = $request->all();
        User::where('remember_token', $data['remember_token'])
            ->update([
                'password' => bcrypt($data['password'])
            ]);
        return redirect(route('login'));
    }
}

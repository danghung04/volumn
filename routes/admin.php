<?php
Route::group(['prefix' => 'admin','middleware' => 'admin', 'namespace' => 'Admin'], function () {
	Route::get('/',['as' => 'admin.index', 'uses' => 'HomeController@index']);
    Route::group(['prefix' => 'room'], function () {
        Route::get('/', ['as' => 'admin.room.index', 'uses' => 'RoomController@index']);
        Route::get('create', ['as' => 'admin.room.create', 'uses' => 'RoomController@create']);
        Route::post('store', ['as' => 'admin.room.store', 'uses' => 'RoomController@store']);
        Route::get('edit/{id}', ['as' => 'admin.room.edit', 'uses' => 'RoomController@edit']);
        Route::post('update', ['as' => 'admin.room.update', 'uses' => 'RoomController@update']);
        Route::get('delete/{id}', ['as' => 'admin.room.delete', 'uses' => 'RoomController@delete']);
        Route::post('check-duplicate', ['as' => 'admin.room.checkDuplicate', 'uses' => 'RoomController@checkDuplicate']);
    });
    Route::group(['prefix' => 'sub-room'], function () {
        Route::post('store', ['as' => 'admin.sub-room.store', 'uses' => 'SubRoomController@store']);
        Route::post('update-position', ['as' => 'admin.sub-room.updatePosition', 'uses' => 'SubRoomController@updatePosition']);
    });

    Route::group(['prefix' => 'equipment'], function () {
    	Route::get('/', ['as' => 'admin.equipment.index', 'uses' => 'EquipmentController@index']);
        Route::post('add', ['as' => 'admin.equipment.postAdd', 'uses' => 'EquipmentController@postAdd']);
        Route::get('edit/{id}', ['as' => 'admin.equipment.edit', 'uses' => 'EquipmentController@edit']);
        Route::post('update', ['as' => 'admin.equipment.update', 'uses' => 'EquipmentController@update']);
        Route::post('update-position', ['as' => 'admin.equipment.updatePosition', 'uses' => 'EquipmentController@updatePosition']);
        Route::post('delete/', ['as' => 'admin.equipment.delete', 'uses' => 'EquipmentController@delete']);
    });

    Route::group(['prefix' => 'iot-device'], function () {
        Route::get('/', ['as' => 'admin.iot-device.index', 'uses' => 'IotDeviceController@index']);
        Route::get('create', ['as' => 'admin.iot-device.create', 'uses' => 'IotDeviceController@create']);
        Route::post('store', ['as' => 'admin.iot-device.store', 'uses' => 'IotDeviceController@store']);
        Route::get('edit/{id}', ['as' => 'admin.iot-device.edit', 'uses' => 'IotDeviceController@edit']);
        Route::post('update', ['as' => 'admin.iot-device.update', 'uses' => 'IotDeviceController@update']);
        Route::post('active', ['as' => 'admin.iot-device.active', 'uses' => 'IotDeviceController@active']);
        Route::get('delete/{id}', ['as' => 'admin.iot-device.delete', 'uses' => 'IotDeviceController@delete']);
        Route::post('delete', ['as' => 'admin.iot-device.postDelete', 'uses' => 'IotDeviceController@postDelete']);
        Route::post('max-equip', ['as' => 'admin.iot-device.getMaxEquip', 'uses' => 'IotDeviceController@getMaxEquip']);
    });

    Route::group(['prefix' => 'equipment-type'], function () {
        Route::get('/', ['as' => 'admin.equipment-type.index', 'uses' => 'EquipmentTypeController@index']);
        Route::get('create', ['as' => 'admin.equipment-type.create', 'uses' => 'EquipmentTypeController@create']);
        Route::post('store', ['as' => 'admin.equipment-type.store', 'uses' => 'EquipmentTypeController@store']);
        Route::get('edit/{id}', ['as' => 'admin.equipment-type.edit', 'uses' => 'EquipmentTypeController@edit']);
        Route::post('update', ['as' => 'admin.equipment-type.update', 'uses' => 'EquipmentTypeController@update']);
        Route::post('update-active', ['as' => 'admin.equipment-type.updateActive', 'uses' => 'EquipmentTypeController@updateActive']);
        Route::get('delete', ['as' => 'admin.equipment-type.delete', 'uses' => 'EquipmentTypeController@delete']);
        Route::post('check-duplicate', ['as' => 'admin.equipment-type.checkDuplicate', 'uses' => 'EquipmentTypeController@checkDuplicate']);
    });
    Route::group(['prefix' => 'group'], function () {
        Route::get('/', ['as' => 'admin.group.index', 'uses' => 'GroupController@index']);
        Route::get('create', ['as' => 'admin.group.create', 'uses' => 'GroupController@create']);
        Route::post('store', ['as' => 'admin.group.store', 'uses' => 'GroupController@store']);
        Route::get('edit/{id}', ['as' => 'admin.group.edit', 'uses' => 'GroupController@edit']);
        Route::post('update', ['as' => 'admin.group.update', 'uses' => 'GroupController@update']);
        Route::get('delete', ['as' => 'admin.group.delete', 'uses' => 'GroupController@delete']);
        Route::post('check-duplicate', ['as' => 'admin.group.checkDuplicate', 'uses' => 'GroupController@checkDuplicate']);
    });

    Route::group(['prefix' => 'user'], function () {
    	Route::get('/', ['as' => 'admin.user.index', 'uses' => 'UserController@index']);
        Route::post('store', ['as' => 'admin.user.store', 'uses' => 'UserController@store']);
        Route::get('edit', ['as' => 'admin.user.edit', 'uses' => 'UserController@edit']);
        Route::post('update', ['as' => 'admin.user.update', 'uses' => 'UserController@update']);
        Route::get('delete', ['as' => 'admin.user.delete', 'uses' => 'UserController@delete']);
        Route::post('check-duplicate', ['as' => 'admin.user.checkDuplicate', 'uses' => 'UserController@checkDuplicate']);
    });
    Route::group(['prefix' => 'wifi'], function () {
        Route::post('update', ['as' => 'admin.wifi.update', 'uses' => 'WifiController@update']);
        Route::post('iot', ['as' => 'admin.wifi.iot', 'uses' => 'WifiController@iot']);
    });
});


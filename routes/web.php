<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
include 'admin.php';
include 'api.php';
Route::group(['namespace' => 'Web', 'middleware' => 'auth'], function () {
    Route::get('/', ['as' => 'web.index', 'uses' => 'WelcomeController@index']);
    Route::get('/group', ['as' => 'web.group', 'uses' => 'WelcomeController@group']);
    Route::post('change-avatar',['as' => 'changeAvatar', 'uses' => 'UserController@changeAvatar']);
    Route::post('change-info-building',['as' => 'changeInfo', 'uses' => 'BuildingController@changeInfo']);
});

Route::group(['prefix' => 'web', 'namespace' => 'Web', 'middleware' => 'auth'], function () {
    Route::group(['prefix' => 'equipment'], function () {
        Route::post('/create', ['as' => 'web.equipment.create', 'uses' => 'EquipmentController@create']);
        Route::post('/update', ['as' => 'web.equipment.update', 'uses' => 'EquipmentController@update']);
    });
});

Route::group(['prefix' => 'equipment'], function () {
    Route::get('/join', ['as' => 'web.equipment.join', 'uses' => 'Web\WelcomeController@join']);
    Route::post('/update-status', ['as' => 'web.equipment.updateStatus', 'uses' => 'Web\EquipmentController@updateStatus']);
});

Route::get('socket', 'SocketController@index');

Auth::routes();
Route::get('logout', 'Auth\LoginController@logout');
Route::get('/home', 'HomeController@index')->name('home');
Route::post('/login-setting', 'Web\WelcomeController@loginSetting');

Route::group(['namespace' => 'Web'], function(){
    Route::get('email-reset-pass', ['as' => 'web.showEmailResetPass', 'uses' => 'UserController@showEmailResetPass']);
    Route::post('email-reset-pass', ['as' => 'web.emailResetPass', 'uses' => 'UserController@emailResetPass']);
    Route::get('reset-pass', ['as' => 'web.showResetPass', 'uses' => 'UserController@showResetPass']);
    Route::post('process-reset-pass', ['as' => 'web.resetPass', 'uses' => 'UserController@resetPass']);
});
Route::get('send-message', 'RedisController@index');
Route::post('send-message', ['as' => 'postMessage', 'uses' => 'RedisController@postSendMessage']);
Route::group(['prefix' => 'design'], function () {
    Route::get('login', 'DesignController@login');
    Route::get('control', 'DesignController@control');
    Route::get('setting', 'DesignController@setting');
});
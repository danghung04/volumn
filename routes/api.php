<?php
Route::group(['prefix' => 'api', 'namespace' => 'Api'], function () {
    Route::post('login', 'UserController@login'); //login -> ok
    Route::post('/floors', 'RoomController@index');
    Route::group(['prefix' => 'floor'], function () {
        Route::post('/equipment', 'RoomController@equipment');
    });
    Route::post('/turn_on', 'RoomController@turnOn');
    Route::post('/update_status', 'RoomController@updateStatus');
    Route::post('/turn_off', 'RoomController@turnOff');
    Route::group(['prefix' => 'alarm'], function () {
        Route::post('/equip', 'RoomController@alarmEquip');
        Route::post('/group', 'RoomController@alarmGroup');
    });

    Route::post('/groups', 'GroupController@index');
    Route::group(['prefix' => 'turn_on'], function () {
        Route::post('/group', 'GroupController@turnOn');
    });
    Route::group(['prefix' => 'turn_off'], function () {
        Route::post('/group', 'GroupController@turnOn');
    });
    Route::post('/all-images', 'RoomController@getAllImages');
    Route::get('iot-all-status/{id}', 'IotDeviceController@listAllStatus');
    Route::get('iot-info/{id}', 'IotDeviceController@getIotDevice');
    Route::get('group-all-equip/{id}', 'GroupController@listAllEquipment');
    Route::get('wifi-all-iot', 'WifiController@listAllIot');
    Route::group(['prefix' => 'iot'], function(){
        Route::post('add','IotDeviceController@store');
        Route::post('update','IotDeviceController@update');
    });
    Route::get('group-by-equip','GroupController@groupByEquip');
});

<?php
/**
 * Created by PhpStorm.
 * User: YaangVu
 * Date: 12/20/2017
 * Time: 19:43
 */
return [
    'path_upload_image'      => public_path() . '/uploads/images/',
    'path_upload_icon'       => public_path() . '/uploads/icons/',
    'image'                  => 'public/uploads/images/',
    'icon'                   => 'public/uploads/icons/',
    'plugin'                   => 'public/plugin/',
    'choose_image'           => 'public/images/choose-image.gif',
    'default_avatar'           => 'public/images/default_avatar.png',
    'connection_image'           => 'public/images/connection.png',
    'default_group_icon'           => 'public/images/default_group_icon.png',
    'default_group_on'           => 'public/images/default_group_on.png',
    'default_group_off'           => 'public/images/default_group_off.png',
    'default_equip_icon'           => 'public/images/default_equip_icon.png',
    'default_equip_on'           => 'public/images/default_equip_on.png',
    'default_equip_off'           => 'public/images/default_equip_off.png',
    'divider_image'           => 'public/images/divider.png',
    'image_valid_extensions' => ['png', 'PNG', 'jpg', 'JPG', 'jpeg', 'JPEG', 'gif', 'GIF', 'ico'],
    'admin_role'             => 1,
    'user_role'              => 2,
    'status'                 => [
        0 => 'Tắt',
        1 => 'Bật',
    ],
    'active_options'         => [
        'all'        => -1,
        'non-active' => 0,
        'active'     => 1,
    ],
];

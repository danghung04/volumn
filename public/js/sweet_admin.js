  /* global $, swal, FileReader */
  $('.download').on('click', function () {
    $('html, body').animate({scrollTop: $('.download-section').offset().top}, 1000)
  })

  $('.showcase.normal button').on('click', function () {
    window.alert('Oops! Something went wrong!')
  })

  $('.showcase.sweet button').on('click', function () {
    swal('Oops...', 'Something went wrong!', 'error').catch(swal.noop)
  })

  $('.donate').on('click', function () {
    $('.currencies').toggleClass('hidden');
  })

  $('.currencies').on('click', function (e) {
    e.stopPropagation()
  })

  $('.examples .message button').on('click', function () {
    swal('Any fool can use a computer').catch(swal.noop)
  })

  $('.examples .timer button').on('click', function () {
    swal({
      title: 'Auto close alert!',
      text: 'I will close in 2 seconds.',
      timer: 2000
    }).then(
      function () {},
      // handling the promise rejection
      function (dismiss) {
        if (dismiss === 'timer') {
          console.log('I was closed by the timer')
        }
      }
    )
  })

  $('.examples .html button').on('click', function () {
    swal({
      title: '<i>HTML</i> <u>example</u>',
      type: 'info',
      html:
        'You can use <b>bold text</b>, ' +
        '<a href="//github.com">links</a> ' +
        'and other HTML tags',
      showCloseButton: true,
      showCancelButton: true,
      confirmButtonText: '<i class="fa fa-thumbs-up"></i> Great!',
      cancelButtonText: '<i class="fa fa-thumbs-down"></i>'
    }).catch(swal.noop)
  })

  $('.examples .html-jquery button').on('click', function () {
    swal({
      title: 'jQuery HTML example',
      html: $('<div>').addClass('some-class').text('jQuery is everywhere.'),
      animation: false,
      customClass: 'animated tada'
    }).catch(swal.noop)
  })

  $('.examples .title-text button').on('click', function () {
    swal('The Internet?', 'That thing is still around?', 'question').catch(swal.noop)
  })

  $('.examples .success button').on('click', function () {
    swal('Good job!', 'You clicked the button!', 'success').catch(swal.noop)
  })


  $('.examples .ajax-request button').on('click', function () {
    swal({
      title: 'Submit email to run ajax request',
      input: 'email',
      showCancelButton: true,
      confirmButtonText: 'Submit',
      width: 600,
      showLoaderOnConfirm: true,
      preConfirm: function (email) {
        return new Promise(function (resolve, reject) {
          setTimeout(function () {
            if (email === 'taken@example.com') {
              reject('This email is already taken.')
            } else {
              resolve()
            }
          }, 2000)
        })
      },
      allowOutsideClick: false
    }).then(function (email) {
      swal({
        type: 'success',
        title: 'Ajax request finished!',
        html: 'Submitted email: ' + '<strong>' + email + '</strong>'
      })
    }).catch(swal.noop)
  })

  $('.examples .chaining-modals button').on('click', function () {
    swal.setDefaults({
      input: 'text',
      confirmButtonText: 'Next &rarr;',
      showCancelButton: true,
      animation: false,
      progressSteps: ['1', '2', '3']
    })

    var steps = [
      {title: 'Question 1', text: 'Chaining swal2 modals is easy'},
      'Question 2',
      'Question 3'
    ]

    swal.queue(steps).then(function (result) {
      swal.resetDefaults()
      swal({
        title: 'All done!',
        html: 'Your answers: <pre>' + JSON.stringify(result) + '</pre>',
        confirmButtonText: 'Lovely!',
        showCancelButton: false
      }).catch(swal.noop)
    }, function () {
      swal.resetDefaults()
    })
  })

  $('.examples .dynamic-queue button').on('click', function () {
    swal.queue([
      {
        title: 'Your public IP',
        confirmButtonText: 'Show my public IP',
        text: 'Your public IP will be received via AJAX request',
        currentProgressStep: 0,
        showLoaderOnConfirm: true,
        preConfirm: function () {
          return new Promise(function (resolve) {
            $.get('https://api.ipify.org?format=json')
              .done(function (data) {
                swal.insertQueueStep(data.ip)
                resolve()
              })
          })
        }
      }
    ]).catch(swal.noop)
  })

  $('.modal-types button').on('click', function () {
    var type = $(this).attr('class').slice(5)
    swal(type + '!', '', type).catch(swal.noop)
  })

  // Google Analytics
  /* eslint-disable */
  if (typeof ga !== 'undeifned') {
    (function (i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function (){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga')
    ga('create', 'UA-83618163-1', 'auto')
    ga('send', 'pageview')
  }